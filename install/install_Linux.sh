#!/bin/sh

echo "====== POLY'ESCAPE DEBIAN INSTALLATER ======"

echo "> PHP 7.2 install"
phpInstall

echo "> Composer install"
apt install composer

echo "> Download of project"
projectInstall

echo "> Install of plugins"
pluginsInstall

echo "> Run of server"
run

echo "============================================"

function phpInstall
{
    sudo apt install ca-certificates apt-transport-https 
    wget -q https://packages.sury.org/php/apt.gpg -O- | sudo apt-key add -
    echo "deb https://packages.sury.org/php/ stretch main" | sudo tee /etc/apt/sources.list.d/php.list
    sudo apt update
    sudo apt install php7.2
    sudo apt install php7.2-cli php7.2-common php7.2-curl php7.2-mbstring php7.2-mysql php7.2-xml
}

function projectInstall
{
    apt install git
    git clone https://gitlab.com/cookiehacker/projet_escape_game
}

function pluginsInstall
{
    cd PROJET_ESCAPE_GAME
    cd polyescape
    ./../installPlugin.sh
}

function run
{
    php bin/console server:start
}