-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  ven. 18 jan. 2019 à 19:18
-- Version du serveur :  10.1.36-MariaDB
-- Version de PHP :  7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `dbcizeau`
--

-- --------------------------------------------------------

--
-- Structure de la table `appartient`
--

CREATE TABLE `appartient` (
  `idgr` int(11) NOT NULL,
  `netu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `appartient`
--

INSERT INTO `appartient` (`idgr`, `netu`) VALUES
(1, 100),
(1, 45646),
(1, 54987),
(1, 265689),
(1, 458864),
(1, 564564),
(1, 2171927);

-- --------------------------------------------------------

--
-- Structure de la table `cadena`
--

CREATE TABLE `cadena` (
  `idcadena` int(11) NOT NULL,
  `intitule` text,
  `niveauc` int(11) DEFAULT NULL,
  `etatc` tinyint(1) DEFAULT NULL,
  `nbessai` int(11) DEFAULT NULL,
  `nbessaimax` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `cadena`
--

INSERT INTO `cadena` (`idcadena`, `intitule`, `niveauc`, `etatc`, `nbessai`, `nbessaimax`) VALUES
(1, 'Première pièce ', 5, 0, 0, 10),
(8, NULL, NULL, 0, 0, 10),
(9, NULL, NULL, 0, 0, 10),
(10, NULL, NULL, 0, 0, 10),
(11, NULL, NULL, 0, 0, 10),
(36, NULL, NULL, 0, 0, 10),
(37, NULL, NULL, 0, 0, 10),
(38, NULL, NULL, 0, 0, 10);

-- --------------------------------------------------------

--
-- Structure de la table `composer_de`
--

CREATE TABLE `composer_de` (
  `idsalle` int(11) NOT NULL,
  `iddev` int(11) NOT NULL,
  `ordresalle` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `composer_de`
--

INSERT INTO `composer_de` (`idsalle`, `iddev`, `ordresalle`) VALUES
(1, 1, '1'),
(2, 1, '2');

-- --------------------------------------------------------

--
-- Structure de la table `contient`
--

CREATE TABLE `contient` (
  `iddm` int(11) NOT NULL,
  `idcadena` int(11) NOT NULL,
  `idsalle` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `contient`
--

INSERT INTO `contient` (`iddm`, `idcadena`, `idsalle`) VALUES
(1, 1, 1),
(1, 8, 1),
(1, 9, 1),
(1, 10, 1),
(1, 11, 2),
(1, 36, 1),
(1, 37, 1),
(1, 38, 2);

-- --------------------------------------------------------

--
-- Structure de la table `devoir`
--

CREATE TABLE `devoir` (
  `iddev` int(11) NOT NULL,
  `statut` varchar(50) DEFAULT NULL,
  `nomDev` varchar(50) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `nbpersonnegroupe` int(11) DEFAULT NULL,
  `nbgroupe` int(11) DEFAULT NULL,
  `idprof` int(11) DEFAULT NULL,
  `idmat` int(11) DEFAULT NULL,
  `maxPoint` int(5) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `devoir`
--

INSERT INTO `devoir` (`iddev`, `statut`, `nomDev`, `date`, `nbpersonnegroupe`, `nbgroupe`, `idprof`, `idmat`, `maxPoint`) VALUES
(1, 'En cours', 'dev1', '2018-12-08', 4, 10, 2221, 1, 500),
(3, 'fermer', NULL, '2018-12-08', 4, 10, 2221, 1, 500);

-- --------------------------------------------------------

--
-- Structure de la table `dm`
--

CREATE TABLE `dm` (
  `iddm` int(11) NOT NULL,
  `note` int(5) DEFAULT '0',
  `idgr` int(11) DEFAULT NULL,
  `iddev` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `dm`
--

INSERT INTO `dm` (`iddm`, `note`, `idgr`, `iddev`) VALUES
(1, 20, 1, 1),
(3, 20, 2, 3),
(4, 20, 3, 3),
(5, 20, 4, 3);

-- --------------------------------------------------------

--
-- Structure de la table `eleve`
--

CREATE TABLE `eleve` (
  `netu` int(11) NOT NULL,
  `classe` varchar(20) DEFAULT NULL,
  `datec` varchar(10) DEFAULT NULL,
  `idpers` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `eleve`
--

INSERT INTO `eleve` (`netu`, `classe`, `datec`, `idpers`) VALUES
(100, '2A21', '2018-2019', 9),
(45646, '2A21', '2018-2019', 7),
(54987, '2A21', '2018-2019', 4),
(265689, '2A21', '2018-2019', 5),
(458864, '2A21', '2018-2019', 3),
(564564, '2A21', '2018-2019', 6),
(2171927, '2A21', '2018-2019', 1);

-- --------------------------------------------------------

--
-- Structure de la table `enigme`
--

CREATE TABLE `enigme` (
  `idenigme` int(11) NOT NULL,
  `nomEnigme` varchar(30) DEFAULT NULL,
  `solution` text,
  `niveau` int(11) DEFAULT NULL,
  `maxpoint` int(11) DEFAULT '0',
  `question` text,
  `idmat` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `enigme`
--

INSERT INTO `enigme` (`idenigme`, `nomEnigme`, `solution`, `niveau`, `maxpoint`, `question`, `idmat`) VALUES
(1, 'test', '2 + 2 = 4', 2, 5, '2 + 2 = ?', 1),
(2, 'E2', '2 + 2 = 4', 2, 5, '2 + 2 = ?', 1),
(3, 'E3', '2 + 2 = 4', 2, 5, '2 + 2 = ?', 1),
(4, 'E4', '2 + 2 = 4', 2, 5, '2 + 2 = ?', 1);

-- --------------------------------------------------------

--
-- Structure de la table `enseigne`
--

CREATE TABLE `enseigne` (
  `idprof` int(11) NOT NULL,
  `idmat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `enseigne`
--

INSERT INTO `enseigne` (`idprof`, `idmat`) VALUES
(2221, 1),
(11452, 4);

-- --------------------------------------------------------

--
-- Structure de la table `est_dans`
--

CREATE TABLE `est_dans` (
  `idenigme` int(11) NOT NULL,
  `iddev` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `est_dans`
--

INSERT INTO `est_dans` (`idenigme`, `iddev`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1);

-- --------------------------------------------------------

--
-- Structure de la table `est_lier_a`
--

CREATE TABLE `est_lier_a` (
  `iddm` int(11) NOT NULL,
  `idobjet` int(11) NOT NULL,
  `idenigme` int(11) NOT NULL,
  `datetrouve` date DEFAULT NULL,
  `idsalle` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `est_lier_a`
--

INSERT INTO `est_lier_a` (`iddm`, `idobjet`, `idenigme`, `datetrouve`, `idsalle`) VALUES
(1, 5, 1, NULL, 1),
(1, 5, 2, NULL, 1),
(1, 5, 4, NULL, 2),
(1, 7, 3, NULL, 2);

-- --------------------------------------------------------

--
-- Structure de la table `groupe`
--

CREATE TABLE `groupe` (
  `idgr` int(11) NOT NULL,
  `nomGroupe` varchar(100) DEFAULT NULL,
  `idpush` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `groupe`
--

INSERT INTO `groupe` (`idgr`, `nomGroupe`, `idpush`) VALUES
(1, 'groupe 1', 1),
(2, 'groupe 2', 1),
(3, 'groupe 2', 1),
(4, 'groupe 2', 1),
(5, 'Groupe 2A21 5', 100),
(6, 'Groupe 2A21 6', 54987),
(7, 'Groupe 2A21 7', 458864),
(8, 'Groupe 2A21 8', 100),
(9, 'Groupe 2A21 9', 54987),
(10, 'Groupe 2A21 10', 458864),
(11, 'Groupe 2A21 11', 100),
(12, 'Groupe 2A21 12', 54987),
(13, 'Groupe 2A21 13', 458864),
(14, 'Groupe 2A21 14', 100),
(15, 'Groupe 2A21 15', 54987),
(16, 'Groupe 2A21 16', 458864),
(17, 'Groupe 2A21 17', 100),
(18, 'Groupe 2A21 18', 54987),
(19, 'Groupe 2A21 19', 458864),
(20, 'Groupe 2A21 20', 100),
(21, 'Groupe 2A21 21', 54987),
(22, 'Groupe 2A21 22', 458864);

-- --------------------------------------------------------

--
-- Structure de la table `is_dans`
--

CREATE TABLE `is_dans` (
  `idsalle` int(11) NOT NULL,
  `idobjet` int(11) NOT NULL,
  `coordobjet` varchar(200) DEFAULT NULL,
  `tailleobjet` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `is_dans`
--

INSERT INTO `is_dans` (`idsalle`, `idobjet`, `coordobjet`, `tailleobjet`) VALUES
(1, 2, '43,222,176,322', 1),
(2, 2, '157,93,237,153', 1),
(1, 3, '120,77,205,141', 1),
(1, 10, '133,379,191,423', 1),
(2, 10, '149,250,192,282', 1);

-- --------------------------------------------------------

--
-- Structure de la table `is_dans_objet`
--

CREATE TABLE `is_dans_objet` (
  `idobjetSuperieur` int(11) NOT NULL,
  `idobjetInferieur` int(11) NOT NULL,
  `coordobjet2` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `is_dans_objet`
--

INSERT INTO `is_dans_objet` (`idobjetSuperieur`, `idobjetInferieur`, `coordobjet2`) VALUES
(3, 4, '54,179,202,290'),
(3, 9, '205,66,302,139'),
(4, 5, '140,164,336,311'),
(4, 6, '39,159,138,233'),
(9, 7, '80,132,337,325'),
(10, 11, '117,79,438,320'),
(11, 12, '173,349,287,435');

-- --------------------------------------------------------

--
-- Structure de la table `matiere`
--

CREATE TABLE `matiere` (
  `idmat` int(11) NOT NULL,
  `nommat` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `matiere`
--

INSERT INTO `matiere` (`idmat`, `nommat`) VALUES
(1, 'Math'),
(2, 'SVT'),
(3, 'Physique'),
(4, 'Informatique');

-- --------------------------------------------------------

--
-- Structure de la table `niveau`
--

CREATE TABLE `niveau` (
  `niveau` int(11) NOT NULL,
  `descriptionniveau` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `niveau`
--

INSERT INTO `niveau` (`niveau`, `descriptionniveau`) VALUES
(1, 'Le premier niveau'),
(2, 'Le second niveau');

-- --------------------------------------------------------

--
-- Structure de la table `objet`
--

CREATE TABLE `objet` (
  `idobjet` int(11) NOT NULL,
  `type` varchar(200) DEFAULT NULL,
  `nomobjet` varchar(500) DEFAULT NULL,
  `imageobjet` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `objet`
--

INSERT INTO `objet` (`idobjet`, `type`, `nomobjet`, `imageobjet`) VALUES
(1, 'contenu', 'Table ronde', NULL),
(2, 'contenant', 'Matelas en mauvais état', NULL),
(3, 'contenant', 'Bureau', 'pics2019011781839.jpg'),
(4, 'contenu', 'Tiroir', 'pics2019011781906.jpg'),
(5, 'objet', 'ordinateur', 'pics2019011781924.jpg'),
(6, 'objet', 'cookie', 'pics2019011782032.jpg'),
(7, 'objet', 'chapeau', 'pics2019011782048.jpg'),
(8, 'objet', 'livre', 'pics2019011782100.jpg'),
(9, 'contenu', 'boite', 'pics2019011782117.jpg'),
(10, 'contenant', 'Armoir', 'pics2019011782245.jpg'),
(11, 'contenu', 'etagere', 'pics2019011782338.jpg'),
(12, 'objet', 'Debian', 'pics2019011725706.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `personne`
--

CREATE TABLE `personne` (
  `idpers` int(11) NOT NULL,
  `nom` varchar(100) DEFAULT NULL,
  `prenom` varchar(100) DEFAULT NULL,
  `mdp` varchar(200) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `personne`
--

INSERT INTO `personne` (`idpers`, `nom`, `prenom`, `mdp`, `email`) VALUES
(1, 'Aubry', 'Erwan', '59c826fc854197cbd4d1083bce8fc00d0761e8b3', 'geekyready@gmail.com'),
(2, 'Kalla', 'Caroline', 'a020e408cf13b0b1e054fd9168ee17cb291300f0', 'caroline.kalla@univ-orleans.fr'),
(3, 'Jurè', 'Marion', '9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684', 'marion.jure@etu.univ-orleans.fr'),
(4, 'Cizeau', 'Valentin', '9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684', 'valentin.cizeau@etu.univ-orleans.fr'),
(5, 'Delaunay', 'Julien', '9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684', 'julien.delaunay@etu.univ-orleans.fr'),
(6, 'Mercier', 'Luka', '9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684', 'luka.mercier@etu.univ-orleans.fr'),
(7, 'Chateigner', 'Corentin', '9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684', 'corentin.chateinger@etu.univ-orleans.fr'),
(8, 'Cleuziou', 'Guillaume', '9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684', 'guillaume.cleuziou@univ-orleans.fr'),
(9, 'test', 'test', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'test@gmail.com');

-- --------------------------------------------------------

--
-- Structure de la table `professeur`
--

CREATE TABLE `professeur` (
  `idprof` int(11) NOT NULL,
  `idpers` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `professeur`
--

INSERT INTO `professeur` (`idprof`, `idpers`) VALUES
(11452, 2),
(2221, 8),
(1, 9);

-- --------------------------------------------------------

--
-- Structure de la table `salle`
--

CREATE TABLE `salle` (
  `idsalle` int(11) NOT NULL,
  `nomsalle` varchar(500) DEFAULT NULL,
  `descriptsalle` text,
  `imagesalle` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `salle`
--

INSERT INTO `salle` (`idsalle`, `nomsalle`, `descriptsalle`, `imagesalle`) VALUES
(1, 'La chambre', 'Une chambre', NULL),
(2, 'Salle de jeu', 'Salle de jeu video', 'pics2019011785610.jpg');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `appartient`
--
ALTER TABLE `appartient`
  ADD PRIMARY KEY (`idgr`,`netu`),
  ADD KEY `netu` (`netu`);

--
-- Index pour la table `cadena`
--
ALTER TABLE `cadena`
  ADD PRIMARY KEY (`idcadena`);

--
-- Index pour la table `composer_de`
--
ALTER TABLE `composer_de`
  ADD PRIMARY KEY (`idsalle`,`iddev`),
  ADD KEY `iddev` (`iddev`),
  ADD KEY `idsalle` (`idsalle`);

--
-- Index pour la table `contient`
--
ALTER TABLE `contient`
  ADD PRIMARY KEY (`iddm`,`idcadena`,`idsalle`),
  ADD KEY `idsalle` (`idsalle`),
  ADD KEY `idcadena` (`idcadena`);

--
-- Index pour la table `devoir`
--
ALTER TABLE `devoir`
  ADD PRIMARY KEY (`iddev`),
  ADD KEY `idmat` (`idmat`),
  ADD KEY `idprof` (`idprof`);

--
-- Index pour la table `dm`
--
ALTER TABLE `dm`
  ADD PRIMARY KEY (`iddm`),
  ADD KEY `iddev` (`iddev`),
  ADD KEY `idgr` (`idgr`);

--
-- Index pour la table `eleve`
--
ALTER TABLE `eleve`
  ADD PRIMARY KEY (`netu`),
  ADD KEY `idpers` (`idpers`);

--
-- Index pour la table `enigme`
--
ALTER TABLE `enigme`
  ADD PRIMARY KEY (`idenigme`),
  ADD KEY `niveau` (`niveau`),
  ADD KEY `idmat` (`idmat`);

--
-- Index pour la table `enseigne`
--
ALTER TABLE `enseigne`
  ADD PRIMARY KEY (`idprof`,`idmat`),
  ADD KEY `idmat` (`idmat`);

--
-- Index pour la table `est_dans`
--
ALTER TABLE `est_dans`
  ADD PRIMARY KEY (`idenigme`,`iddev`),
  ADD KEY `iddev` (`iddev`);

--
-- Index pour la table `est_lier_a`
--
ALTER TABLE `est_lier_a`
  ADD PRIMARY KEY (`iddm`,`idobjet`,`idenigme`),
  ADD KEY `idenigme` (`idenigme`),
  ADD KEY `idobjet` (`idobjet`),
  ADD KEY `EST_LIER_A_ibfk_4` (`idsalle`);

--
-- Index pour la table `groupe`
--
ALTER TABLE `groupe`
  ADD PRIMARY KEY (`idgr`);

--
-- Index pour la table `is_dans`
--
ALTER TABLE `is_dans`
  ADD PRIMARY KEY (`idobjet`,`idsalle`),
  ADD KEY `idsalle` (`idsalle`);

--
-- Index pour la table `is_dans_objet`
--
ALTER TABLE `is_dans_objet`
  ADD PRIMARY KEY (`idobjetSuperieur`,`idobjetInferieur`),
  ADD KEY `idobjetInferieur` (`idobjetInferieur`);

--
-- Index pour la table `matiere`
--
ALTER TABLE `matiere`
  ADD PRIMARY KEY (`idmat`);

--
-- Index pour la table `niveau`
--
ALTER TABLE `niveau`
  ADD PRIMARY KEY (`niveau`);

--
-- Index pour la table `objet`
--
ALTER TABLE `objet`
  ADD PRIMARY KEY (`idobjet`);

--
-- Index pour la table `personne`
--
ALTER TABLE `personne`
  ADD PRIMARY KEY (`idpers`);

--
-- Index pour la table `professeur`
--
ALTER TABLE `professeur`
  ADD PRIMARY KEY (`idprof`),
  ADD KEY `idpers` (`idpers`);

--
-- Index pour la table `salle`
--
ALTER TABLE `salle`
  ADD PRIMARY KEY (`idsalle`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `cadena`
--
ALTER TABLE `cadena`
  MODIFY `idcadena` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT pour la table `composer_de`
--
ALTER TABLE `composer_de`
  MODIFY `idsalle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `devoir`
--
ALTER TABLE `devoir`
  MODIFY `iddev` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `dm`
--
ALTER TABLE `dm`
  MODIFY `iddm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `eleve`
--
ALTER TABLE `eleve`
  MODIFY `netu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2171928;

--
-- AUTO_INCREMENT pour la table `enigme`
--
ALTER TABLE `enigme`
  MODIFY `idenigme` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `groupe`
--
ALTER TABLE `groupe`
  MODIFY `idgr` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT pour la table `matiere`
--
ALTER TABLE `matiere`
  MODIFY `idmat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `objet`
--
ALTER TABLE `objet`
  MODIFY `idobjet` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pour la table `personne`
--
ALTER TABLE `personne`
  MODIFY `idpers` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `professeur`
--
ALTER TABLE `professeur`
  MODIFY `idprof` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11453;

--
-- AUTO_INCREMENT pour la table `salle`
--
ALTER TABLE `salle`
  MODIFY `idsalle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `appartient`
--
ALTER TABLE `appartient`
  ADD CONSTRAINT `APPARTIENT_ibfk_1` FOREIGN KEY (`netu`) REFERENCES `eleve` (`netu`),
  ADD CONSTRAINT `APPARTIENT_ibfk_2` FOREIGN KEY (`idgr`) REFERENCES `groupe` (`idgr`);

--
-- Contraintes pour la table `composer_de`
--
ALTER TABLE `composer_de`
  ADD CONSTRAINT `COMPOSER_DE_ibfk_1` FOREIGN KEY (`iddev`) REFERENCES `devoir` (`iddev`),
  ADD CONSTRAINT `COMPOSER_DE_ibfk_2` FOREIGN KEY (`idsalle`) REFERENCES `salle` (`idsalle`);

--
-- Contraintes pour la table `contient`
--
ALTER TABLE `contient`
  ADD CONSTRAINT `CONTIENT_ibfk_1` FOREIGN KEY (`idsalle`) REFERENCES `salle` (`idsalle`),
  ADD CONSTRAINT `CONTIENT_ibfk_2` FOREIGN KEY (`idcadena`) REFERENCES `cadena` (`idcadena`),
  ADD CONSTRAINT `CONTIENT_ibfk_3` FOREIGN KEY (`iddm`) REFERENCES `dm` (`iddm`);

--
-- Contraintes pour la table `devoir`
--
ALTER TABLE `devoir`
  ADD CONSTRAINT `DEVOIR_ibfk_1` FOREIGN KEY (`idmat`) REFERENCES `matiere` (`idmat`),
  ADD CONSTRAINT `DEVOIR_ibfk_2` FOREIGN KEY (`idprof`) REFERENCES `professeur` (`idprof`);

--
-- Contraintes pour la table `dm`
--
ALTER TABLE `dm`
  ADD CONSTRAINT `DM_ibfk_1` FOREIGN KEY (`iddev`) REFERENCES `devoir` (`iddev`),
  ADD CONSTRAINT `DM_ibfk_2` FOREIGN KEY (`idgr`) REFERENCES `groupe` (`idgr`);

--
-- Contraintes pour la table `eleve`
--
ALTER TABLE `eleve`
  ADD CONSTRAINT `ELEVE_ibfk_1` FOREIGN KEY (`idpers`) REFERENCES `personne` (`idpers`);

--
-- Contraintes pour la table `enigme`
--
ALTER TABLE `enigme`
  ADD CONSTRAINT `ENIGME_ibfk_1` FOREIGN KEY (`niveau`) REFERENCES `niveau` (`niveau`),
  ADD CONSTRAINT `ENIGME_ibfk_2` FOREIGN KEY (`idmat`) REFERENCES `matiere` (`idmat`);

--
-- Contraintes pour la table `enseigne`
--
ALTER TABLE `enseigne`
  ADD CONSTRAINT `ENSEIGNE_ibfk_1` FOREIGN KEY (`idmat`) REFERENCES `matiere` (`idmat`),
  ADD CONSTRAINT `ENSEIGNE_ibfk_2` FOREIGN KEY (`idprof`) REFERENCES `professeur` (`idprof`);

--
-- Contraintes pour la table `est_dans`
--
ALTER TABLE `est_dans`
  ADD CONSTRAINT `EST_DANS_ibfk_1` FOREIGN KEY (`iddev`) REFERENCES `devoir` (`iddev`),
  ADD CONSTRAINT `EST_DANS_ibfk_2` FOREIGN KEY (`idenigme`) REFERENCES `enigme` (`idenigme`);

--
-- Contraintes pour la table `est_lier_a`
--
ALTER TABLE `est_lier_a`
  ADD CONSTRAINT `EST_LIER_A_ibfk_1` FOREIGN KEY (`idenigme`) REFERENCES `enigme` (`idenigme`),
  ADD CONSTRAINT `EST_LIER_A_ibfk_2` FOREIGN KEY (`idobjet`) REFERENCES `objet` (`idobjet`),
  ADD CONSTRAINT `EST_LIER_A_ibfk_3` FOREIGN KEY (`iddm`) REFERENCES `dm` (`iddm`),
  ADD CONSTRAINT `EST_LIER_A_ibfk_4` FOREIGN KEY (`idsalle`) REFERENCES `salle` (`idsalle`);

--
-- Contraintes pour la table `is_dans`
--
ALTER TABLE `is_dans`
  ADD CONSTRAINT `IS_DANS_ibfk_1` FOREIGN KEY (`idobjet`) REFERENCES `objet` (`idobjet`),
  ADD CONSTRAINT `IS_DANS_ibfk_2` FOREIGN KEY (`idsalle`) REFERENCES `salle` (`idsalle`);

--
-- Contraintes pour la table `is_dans_objet`
--
ALTER TABLE `is_dans_objet`
  ADD CONSTRAINT `IS_DANS_OBJET_ibfk_1` FOREIGN KEY (`idobjetSuperieur`) REFERENCES `objet` (`idobjet`),
  ADD CONSTRAINT `IS_DANS_OBJET_ibfk_2` FOREIGN KEY (`idobjetInferieur`) REFERENCES `objet` (`idobjet`);

--
-- Contraintes pour la table `professeur`
--
ALTER TABLE `professeur`
  ADD CONSTRAINT `PROFESSEUR_ibfk_1` FOREIGN KEY (`idpers`) REFERENCES `personne` (`idpers`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
