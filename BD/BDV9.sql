﻿-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: servinfo-mariadb
-- Generation Time: Jan 21, 2019 at 08:58 AM
-- Server version: 10.1.34-MariaDB-0ubuntu0.18.04.1
-- PHP Version: 7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `DBcizeau`
--

DELIMITER $$
--
-- Procedures
--
CREATE PROCEDURE IF NOT EXISTS `afficher` (IN `seed` INT)  BEGIN
  -- Temporary storage
  DROP TABLE IF EXISTS _result;
  CREATE TEMPORARY TABLE _result (idobjet INT PRIMARY KEY);

  -- Seeding
  INSERT INTO _result VALUES (seed);

  -- Iteration
  DROP TABLE IF EXISTS _tmp;
  CREATE TEMPORARY TABLE _tmp LIKE _result;
  REPEAT
    TRUNCATE TABLE _tmp;
    INSERT INTO _tmp SELECT idobjetInferieur  AS idobjet
      FROM _result JOIN IS_DANS_OBJET ON idobjet = idobjetSuperieur;

    INSERT IGNORE INTO _result SELECT idobjet FROM _tmp;
  UNTIL ROW_COUNT() = 0
  END REPEAT;
  DROP TABLE _tmp;
  SELECT * FROM _result natural join OBJET where type='objet';
END$$

CREATE PROCEDURE IF NOT EXISTS `find_parts` (`seed` INT)  BEGIN
  -- Temporary storage
  DROP TABLE IF EXISTS _result;
  CREATE TEMPORARY TABLE _result (node INT PRIMARY KEY);

  -- Seeding
  INSERT INTO _result VALUES (seed);

  -- Iteration
  DROP TABLE IF EXISTS _tmp;
  CREATE TEMPORARY TABLE _tmp LIKE _result;
  REPEAT
    TRUNCATE TABLE _tmp;
    INSERT INTO _tmp SELECT idobjetInferieur  AS node
      FROM _result JOIN IS_DANS_OBJET ON node = idobjetSuperieur;

    INSERT IGNORE INTO _result SELECT node FROM _tmp;
  UNTIL ROW_COUNT() = 0
  END REPEAT;
  DROP TABLE _tmp;
  SELECT * FROM _result;
END$$

DELIMITER ;
-- --------------------------------------------------------

--
-- DROP for tables
--

DROP TABLE IF EXISTS APPARTIENT;
DROP TABLE IF EXISTS COMPOSER_DE;
DROP TABLE IF EXISTS CONTIENT;
DROP TABLE IF EXISTS CADENA;
DROP TABLE IF EXISTS ELEVE;
DROP TABLE IF EXISTS ENSEIGNE;
DROP TABLE IF EXISTS EST_DANS;
DROP TABLE IF EXISTS EST_LIER_A;
DROP TABLE IF EXISTS IS_DANS;
DROP TABLE IF EXISTS IS_DANS_OBJET;
DROP TABLE IF EXISTS DM;
DROP TABLE IF EXISTS DEVOIR;
DROP TABLE IF EXISTS ENIGME;
DROP TABLE IF EXISTS GROUPE;
DROP TABLE IF EXISTS MATIERE;
DROP TABLE IF EXISTS NIVEAU;
DROP TABLE IF EXISTS OBJET;
DROP TABLE IF EXISTS PROFESSEUR;
DROP TABLE IF EXISTS PERSONNE;
DROP TABLE IF EXISTS SALLE;

-- --------------------------------------------------------

--
-- Create Table
--

CREATE TABLE `APPARTIENT` (
  `idgr` int(11) NOT NULL,
  `netu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `CADENA` (
  `idcadena` int(11) NOT NULL,
  `intitule` text,
  `niveauc` int(11) DEFAULT NULL,
  `etatc` tinyint(1) DEFAULT NULL,
  `nbessai` int(11) DEFAULT NULL,
  `nbessaimax` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `COMPOSER_DE` (
  `idsalle` int(11) NOT NULL,
  `iddev` int(11) NOT NULL,
  `ordresalle` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `CONTIENT` (
  `iddm` int(11) NOT NULL,
  `idcadena` int(11) NOT NULL,
  `idsalle` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `DEVOIR` (
  `iddev` int(11) NOT NULL,
  `statut` varchar(50) DEFAULT NULL,
  `nomDev` varchar(50) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `nbpersonnegroupe` int(11) DEFAULT NULL,
  `nbgroupe` int(11) DEFAULT NULL,
  `idprof` int(11) DEFAULT NULL,
  `idmat` int(11) DEFAULT NULL,
  `maxPoint` int(5) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `DM` (
  `iddm` int(11) NOT NULL,
  `note` int(5) DEFAULT '0',
  `idgr` int(11) DEFAULT NULL,
  `iddev` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ELEVE` (
  `netu` int(11) NOT NULL,
  `classe` varchar(20) DEFAULT NULL,
  `datec` varchar(10) DEFAULT NULL,
  `idpers` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ENIGME` (
  `idenigme` int(11) NOT NULL,
  `nomEnigme` varchar(30) DEFAULT NULL,
  `solution` text,
  `niveau` int(11) DEFAULT NULL,
  `maxpoint` int(11) DEFAULT '0',
  `question` text,
  `idmat` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ENSEIGNE` (
  `idprof` int(11) NOT NULL,
  `idmat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `EST_DANS` (
  `idenigme` int(11) NOT NULL,
  `iddev` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `EST_LIER_A` (
  `iddm` int(11) NOT NULL,
  `idobjet` int(11) NOT NULL,
  `idenigme` int(11) NOT NULL,
  `datetrouve` date DEFAULT NULL,
  `idsalle` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `GROUPE` (
  `idgr` int(11) NOT NULL,
  `nomGroupe` varchar(100) DEFAULT NULL,
  `idpush` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `IS_DANS` (
  `idsalle` int(11) NOT NULL,
  `idobjet` int(11) NOT NULL,
  `coordobjet` varchar(200) DEFAULT NULL,
  `tailleobjet` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `IS_DANS_OBJET` (
  `idobjetSuperieur` int(11) NOT NULL,
  `idobjetInferieur` int(11) NOT NULL,
  `coordobjet2` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `MATIERE` (
  `idmat` int(11) NOT NULL,
  `nommat` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `NIVEAU` (
  `niveau` int(11) NOT NULL,
  `descriptionniveau` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `OBJET` (
  `idobjet` int(11) NOT NULL,
  `type` varchar(200) DEFAULT NULL,
  `nomobjet` varchar(500) DEFAULT NULL,
  `imageobjet` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `PERSONNE` (
  `idpers` int(11) NOT NULL,
  `nom` varchar(100) DEFAULT NULL,
  `prenom` varchar(100) DEFAULT NULL,
  `mdp` varchar(200) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `PROFESSEUR` (
  `idprof` int(11) NOT NULL,
  `idpers` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `SALLE` (
  `idsalle` int(11) NOT NULL,
  `nomsalle` varchar(500) DEFAULT NULL,
  `descriptsalle` text,
  `imagesalle` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- INSERT into tables
--

INSERT INTO `APPARTIENT` (`idgr`, `netu`) VALUES
(1, 100),
(1, 45646),
(1, 54987),
(1, 265689),
(1, 458864),
(1, 564564),
(1, 2171927),
(2, 100),
(23, 45646),
(23, 2171927),
(24, 54987),
(24, 265689),
(25, 458864),
(25, 564564),
(26, 100),
(26, 45646),
(26, 2171927),
(27, 54987),
(27, 265689),
(28, 458864),
(28, 564564),
(29, 45646),
(29, 54987),
(30, 265689),
(30, 458864),
(31, 100),
(31, 45646),
(31, 2171927),
(32, 54987),
(32, 265689),
(33, 458864),
(33, 564564),
(34, 1),
(34, 2),
(34, 3),
(35, 100),
(35, 45646),
(35, 54987),
(35, 2171927),
(36, 265689),
(36, 458864),
(36, 564564);

INSERT INTO `CADENA` (`idcadena`, `intitule`, `niveauc`, `etatc`, `nbessai`, `nbessaimax`) VALUES
(1, 'Première pièce ', 5, 0, 0, 10),
(8, NULL, NULL, 0, 0, 10),
(9, NULL, NULL, 0, 0, 10),
(10, NULL, NULL, 0, 0, 10),
(11, NULL, NULL, 0, 0, 10),
(36, NULL, NULL, 0, 0, 10),
(37, NULL, NULL, 0, 0, 10),
(38, NULL, NULL, 0, 0, 10),
(39, NULL, NULL, 0, 0, 50),
(40, NULL, NULL, 0, 0, 50),
(41, NULL, NULL, 0, 0, 50),
(42, NULL, NULL, 0, 0, 50),
(43, NULL, NULL, 0, 0, 50),
(44, NULL, NULL, 0, 0, 50),
(45, NULL, NULL, 0, 0, 50),
(46, NULL, NULL, 0, 0, 50),
(47, NULL, NULL, 0, 0, 50),
(48, NULL, NULL, 0, 0, 50),
(49, NULL, NULL, 1, 0, 50),
(50, NULL, NULL, 1, 0, 50),
(51, NULL, NULL, 0, 0, 20),
(52, NULL, NULL, 0, 0, 20),
(53, NULL, NULL, 0, 0, 20),
(54, NULL, NULL, 0, 0, 20),
(55, NULL, NULL, 0, 0, 20),
(56, NULL, NULL, 0, 0, 20),
(57, NULL, NULL, 0, 0, 20),
(58, NULL, NULL, 0, 0, 20),
(59, NULL, NULL, 0, 0, 10),
(60, NULL, NULL, 0, 0, 10),
(61, NULL, NULL, 0, 0, 10),
(62, NULL, NULL, 0, 0, 10),
(63, NULL, NULL, 0, 0, 50),
(64, NULL, NULL, 0, 0, 50),
(65, NULL, NULL, 0, 0, 50),
(66, NULL, NULL, 0, 0, 50),
(67, NULL, NULL, 0, 0, 50),
(68, NULL, NULL, 0, 0, 50),
(69, NULL, NULL, 0, 0, 50),
(70, NULL, NULL, 0, 0, 50),
(71, NULL, NULL, 0, 0, 50),
(72, NULL, NULL, 0, 0, 50),
(73, NULL, NULL, 0, 0, 50),
(74, NULL, NULL, 0, 0, 50),
(75, NULL, NULL, 0, 0, 50),
(76, NULL, NULL, 0, 0, 50),
(77, NULL, NULL, 0, 0, 50),
(78, NULL, NULL, 0, 0, 50);

INSERT INTO `COMPOSER_DE` (`idsalle`, `iddev`, `ordresalle`) VALUES
(1, 1, '1'),
(1, 7, '1'),
(1, 9, NULL),
(1, 10, '1'),
(1, 11, '1'),
(1, 12, '1'),
(1, 13, '1'),
(1, 14, '1'),
(1, 15, '1'),
(1, 16, '1'),
(2, 1, '2'),
(2, 7, NULL),
(2, 9, NULL),
(2, 10, '2'),
(2, 11, '2'),
(2, 12, '2'),
(2, 13, '2'),
(2, 14, '2'),
(2, 15, '2'),
(2, 16, '2');


INSERT INTO `CONTIENT` (`iddm`, `idcadena`, `idsalle`) VALUES
(1, 1, 1),
(1, 8, 1),
(1, 9, 1),
(1, 10, 1),
(1, 11, 2),
(1, 36, 1),
(1, 37, 1),
(1, 38, 2),
(6, 39, 1),
(6, 40, 2),
(7, 41, 1),
(7, 42, 2),
(8, 43, 1),
(8, 44, 2),
(9, 45, 1),
(9, 46, 2),
(10, 47, 1),
(10, 48, 2),
(11, 49, 1),
(11, 50, 2),
(12, 51, 1),
(12, 52, 2),
(13, 53, 1),
(13, 54, 2),
(14, 55, 1),
(14, 56, 2),
(15, 57, 1),
(15, 58, 2),
(16, 59, 1),
(16, 60, 2),
(17, 61, 1),
(17, 62, 2),
(18, 63, 1),
(18, 64, 2),
(19, 65, 1),
(19, 66, 2),
(20, 67, 1),
(20, 68, 2),
(21, 69, 1),
(21, 70, 2),
(22, 71, 1),
(22, 72, 2),
(23, 73, 1),
(23, 74, 2),
(24, 75, 1),
(24, 76, 2),
(25, 77, 1),
(25, 78, 2);

INSERT INTO `DEVOIR` (`iddev`, `statut`, `nomDev`, `date`, `nbpersonnegroupe`, `nbgroupe`, `idprof`, `idmat`, `maxPoint`) VALUES
(1, 'En cours', 'dev1', '2018-12-08', 4, 10, 2221, 1, 500),
(3, 'fermer', NULL, '2018-12-08', 4, 10, 2221, 1, 500),
(4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(7, 'En developpement', 'mpmp', '2019-01-25', 2, 7, 1, 1, 0),
(8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(9, 'En developpement', 'nn', '2019-01-26', NULL, NULL, 1, 1, 0),
(10, 'En developpement', 'ooo', '2019-01-02', NULL, NULL, 1, 1, 0),
(11, 'En cours', 'aaaaa', '2019-01-27', 2, 7, 1, 1, 0),
(12, 'En developpement', 'test1', '2019-04-14', 7, 14, 1, 1, 0),
(13, 'En developpement', 'test 1E', '2019-01-19', 7, 7, 1, 1, 0),
(14, 'En developpement', 'test 2E', '1783-12-02', 2, 4, 1, 1, 0),
(15, 'En developpement', 'test 10', '2019-01-03', 2, 7, 1, 1, 0),
(16, 'En developpement', 'testhhh', '2019-01-25', 3, 10, 1, 1, 0);

INSERT INTO `DM` (`iddm`, `note`, `idgr`, `iddev`) VALUES
(1, 20, 1, 1),
(3, 20, 2, 1),
(4, 20, 3, 3),
(5, 20, 4, 3),
(6, NULL, 23, 7),
(7, NULL, 24, 7),
(8, NULL, 25, 7),
(9, NULL, 26, 11),
(10, NULL, 27, 11),
(11, NULL, 28, 11),
(12, 0, 1, 12),
(13, 0, 2, 12),
(14, 0, 1, 12),
(15, 0, 2, 12),
(16, 0, 1, 13),
(17, 0, 2, 13),
(18, 0, 29, 14),
(19, 0, 30, 14),
(20, 0, 31, 15),
(21, 0, 32, 15),
(22, 0, 33, 15),
(23, 0, 34, 16),
(24, 0, 35, 16),
(25, 0, 36, 16);

INSERT INTO `ELEVE` (`netu`, `classe`, `datec`, `idpers`) VALUES
(1, '2A22', '2018-2019', 10),
(2, '2A22', '2018-2019', 11),
(3, '2A22', '2018-2019', 12),
(100, '2A21', '2018-2019', 9),
(45646, '2A21', '2018-2019', 7),
(54987, '2A21', '2018-2019', 4),
(265689, '2A21', '2018-2019', 5),
(458864, '2A21', '2018-2019', 3),
(564564, '2A21', '2018-2019', 6),
(2171927, '2A21', '2018-2019', 1);


INSERT INTO `ENIGME` (`idenigme`, `nomEnigme`, `solution`, `niveau`, `maxpoint`, `question`, `idmat`) VALUES
(1, 'test', '2 + 2 = 4', 2, 5, '2 + 2 = ?', 1),
(2, 'E2', '2 + 2 = 4', 2, 5, '2 + 2 = ?', 1),
(3, 'E3', '2 + 2 = 4', 2, 5, '2 + 2 = ?', 1),
(4, 'E4', '2 + 2 = 4', 2, 5, '2 + 2 = ?', 1);

INSERT INTO `ENSEIGNE` (`idprof`, `idmat`) VALUES
(2221, 1),
(11452, 4);

INSERT INTO `EST_DANS` (`idenigme`, `iddev`) VALUES
(1, 1),
(1, 7),
(1, 10),
(1, 13),
(1, 15),
(1, 16),
(2, 1),
(2, 7),
(2, 9),
(2, 11),
(2, 12),
(2, 15),
(2, 16),
(3, 1),
(3, 12),
(3, 13),
(3, 14),
(4, 1),
(4, 9),
(4, 10),
(4, 11),
(4, 14);

INSERT INTO `EST_LIER_A` (`iddm`, `idobjet`, `idenigme`, `datetrouve`, `idsalle`) VALUES
(1, 5, 1, NULL, 1),
(1, 5, 2, NULL, 1),
(1, 5, 4, NULL, 2),
(1, 7, 3, NULL, 2),
(6, 12, 1, NULL, 1),
(6, 12, 2, NULL, 2),
(7, 6, 1, NULL, 1),
(7, 12, 2, NULL, 2),
(8, 7, 1, NULL, 1),
(8, 12, 2, NULL, 2),
(9, 5, 2, NULL, 1),
(9, 12, 4, NULL, 2),
(10, 5, 2, NULL, 1),
(10, 12, 4, NULL, 2),
(11, 7, 4, NULL, 1),
(11, 12, 2, NULL, 2),
(12, 7, 2, NULL, 1),
(12, 12, 3, NULL, 2),
(13, 6, 3, NULL, 1),
(13, 12, 2, NULL, 2),
(14, 7, 2, NULL, 1),
(14, 12, 3, NULL, 2),
(15, 6, 2, NULL, 1),
(15, 12, 3, NULL, 2),
(16, 12, 1, NULL, 2),
(16, 12, 3, NULL, 1),
(17, 7, 3, NULL, 1),
(17, 12, 1, NULL, 2),
(18, 5, 4, NULL, 1),
(18, 12, 3, NULL, 2),
(19, 12, 3, NULL, 1),
(19, 12, 4, NULL, 2),
(20, 7, 1, NULL, 1),
(20, 12, 2, NULL, 2),
(21, 6, 2, NULL, 1),
(21, 12, 1, NULL, 2),
(22, 5, 2, NULL, 1),
(22, 12, 1, NULL, 2),
(23, 5, 2, NULL, 1),
(23, 12, 1, NULL, 2),
(24, 6, 1, NULL, 1),
(24, 12, 2, NULL, 2),
(25, 5, 1, NULL, 1),
(25, 12, 2, NULL, 2);

INSERT INTO `GROUPE` (`idgr`, `nomGroupe`, `idpush`) VALUES
(1, 'groupe 1', 1),
(2, 'groupe 2', 1),
(3, 'groupe 2', 1),
(4, 'groupe 2', 1),
(5, 'Groupe 2A21 5', 100),
(6, 'Groupe 2A21 6', 54987),
(7, 'Groupe 2A21 7', 458864),
(8, 'Groupe 2A21 8', 100),
(9, 'Groupe 2A21 9', 54987),
(10, 'Groupe 2A21 10', 458864),
(11, 'Groupe 2A21 11', 100),
(12, 'Groupe 2A21 12', 54987),
(13, 'Groupe 2A21 13', 458864),
(14, 'Groupe 2A21 14', 100),
(15, 'Groupe 2A21 15', 54987),
(16, 'Groupe 2A21 16', 458864),
(17, 'Groupe 2A21 17', 100),
(18, 'Groupe 2A21 18', 54987),
(19, 'Groupe 2A21 19', 458864),
(20, 'Groupe 2A21 20', 100),
(21, 'Groupe 2A21 21', 54987),
(22, 'Groupe 2A21 22', 458864),
(23, 'Groupe 2A21 23', 100),
(24, 'Groupe 2A21 24', 54987),
(25, 'Groupe 2A21 25', 458864),
(26, 'Groupe 2A21 26', 100),
(27, 'Groupe 2A21 27', 54987),
(28, 'Groupe 2A21 28', 458864),
(29, 'Groupe 2A21 29', 45646),
(30, 'Groupe 2A21 30', 265689),
(31, 'Groupe 2A21 31', 100),
(32, 'Groupe 2A21 32', 54987),
(33, 'Groupe 2A21 33', 458864),
(34, 'Groupe 2A22 34', 1),
(35, 'Groupe 2A21 35', 100),
(36, 'Groupe 2A21 36', 265689);

INSERT INTO `IS_DANS` (`idsalle`, `idobjet`, `coordobjet`, `tailleobjet`) VALUES
(1, 2, '43,222,176,322', 1),
(2, 2, '157,93,237,153', 1),
(1, 3, '120,77,205,141', 1),
(1, 10, '133,379,191,423', 1),
(2, 10, '149,250,192,282', 1);

INSERT INTO `IS_DANS_OBJET` (`idobjetSuperieur`, `idobjetInferieur`, `coordobjet2`) VALUES
(3, 4, '54,179,202,290'),
(3, 9, '205,66,302,139'),
(4, 5, '140,164,336,311'),
(4, 6, '39,159,138,233'),
(9, 7, '80,132,337,325'),
(10, 11, '117,79,438,320'),
(11, 12, '173,349,287,435');

INSERT INTO `MATIERE` (`idmat`, `nommat`) VALUES
(1, 'Math'),
(2, 'SVT'),
(3, 'Physique'),
(4, 'Informatique');

INSERT INTO `NIVEAU` (`niveau`, `descriptionniveau`) VALUES
(1, 'Le premier niveau'),
(2, 'Le second niveau');

INSERT INTO `OBJET` (`idobjet`, `type`, `nomobjet`, `imageobjet`) VALUES
(1, 'contenu', 'Table ronde', NULL),
(2, 'contenant', 'Matelas en mauvais état', NULL),
(3, 'contenant', 'Bureau', 'pics2019011781839.jpg'),
(4, 'contenu', 'Tiroir', 'pics2019011781906.jpg'),
(5, 'objet', 'ordinateur', 'pics2019011781924.jpg'),
(6, 'objet', 'cookie', 'pics2019011782032.jpg'),
(7, 'objet', 'chapeau', 'pics2019011782048.jpg'),
(8, 'objet', 'livre', 'pics2019011782100.jpg'),
(9, 'contenu', 'boite', 'pics2019011782117.jpg'),
(10, 'contenant', 'Armoir', 'pics2019011782245.jpg'),
(11, 'contenu', 'etagere', 'pics2019011782338.jpg'),
(12, 'objet', 'Debian', 'pics2019011725706.jpg');

INSERT INTO `PERSONNE` (`idpers`, `nom`, `prenom`, `mdp`, `email`) VALUES
(1, 'Aubry', 'Erwan', '59c826fc854197cbd4d1083bce8fc00d0761e8b3', 'geekyready@gmail.com'),
(2, 'Kalla', 'Caroline', 'a020e408cf13b0b1e054fd9168ee17cb291300f0', 'caroline.kalla@univ-orleans.fr'),
(3, 'Jurè', 'Marion', '9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684', 'marion.jure@etu.univ-orleans.fr'),
(4, 'Cizeau', 'Valentin', '9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684', 'valentin.cizeau@etu.univ-orleans.fr'),
(5, 'Delaunay', 'Julien', '9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684', 'julien.delaunay@etu.univ-orleans.fr'),
(6, 'Mercier', 'Luka', '9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684', 'luka.mercier@etu.univ-orleans.fr'),
(7, 'Chateigner', 'Corentin', '9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684', 'corentin.chateinger@etu.univ-orleans.fr'),
(8, 'Cleuziou', 'Guillaume', '9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684', 'guillaume.cleuziou@univ-orleans.fr'),
(9, 'test', 'test', 'ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff', 'test@gmail.com'),
(10, 'Paul', 'Corentin', '9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684', 'corentin.chateinger@etu.univ-orleans.fr'),
(11, 'Pierre', 'Guillaume', '9d4e1e23bd5b727046a9e3b4b7db57bd8d6ee684', 'guillaume.cleuziou@univ-orleans.fr'),
(12, 'Marie', 'test', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'test@gmail.com');

INSERT INTO `PROFESSEUR` (`idprof`, `idpers`) VALUES
(11452, 2),
(2221, 8),
(1, 9),
(34, 34);

INSERT INTO `SALLE` (`idsalle`, `nomsalle`, `descriptsalle`, `imagesalle`) VALUES
(1, 'La chambre', 'Une chambre', NULL),
(2, 'Salle de jeu', 'Salle de jeu video', 'pics2019011785610.jpg');

-- --------------------------------------------------------

--
-- Key for tables
--
ALTER TABLE `APPARTIENT`
  ADD PRIMARY KEY (`idgr`,`netu`),
  ADD KEY `netu` (`netu`);

ALTER TABLE `CADENA`
  ADD PRIMARY KEY (`idcadena`);

ALTER TABLE `COMPOSER_DE`
  ADD PRIMARY KEY (`idsalle`,`iddev`),
  ADD KEY `iddev` (`iddev`),
  ADD KEY `idsalle` (`idsalle`);

ALTER TABLE `CONTIENT`
  ADD PRIMARY KEY (`iddm`,`idcadena`,`idsalle`),
  ADD KEY `idsalle` (`idsalle`),
  ADD KEY `idcadena` (`idcadena`);

ALTER TABLE `DEVOIR`
  ADD PRIMARY KEY (`iddev`),
  ADD KEY `idmat` (`idmat`),
  ADD KEY `idprof` (`idprof`);

ALTER TABLE `DM`
  ADD PRIMARY KEY (`iddm`),
  ADD KEY `iddev` (`iddev`),
  ADD KEY `idgr` (`idgr`);

ALTER TABLE `ELEVE`
  ADD PRIMARY KEY (`netu`),
  ADD KEY `idpers` (`idpers`);

ALTER TABLE `ENIGME`
  ADD PRIMARY KEY (`idenigme`),
  ADD KEY `niveau` (`niveau`),
  ADD KEY `idmat` (`idmat`);

ALTER TABLE `ENSEIGNE`
  ADD PRIMARY KEY (`idprof`,`idmat`),
  ADD KEY `idmat` (`idmat`);

ALTER TABLE `EST_DANS`
  ADD PRIMARY KEY (`idenigme`,`iddev`),
  ADD KEY `iddev` (`iddev`);

ALTER TABLE `EST_LIER_A`
  ADD PRIMARY KEY (`iddm`,`idobjet`,`idenigme`),
  ADD KEY `idenigme` (`idenigme`),
  ADD KEY `idobjet` (`idobjet`),
  ADD KEY `EST_LIER_A_ibfk_4` (`idsalle`);

ALTER TABLE `GROUPE`
  ADD PRIMARY KEY (`idgr`);

ALTER TABLE `IS_DANS`
  ADD PRIMARY KEY (`idobjet`,`idsalle`),
  ADD KEY `idsalle` (`idsalle`);

ALTER TABLE `IS_DANS_OBJET`
  ADD PRIMARY KEY (`idobjetSuperieur`,`idobjetInferieur`),
  ADD KEY `idobjetInferieur` (`idobjetInferieur`);

ALTER TABLE `MATIERE`
  ADD PRIMARY KEY (`idmat`);

ALTER TABLE `NIVEAU`
  ADD PRIMARY KEY (`niveau`);

ALTER TABLE `OBJET`
  ADD PRIMARY KEY (`idobjet`);

ALTER TABLE `PERSONNE`
  ADD PRIMARY KEY (`idpers`);

ALTER TABLE `PROFESSEUR`
  ADD PRIMARY KEY (`idprof`),
  ADD KEY `idpers` (`idpers`);

ALTER TABLE `SALLE`
  ADD PRIMARY KEY (`idsalle`);


-- --------------------------------------------------------

--
-- AUTO_INCREMENT for tables
--

ALTER TABLE `CADENA`
  MODIFY `idcadena` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

ALTER TABLE `COMPOSER_DE`
  MODIFY `idsalle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

ALTER TABLE `DEVOIR`
  MODIFY `iddev` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

ALTER TABLE `DM`
  MODIFY `iddm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

ALTER TABLE `ELEVE`
  MODIFY `netu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2171928;

ALTER TABLE `ENIGME`
  MODIFY `idenigme` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

ALTER TABLE `GROUPE`
  MODIFY `idgr` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

ALTER TABLE `MATIERE`
  MODIFY `idmat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

ALTER TABLE `OBJET`
  MODIFY `idobjet` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

ALTER TABLE `PERSONNE`
  MODIFY `idpers` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

ALTER TABLE `PROFESSEUR`
  MODIFY `idprof` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11453;

ALTER TABLE `SALLE`
  MODIFY `idsalle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

-- --------------------------------------------------------

--
-- CONSTRAINT for tables
--

ALTER TABLE `APPARTIENT`
  ADD CONSTRAINT `APPARTIENT_ibfk_1` FOREIGN KEY (`netu`) REFERENCES `ELEVE` (`netu`),
  ADD CONSTRAINT `APPARTIENT_ibfk_2` FOREIGN KEY (`idgr`) REFERENCES `GROUPE` (`idgr`);

ALTER TABLE `COMPOSER_DE`
  ADD CONSTRAINT `COMPOSER_DE_ibfk_1` FOREIGN KEY (`iddev`) REFERENCES `DEVOIR` (`iddev`),
  ADD CONSTRAINT `COMPOSER_DE_ibfk_2` FOREIGN KEY (`idsalle`) REFERENCES `SALLE` (`idsalle`);

ALTER TABLE `CONTIENT`
  ADD CONSTRAINT `CONTIENT_ibfk_1` FOREIGN KEY (`idsalle`) REFERENCES `SALLE` (`idsalle`),
  ADD CONSTRAINT `CONTIENT_ibfk_2` FOREIGN KEY (`idcadena`) REFERENCES `CADENA` (`idcadena`),
  ADD CONSTRAINT `CONTIENT_ibfk_3` FOREIGN KEY (`iddm`) REFERENCES `DM` (`iddm`);

ALTER TABLE `DEVOIR`
  ADD CONSTRAINT `DEVOIR_ibfk_1` FOREIGN KEY (`idmat`) REFERENCES `MATIERE` (`idmat`),
  ADD CONSTRAINT `DEVOIR_ibfk_2` FOREIGN KEY (`idprof`) REFERENCES `PROFESSEUR` (`idprof`);

ALTER TABLE `DM`
  ADD CONSTRAINT `DM_ibfk_1` FOREIGN KEY (`iddev`) REFERENCES `DEVOIR` (`iddev`),
  ADD CONSTRAINT `DM_ibfk_2` FOREIGN KEY (`idgr`) REFERENCES `GROUPE` (`idgr`);

ALTER TABLE `ELEVE`
  ADD CONSTRAINT `ELEVE_ibfk_1` FOREIGN KEY (`idpers`) REFERENCES `PERSONNE` (`idpers`);

ALTER TABLE `ENIGME`
  ADD CONSTRAINT `ENIGME_ibfk_1` FOREIGN KEY (`niveau`) REFERENCES `NIVEAU` (`niveau`),
  ADD CONSTRAINT `ENIGME_ibfk_2` FOREIGN KEY (`idmat`) REFERENCES `MATIERE` (`idmat`);

ALTER TABLE `ENSEIGNE`
  ADD CONSTRAINT `ENSEIGNE_ibfk_1` FOREIGN KEY (`idmat`) REFERENCES `MATIERE` (`idmat`),
  ADD CONSTRAINT `ENSEIGNE_ibfk_2` FOREIGN KEY (`idprof`) REFERENCES `PROFESSEUR` (`idprof`);

ALTER TABLE `EST_DANS`
  ADD CONSTRAINT `EST_DANS_ibfk_1` FOREIGN KEY (`iddev`) REFERENCES `DEVOIR` (`iddev`),
  ADD CONSTRAINT `EST_DANS_ibfk_2` FOREIGN KEY (`idenigme`) REFERENCES `ENIGME` (`idenigme`);

ALTER TABLE `EST_LIER_A`
  ADD CONSTRAINT `EST_LIER_A_ibfk_1` FOREIGN KEY (`idenigme`) REFERENCES `ENIGME` (`idenigme`),
  ADD CONSTRAINT `EST_LIER_A_ibfk_2` FOREIGN KEY (`idobjet`) REFERENCES `OBJET` (`idobjet`),
  ADD CONSTRAINT `EST_LIER_A_ibfk_3` FOREIGN KEY (`iddm`) REFERENCES `DM` (`iddm`),
  ADD CONSTRAINT `EST_LIER_A_ibfk_4` FOREIGN KEY (`idsalle`) REFERENCES `SALLE` (`idsalle`);

ALTER TABLE `IS_DANS`
  ADD CONSTRAINT `IS_DANS_ibfk_1` FOREIGN KEY (`idobjet`) REFERENCES `OBJET` (`idobjet`),
  ADD CONSTRAINT `IS_DANS_ibfk_2` FOREIGN KEY (`idsalle`) REFERENCES `SALLE` (`idsalle`);

ALTER TABLE `IS_DANS_OBJET`
  ADD CONSTRAINT `IS_DANS_OBJET_ibfk_1` FOREIGN KEY (`idobjetSuperieur`) REFERENCES `OBJET` (`idobjet`),
  ADD CONSTRAINT `IS_DANS_OBJET_ibfk_2` FOREIGN KEY (`idobjetInferieur`) REFERENCES `OBJET` (`idobjet`);

ALTER TABLE `PROFESSEUR`
  ADD CONSTRAINT `PROFESSEUR_ibfk_1` FOREIGN KEY (`idpers`) REFERENCES `PERSONNE` (`idpers`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
