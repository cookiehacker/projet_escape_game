$(document).ready(function () {
    $('#room').imgAreaSelect({ aspectRatio: '4:3', handles: true });
  });

  $(document).ready(function () {
    $('#room').imgAreaSelect({
        onSelectEnd: function (img, selection) {
            $('input[name="form[x1]"]').val(selection.x1);
            $('input[name="form[y1]"]').val(selection.y1);
            $('input[name="form[x2]"]').val(selection.x2);
            $('input[name="form[y2]"]').val(selection.y2);            
        }
    });
});