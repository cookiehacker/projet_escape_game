<?php

namespace App\Repository;

use App\Entity\Salle;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class SalleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Salle::class);
    }

    /**
     * Suppression d'une salle
     */
    public function SupprimerSalleRequet($idSalle): void
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        DELETE 
        FROM SALLE 
        WHERE idsalle = :idsal 
            and :idsal not in(select idsalle FROM IS_DANS)
            and :idsal not in(select idsalle FROM CONTIENT)
            and :idsal not in(select idsalle FROM COMPOSER_DE)
            ;
        ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(['idsal' => $idSalle]);
    }

    /**
     * Trouver les objets inferieur à cette objet
     */
    public function getObjet($idobjet): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        CALL afficher(:idobjet);
            ';
        
        $stmt = $conn->prepare($sql);
        $stmt->execute(['idobjet' => intval($idobjet)]);

        return $stmt->fetchAll();
    }

    public function getRoomByDM($idDm): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT idsalle 
        FROM SALLE
        WHERE idsalle in 
        (
            SELECT idsalle 
            FROM COMPOSER_DE NATURAL JOIN CONTIENT NATURAL JOIN CADENA 
            WHERE etatc=0 and iddm=:dm
            ORDER BY ordresalle 
        )
        ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(["dm" => $idDm]);
        $s = $stmt->fetchAll();
        if (sizeof($s) > 0)
        {
            $sql = '
            SELECT idsalle, imagesalle, coordobjet, idobjet, type
            FROM SALLE NATURAL JOIN IS_DANS NATURAL JOIN OBJET
            WHERE idsalle=:ids
            ';
            $stmt = $conn->prepare($sql);
            $stmt->execute(["ids" => $s[0]["idsalle"]]);
            return $stmt->fetchAll();
        }
        return [];
    }

    public function getNbSalle($idDm)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT COUNT(idsalle) as nbSalle, idsalle 
        FROM SALLE
        WHERE idsalle in 
        (
            SELECT idsalle 
            FROM COMPOSER_DE NATURAL JOIN CONTIENT NATURAL JOIN CADENA 
            WHERE etatc=0 and iddm=:dm
            ORDER BY ordresalle 
        )
        ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(["dm" => $idDm]);
        return $stmt->fetchAll()[0]['nbSalle'];
    }

    public function getObjContenant($idSalle)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT coordobjet, nomobjet
        FROM IS_DANS NATURAL JOIN OBJET
        WHERE idsalle = :salle
        ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(["salle" => $idSalle]);
        return $stmt->fetchAll();
    }

    public function autorisationPlacerCadena($idSalle) : bool
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql= "
        SELECT COUNT(idobjet) AS nbCadena 
        FROM IS_DANS NATURAL JOIN OBJET 
        WHERE type='cadena' AND idsalle = :salle
        ";

        $stmt = $conn->prepare($sql);
        $stmt->execute(["salle" => $idSalle]);
        return $stmt->fetchAll()[0]['nbCadena'] == 0;
    }
    
    public function supprimerCadena($idSalle) : void
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "
        DELETE i 
        FROM IS_DANS i INNER JOIN OBJET o 
        ON i.idobjet = o.idobjet  
        WHERE o.type = 'cadena' AND i.idsalle = :salle
        ";

        $stmt = $conn->prepare($sql);
        $stmt->execute(["salle" => $idSalle]);
    }
}
