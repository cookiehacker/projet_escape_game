<?php

namespace App\Repository;

use App\Entity\Objet;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;


class ObjetRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Objet::class);
    }

    /**
     * Fonction permettant l'insertion d'un objet dans une salle via la table IS_DANS
     * 
     * Attributs :
     *  - idSalle : l'ID de la salle
     *  - idObjet : l'ID de l'objet
     *  - coord : Coordonnées de la zone de l'objet
     *  - taille : taille de l'objet
     * 
     * Forme des coordonnées :
     *  "x1;y1;x2;y2"
     */
    public function placeObjectInRoom($idSalle, $idObjet, $coord, $taille): void
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        INSERT INTO IS_DANS VALUES(:ids, :ido, :co, :size)
        ';

        $stmt = $conn->prepare($sql);
        $stmt->execute([
            'ids' => $idSalle,
            'ido' => $idObjet,
            'co' => $coord,
            'size' => $taille
        ]);
    }

    /**
     * Fonction permettant l'insertion d'un objet dans une salle via la table IS_DANS_OBJET
     * 
     * Attributs :
     *  - idSalle : l'ID du premier objet
     *  - idObjet : l'ID du second objet
     *  - coord : Coordonnées de la zone de l'objet
     *  - taille : taille de l'objet
     * 
     * Forme des coordonnées :
     *  "x1;y1;x2;y2"
     */
    public function placeObjectInObj($idObjet1, $idObjet2, $coord): void
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        INSERT INTO IS_DANS_OBJET VALUES(:ido1, :ido2, :co)
        ';

        $stmt = $conn->prepare($sql);
        $stmt->execute([
            'ido1' => $idObjet1,
            'ido2' => $idObjet2,
            'co' => $coord,
        ]);
    }

    /**
     * Methode permettant de verifier si deux objet se croise ou non
     * 
     * Attributs : 
     *  - idsalle : ID de la salle
     *  - coord : Coordonnée de la zone de l'objet
     */
    public function verifConflicZone($idsalle, $coord): bool
    {
        $conn = $this->getEntityManager()->getConnection();

        //Recuperation des coordonnées des objet dans la salle choisis
        $sql = '
        SELECT * FROM IS_DANS WHERE idsalle = :id
        ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(['id' => $idsalle]);

        $data = $stmt->fetchAll();

        $rect1 = explode(",", $coord);
        
        //Si la salle contient des objets
        if (!empty($data))
        {
            //Pour chaque objet
            foreach ($data as $d) {
                //Recuperer leurs ID sous forme de tableau [x1,y1,x2,y2]
                $rect2 = explode(",", $d["coordobjet"]);

                /**
                * Si :
                * la plus petite valeur entre la coordonnée en x du point en bas à droite du rectangle 1 et 2
                * est plus petit ou egal à la plus grande valeur entre la coordonnée en x du point en haut à
                * gauche du rectangcoordobjete 1 et 2.
                * 
                * OU
                * 
                * la plus petite vacoordobjeteur entre la coordonnée en y du point en bas à droite du rectangle 1 et 2
                * est plus petit oucoordobjetegal à la plus grande valeur entre la coordonnée en y du point en haut à
                * gauche du rectangcoordobjete 1 et 2.
                * 
                * ALORS : VRAI
                * SINON : FAUX
                */
                return (
                    min($rect1[2], $rect2[2]) <= max($rect1[0], $rect2[0]) ||
                    min($rect1[3], $rect2[3]) <= max($rect1[1], $rect2[1])
                );
            }
        }
        return true;
    }

    /**
     * Methode permettant de verifier si deux objet se croise ou non
     * 
     * Attributs : 
     *  - idsalle : ID de la salle
     *  - coord : Coordonnée de la zone de l'objet
     */
    public function verifConflicZoneObject($idObjet, $coord): bool
    {
        $conn = $this->getEntityManager()->getConnection();

        //Recuperation des coordonnées des objet dans la salle choisis
        $sql = '
        SELECT * FROM IS_DANS_OBJET WHERE idobjetSuperieur = :id
        ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(['id' => $idObjet]);

        $data = $stmt->fetchAll();

        $rect1 = explode(",", $coord);

        //Si la salle contient des objets
        if (!empty($data))
        {
            //Pour chaque objet
            foreach ($data as $d) {
                //Recuperer leurs ID sous forme de tableau [x1,y1,x2,y2]
                $rect2 = explode(",", $d["coordobjet2"]);

                /**
                * Si :
                * la plus petite valeur entre la coordonnée en x du point en bas à droite du rectangle 1 et 2
                * est plus petit ou egal à la plus grande valeur entre la coordonnée en x du point en haut à
                * gauche du rectangle 1 et 2.
                * 
                * OU
                * 
                * la plus petite valeur entre la coordonnée en y du point en bas à droite du rectangle 1 et 2
                * est plus petit ou egal à la plus grande valeur entre la coordonnée en y du point en haut à
                * gauche du rectangle 1 et 2.
                * 
                * ALORS : VRAI
                * SINON : FAUX
                */
                return (
                    min($rect1[2], $rect2[2]) <= max($rect1[0], $rect2[0]) ||
                    min($rect1[3], $rect2[3]) <= max($rect1[1], $rect2[1])
                );
            }
        }
        return true;
    }

    /**
     * Trouver les objet inferieur via un type
     */
    public function findObjetInferieur($typeObjet): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT DISTINCT idobjet ,type,nomobjet,imageobjet
        FROM OBJET
        WHERE type = :type;
            ';
        $dico= array('contenant'=>'contenu','contenu'=>'objet','objet' => 'objet');
        $stmt = $conn->prepare($sql);
        $stmt->execute(['type' => $dico[$typeObjet]]);

        return $stmt->fetchAll();
    }
    
    /**
     * Suppression d'un objet
     */
    public function SupprimerObjetRequet($idObjet): void
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        DELETE 
        FROM OBJET 
        WHERE idobjet = :idobj 
            and :idobj not in(select idobjet FROM IS_DANS)
            and :idobj not in(select idobjet FROM EST_LIER_A)
            and :idobj not in(select idobjetSuperieur FROM IS_DANS_OBJET)
            and :idobj not in(select idobjetInferieur FROM IS_DANS_OBJET)
            ;
        ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(['idobj' => $idObjet]);
    }

    public function SupprimerObjetDansObjetRequet($idObjet): void
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        DELETE FROM IS_DANS_OBJET 
        WHERE idobjetInferieur = :ido;
        ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(['ido' => $idObjet]);
    }

    public function getInfObject($idObjet): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT *
        FROM IS_DANS_OBJET NATURAL JOIN OBJET
        WHERE idobjetSuperieur = :id and idobjet = idobjetInferieur
        ORDER BY idobjet
        ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(['id' => $idObjet]);

        return $stmt->fetchAll();
    }

    public function getEnigme($dm)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT *
        FROM EST_LIER_A
        NATURAL JOIN ENIGME
        WHERE iddm = :dm
        ORDER BY idobjet
        ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(['dm' => $dm]);

        return $stmt->fetchAll();
    }

    public function getInfObjectWithEnigme($iddm, $idObjet): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT *
        FROM 
            IS_DANS_OBJET 
            NATURAL JOIN OBJET 
            NATURAL JOIN EST_LIER_A 
        WHERE idobjetSuperieur = :id and idobjet = idobjetInferieur and iddm = :idm
        ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(['id' => $idObjet, 'idm' => $iddm]);

        return $stmt->fetchAll();
    }

    public function getCoordNameObjetInf($idObjet)
    {
        $conn = $this->getEntityManager()->getConnection();
        
        $sql = '
        SELECT coordobjet2, nomobjet, idobjetInferieur
        FROM IS_DANS_OBJET NATURAL JOIN OBJET
        WHERE idobjetSuperieur = :obj and idobjet = idobjetInferieur
        ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(['obj' => $idObjet]);

        return $stmt->fetchAll();
    }
    
}
