<?php

namespace App\Repository;

use App\Entity\Groupe;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class GroupeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Groupe::class);
    }

    /**
     * Suppression d'une salle
     */

    public function getNbGroupe($iddevoir): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT COUNT(*) as cpt
        FROM GROUPE 
        WHERE idgr in (select idgr from DM 
        WHERE iddev=:iddevoir )
            ';
        
        $stmt = $conn->prepare($sql);
        $stmt->execute(['iddevoir' =>$iddevoir]);

        return $stmt->fetchAll();
    }
    
        public function getNbEtudiantMoyenne($iddevoir): int
    {
        $conn = $this->getEntityManager()->getConnection();

        //~ $sql = '
        //~ SELECT idgr , COUNT(netu) as val
		//~ FROM  APPARTIENT 
		//~ WHERE idgr in (SELECT idgr
        //~ FROM  APPARTIENT NATURAL JOIN DM 
        //~ WHERE iddev=:iddevoir)
		//~ GROUP BY idgr
            //~ ';
        $sql = '
        SELECT COUNT(netu) as val
		FROM  ELEVE 
		WHERE netu in 
		   (SELECT netu
			FROM APPARTIENT 
			WHERE idgr in 
			   (SELECT idgr 
				FROM DM 
				WHERE iddev=:iddevoir));
		
            ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['iddevoir' =>$iddevoir]);

        $liste= $stmt->fetchAll();
        //~ $cpt=0;
        //~ foreach($liste as $l){
			//~ $cpt+=intval($l['val']);
			//~ }
		//~ return floor($cpt/sizeof($liste));
		dump($liste);
        return intval($liste[0]['val']);
    }
    
    /**
     * Trouver le groupe qui ne sont pas dans un devoir
     */
     public function getGroupeParDevoir($iddevoir): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        select idgr,nomgroupe
        from GROUPE 
        where idgr in(
			select idgr 
			from APPARTIENT A  
			group by idgr 
			having count(netu )= 
				(select count(netu) 
				from APPARTIENT B  
				where netu not in
					(select distinct netu 
					from APPARTIENT natural join DM 
					where iddev=:iddevoir) 
				and A.idgr= B.idgr  
				group by idgr));
            ';
        
        $stmt = $conn->prepare($sql);
        $stmt->execute(['iddevoir' =>$iddevoir]);

        $liste=$stmt->fetchAll();
        $res=array();
        foreach ($liste as $groupe){
			$res[$groupe['nomgroupe']]=$groupe['idgr'];
			}
		return $res;
        
    }

    /**
     * Trouver les groupes d'un devoir
     */
     public function getGroupeDevoir($iddevoir): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
       select idgr,nomgroupe
       from DM natural join GROUPE
       where iddev=:iddevoir;
            ';
        
        $stmt = $conn->prepare($sql);
        $stmt->execute(['iddevoir' =>$iddevoir]);

        $liste=$stmt->fetchAll();
        $res=array();
        foreach ($liste as $groupe){
			$res[$groupe['nomgroupe']]=$groupe['idgr'];
			}
		return $res;
        
        
    }
    public function getEudiantParGroupe($iddev): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT idgr,count(netu) as cpt
		from APPARTIENT
		group by idgr;
		
            ';
        
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $liste=$stmt->fetchAll();
        $res=array();
        foreach ($liste as $groupe){
			$res[$groupe['idgr']]=$groupe['cpt'];
			}
		return $res;
        
    
    }

    /**
     * Supprimer un groupe et ses dépendances 
     */    
      public function deleteGroupe($idgroupe): bool
    {
        $conn = $this->getEntityManager()->getConnection();
	
        $sql = '
        delete from APPARTIENT
		where idgr= :idgroupe
		and idgr not in (select idgr from DM);
            ';
        
        $stmt = $conn->prepare($sql);
        $stmt->execute(['idgroupe' =>$idgroupe]);
		
		$sql = '
        delete from GROUPE
		where idgr= :idgroupe
		and idgr not in (select idgr from DM);
            ';
        
        $stmt = $conn->prepare($sql);
        $stmt->execute(['idgroupe' =>$idgroupe]);
		
		$sql = '
        select * from GROUPE
		where idgr= :idgroupe;
            ';
        
        $stmt = $conn->prepare($sql);
        $stmt->execute(['idgroupe' =>$idgroupe]);
		$liste= $stmt->fetchAll();
		dump($liste);
		return empty($liste);
        }
        
    //~ /**
     //~ * Trouver les étudiants qui ne sont pas dans ce groupe 
     //~ */
        //~ public function getPasEtudiantGroupe($idgroupe): array
    //~ {
        //~ $conn = $this->getEntityManager()->getConnection();

        //~ $sql = '
        //~ SELECT * 
        //~ FROM GROUPE NATURAL JOIN APPARTIENT NATURAL JOIN ELEVE NATURAL JOIN PERSONNE 
        //~ WHERE netu not in 
        //~ (select netu from APPARTIENT where  idgr=:idgr);
            //~ ';
        
        //~ $stmt = $conn->prepare($sql);
        //~ $stmt->execute(['idgr' =>$idgroupe]);

        //~ $liste=$stmt->fetchAll();
        //~ $res=array();
        //~ foreach ($liste as $etudiant){
			//~ $res[$etudiant['prenom']." ".$etudiant['nom']]=$etudiant['netu'];
			//~ }
		//~ return $res;
    //~ }
    
    /**
     * Trouver les étudiants de ce groupe 
     */
    public function getEtudiantGroupe($idgroupe): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT *
        FROM GROUPE NATURAL JOIN APPARTIENT NATURAL JOIN ELEVE NATURAL JOIN PERSONNE
        WHERE idgr=:idgr
        
            ';
        
        $stmt = $conn->prepare($sql);
        $stmt->execute(['idgr' =>$idgroupe->getIdgr()]);

        $liste=$stmt->fetchAll();
        $res=array();
        foreach ($liste as $etudiant){
			$res[$etudiant['prenom']." ".$etudiant['nom']]=$etudiant['netu'];
			}
		return $res;
    }
    /**
     * Trouver les étudiants de ce groupe 
     */
    public function getEtudiantGroupeSansPush($idgroupe): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT *
        FROM GROUPE NATURAL JOIN APPARTIENT NATURAL JOIN ELEVE NATURAL JOIN PERSONNE
        WHERE idgr=:idgr
        AND netu not in (select idpush from GROUPE where idgr=:idgr)
        
            ';
        
        $stmt = $conn->prepare($sql);
        $stmt->execute(['idgr' =>$idgroupe->getIdgr()]);

        $liste=$stmt->fetchAll();
        $res=array();
        foreach ($liste as $etudiant){
			$res[$etudiant['prenom']." ".$etudiant['nom']]=$etudiant['netu'];
			}
		return $res;
    }
       public function getEtudiantPush(): array
    {
        $conn = $this->getEntityManager()->getConnection();


	   $sql = '
		SELECT * 
		FROM PERSONNE NATURAL join ELEVE 
		WHERE netu in (select idpush from GROUPE)

        
            ';

        //~ $sql = '
        //~ SELECT *
        //~ FROM PERSONNE
        //~ WHERE idpers in (select idpush from GROUPE)
        //~ AND idpers in (select idpers from ELEVE);
        
            //~ ';
        
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $liste=$stmt->fetchAll();
        $res=array();
        foreach ($liste as $etudiant){
			$res[$etudiant['netu']]=$etudiant['prenom']." ".$etudiant['nom'];
			}
		$res[""]="";
		return $res;
    }
}
