<?php

namespace App\Repository;

use App\Entity\Personne;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class PersonneRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Personne::class);
    }

    public function StudentAccountVerification($number, $mdp): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT DISTINCT nom, prenom 
        FROM PERSONNE, ELEVE
        WHERE netu = :id AND mdp = :mdp
            ';
        
        $stmt = $conn->prepare($sql);
        $stmt->execute(['id' => $number, 'mdp' => $mdp]);

        return $stmt->fetchAll();
    }

    public function TeacherAccountVerification($number, $mdp): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT DISTINCT nom, prenom 
        FROM PERSONNE, PROFESSEUR
        WHERE idprof = :id AND mdp = :mdp
            ';
        
        $stmt = $conn->prepare($sql);
        $stmt->execute(['id' => $number, 'mdp' => $mdp]);

        return $stmt->fetchAll();
    }
}
