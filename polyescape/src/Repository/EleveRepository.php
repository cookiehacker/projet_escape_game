<?php

namespace App\Repository;

use App\Entity\Eleve;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class EleveRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Eleve::class);
    }

    /**
     * Trouver toutes les classes 
     */    
    public function ListeClasse(): array
    {
	$conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT DISTINCT  classe 
        FROM ELEVE;
            ';
            
        $stmt = $conn->prepare($sql);
        $stmt->execute([]);

        return $stmt->fetchAll();
    }

    /**
     * Trouver tous les étudiants pour une classe 
     */
        public function ListeEtudiantParClasse($classe): array
    {
	$conn = $this->getEntityManager()->getConnection();

        $sql = '
        SELECT *
        FROM ELEVE
        where classe = :c;
            ';
            
      $stmt = $conn->prepare($sql);
        $stmt->execute(['c' => $classe]);

        return $stmt->fetchAll();
    }


    /**
     * Trouver les étudiants qui ne sont pas dans ce groupe 
     */
    public function getPasEtudiantGroupe($idgroupe): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        select prenom,nom,netu
        FROM GROUPE NATURAL JOIN APPARTIENT NATURAL JOIN ELEVE NATURAL JOIN PERSONNE 
        WHERE netu not in 
        (select netu from APPARTIENT where  idgr=:idgr);
            ';
        
        $stmt = $conn->prepare($sql);
        $stmt->execute(['idgr' =>$idgroupe]);

        $liste=$stmt->fetchAll();
        $res=array();
        foreach ($liste as $etudiant){
			$res[$etudiant['prenom']." ".$etudiant['nom']]=$etudiant['netu'];
			}
		return $res;
		//~ return $stmt->fetchAll();
    }
    public function removeEleve($netu): void
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
        DELETE 
            FROM APPARTIENT 
                WHERE netu = :netu;
        DELETE 
            FROM ELEVE 
                WHERE netu = :netu;
        DELETE 
            FROM PERSONNE
                WHERE netu = :netu;
        ';

        $stmt = $conn->prepare($sql);
        $stmt->execute([
            'netu' => $netu,
        ]);
    }

   

}
