<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Niveau
 *
 * @ORM\Table(name="NIVEAU")
 * @ORM\Entity
 */
class Niveau
{
    /**
     * @var int
     *
     * @ORM\Column(name="niveau", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $niveau;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descriptionniveau", type="text", length=65535, nullable=true)
     */
    private $descriptionniveau;

    public function getNiveau(): ?int
    {
        return $this->niveau;
    }

    public function getDescriptionniveau(): ?string
    {
        return $this->descriptionniveau;
    }

    public function setDescriptionniveau(?string $descriptionniveau): self
    {
        $this->descriptionniveau = $descriptionniveau;

        return $this;
    }


}
