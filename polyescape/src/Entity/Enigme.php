<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Enigme
 *
 * @ORM\Table(name="ENIGME", indexes={@ORM\Index(name="niveau", columns={"niveau"}), @ORM\Index(name="idmat", columns={"idmat"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\EnigmeRepository")
 */
class Enigme
{
    /**
     * @var int
     *
     * @ORM\Column(name="idenigme", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idenigme;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nomEnigme", type="string", length=30, nullable=true)
     */
    private $nomenigme;

    /**
     * @var string|null
     *
     * @ORM\Column(name="solution", type="text", length=65535, nullable=true)
     */
    private $solution;

    /**
     * @var int|null
     *
     * @ORM\Column(name="maxpoint", type="integer", nullable=true)
     */
    private $maxpoint = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="question", type="text", length=65535, nullable=true)
     */
    private $question;

    /**
     * @var \Niveau
     *
     * @ORM\ManyToOne(targetEntity="Niveau")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="niveau", referencedColumnName="niveau")
     * })
     */
    private $niveau;

    /**
     * @var \Matiere
     *
     * @ORM\ManyToOne(targetEntity="Matiere")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idmat", referencedColumnName="idmat")
     * })
     */
    private $idmat;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Devoir", inversedBy="idenigme")
     * @ORM\JoinTable(name="EST_DANS",
     *   joinColumns={
     *     @ORM\JoinColumn(name="idenigme", referencedColumnName="idenigme")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="iddev", referencedColumnName="iddev")
     *   }
     * )
     */
    private $iddev;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->iddev = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getIdenigme(): ?int
    {
        return $this->idenigme;
    }

    public function getNomenigme(): ?string
    {
        return $this->nomenigme;
    }

    public function setNomenigme(?string $nomenigme): self
    {
        $this->nomenigme = $nomenigme;

        return $this;
    }

    public function getSolution(): ?string
    {
        return $this->solution;
    }

    public function setSolution(?string $solution): self
    {
        $this->solution = $solution;

        return $this;
    }

    public function getMaxpoint(): ?int
    {
        return $this->maxpoint;
    }

    public function setMaxpoint(?int $maxpoint): self
    {
        $this->maxpoint = $maxpoint;

        return $this;
    }

    public function getQuestion(): ?string
    {
        return $this->question;
    }

    public function setQuestion(?string $question): self
    {
        $this->question = $question;

        return $this;
    }

    public function getNiveau(): ?Niveau
    {
        return $this->niveau;
    }

    public function setNiveau(?Niveau $niveau): self
    {
        $this->niveau = $niveau;

        return $this;
    }

    public function getIdmat(): ?Matiere
    {
        return $this->idmat;
    }

    public function setIdmat(?Matiere $idmat): self
    {
        $this->idmat = $idmat;

        return $this;
    }

    /**
     * @return Collection|Devoir[]
     */
    public function getIddev(): Collection
    {
        return $this->iddev;
    }

    public function addIddev(Devoir $iddev): self
    {
        if (!$this->iddev->contains($iddev)) {
            $this->iddev[] = $iddev;
        }

        return $this;
    }

    public function removeIddev(Devoir $iddev): self
    {
        if ($this->iddev->contains($iddev)) {
            $this->iddev->removeElement($iddev);
        }

        return $this;
    }

}
