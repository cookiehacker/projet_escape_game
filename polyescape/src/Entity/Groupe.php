<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Groupe
 *
 * @ORM\Table(name="GROUPE")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\GroupeRepository")
 */
class Groupe
{
    /**
     * @var int
     *
     * @ORM\Column(name="idgr", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idgr;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nomGroupe", type="string", length=100, nullable=true)
     */
    private $nomgroupe;

    /**
     * @var int|null
     *
     * @ORM\Column(name="idpush", type="integer", nullable=true)
     */
    private $idpush;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Eleve", inversedBy="idgr")
     * @ORM\JoinTable(name="APPARTIENT",
     *   joinColumns={
     *     @ORM\JoinColumn(name="idgr", referencedColumnName="idgr")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="netu", referencedColumnName="netu")
     *   }
     * )
     */
    private $netu;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->netu = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getIdgr(): ?int
    {
        return $this->idgr;
    }

    public function getNomgroupe(): ?string
    {
        return $this->nomgroupe;
    }

    public function setNomgroupe(?string $nomgroupe): self
    {
        $this->nomgroupe = $nomgroupe;

        return $this;
    }

    public function getIdpush(): ?int
    {
        return $this->idpush;
    }

    public function setIdpush(?int $idpush): self
    {
        $this->idpush = $idpush;

        return $this;
    }

    /**
     * @return Collection|Eleve[]
     */
    public function getNetu(): Collection
    {
        return $this->netu;
    }

    public function addNetu(Eleve $netu): self
    {
        if (!$this->netu->contains($netu)) {
            $this->netu[] = $netu;
        }

        return $this;
    }

    public function removeNetu(Eleve $netu): self
    {
        if ($this->netu->contains($netu)) {
            $this->netu->removeElement($netu);
        }

        return $this;
    }

}
