<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cadena
 *
 * @ORM\Table(name="CADENA")
 * @ORM\Entity
 */
class Cadena
{
    /**
     * @var int
     *
     * @ORM\Column(name="idcadena", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcadena;

    /**
     * @var string|null
     *
     * @ORM\Column(name="intitule", type="text", length=65535, nullable=true)
     */
    private $intitule;

    /**
     * @var int|null
     *
     * @ORM\Column(name="niveauc", type="integer", nullable=true)
     */
    private $niveauc;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="etatc", type="boolean", nullable=true)
     */
    private $etatc;

    /**
     * @var int|null
     *
     * @ORM\Column(name="nbessai", type="integer", nullable=true)
     */
    private $nbessai;

    /**
     * @var int|null
     *
     * @ORM\Column(name="nbessaimax", type="integer", nullable=true)
     */
    private $nbessaimax;

    public function getIdcadena(): ?int
    {
        return $this->idcadena;
    }

    public function getIntitule(): ?string
    {
        return $this->intitule;
    }

    public function setIntitule(?string $intitule): self
    {
        $this->intitule = $intitule;

        return $this;
    }

    public function getNiveauc(): ?int
    {
        return $this->niveauc;
    }

    public function setNiveauc(?int $niveauc): self
    {
        $this->niveauc = $niveauc;

        return $this;
    }

    public function getEtatc(): ?bool
    {
        return $this->etatc;
    }

    public function setEtatc(?bool $etatc): self
    {
        $this->etatc = $etatc;

        return $this;
    }

    public function getNbessai(): ?int
    {
        return $this->nbessai;
    }

    public function setNbessai(?int $nbessai): self
    {
        $this->nbessai = $nbessai;

        return $this;
    }

    public function getNbessaimax(): ?int
    {
        return $this->nbessaimax;
    }

    public function setNbessaimax(?int $nbessaimax): self
    {
        $this->nbessaimax = $nbessaimax;

        return $this;
    }


}
