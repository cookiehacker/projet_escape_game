<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Dm
 *
 * @ORM\Table(name="DM", indexes={@ORM\Index(name="iddev", columns={"iddev"}), @ORM\Index(name="idgr", columns={"idgr"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\DmRepository")
 */
class Dm
{
    /**
     * @var int
     *
     * @ORM\Column(name="iddm", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iddm;

    /**
     * @var int|null
     *
     * @ORM\Column(name="note", type="integer", nullable=true)
     */
    private $note = '0';

    /**
     * @var \Devoir
     *
     * @ORM\ManyToOne(targetEntity="Devoir")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="iddev", referencedColumnName="iddev")
     * })
     */
    private $iddev;

    /**
     * @var \Groupe
     *
     * @ORM\ManyToOne(targetEntity="Groupe")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idgr", referencedColumnName="idgr")
     * })
     */
    private $idgr;

    public function getIddm(): ?int
    {
        return $this->iddm;
    }

    public function getNote(): ?int
    {
        return $this->note;
    }

    public function setNote(?int $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getIddev(): ?Devoir
    {
        return $this->iddev;
    }

    public function setIddev(?Devoir $iddev): self
    {
        $this->iddev = $iddev;

        return $this;
    }

    public function getIdgr(): ?Groupe
    {
        return $this->idgr;
    }

    public function setIdgr(?Groupe $idgr): self
    {
        $this->idgr = $idgr;

        return $this;
    }


}
