<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Devoir
 *
 * @ORM\Table(name="DEVOIR", indexes={@ORM\Index(name="idmat", columns={"idmat"}), @ORM\Index(name="idprof", columns={"idprof"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\DevoirRepository")
 */
class Devoir
{
    /**
     * @var int
     *
     * @ORM\Column(name="iddev", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iddev;

    /**
     * @var string|null
     *
     * @ORM\Column(name="statut", type="string", length=50, nullable=true)
     */
    private $statut;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nomDev", type="string", length=50, nullable=true)
     */
    private $nomdev;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     */
    private $date;

    /**
     * @var int|null
     *
     * @ORM\Column(name="nbpersonnegroupe", type="integer", nullable=true)
     */
    private $nbpersonnegroupe;

    /**
     * @var int|null
     *
     * @ORM\Column(name="nbgroupe", type="integer", nullable=true)
     */
    private $nbgroupe;

    /**
     * @var int|null
     *
     * @ORM\Column(name="maxPoint", type="integer", nullable=true)
     */
    private $maxpoint = '0';

    /**
     * @var \Matiere
     *
     * @ORM\ManyToOne(targetEntity="Matiere")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="idmat", referencedColumnName="idmat")
     * })
     */
    private $idmat;

    /**
     * @var \Professeur
     *
     * @ORM\ManyToOne(targetEntity="Professeur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idprof", referencedColumnName="idprof")
     * })
     */
    private $idprof;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Salle", mappedBy="iddev")
     */
    private $idsalle;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Enigme", mappedBy="iddev")
     */
    private $idenigme;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idsalle = new \Doctrine\Common\Collections\ArrayCollection();
        $this->idenigme = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getIddev(): ?int
    {
        return $this->iddev;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(?string $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    public function getNomdev(): ?string
    {
        return $this->nomdev;
    }

    public function setNomdev(?string $nomdev): self
    {
        $this->nomdev = $nomdev;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getNbpersonnegroupe(): ?int
    {
        return $this->nbpersonnegroupe;
    }

    public function setNbpersonnegroupe(?int $nbpersonnegroupe): self
    {
        $this->nbpersonnegroupe = $nbpersonnegroupe;

        return $this;
    }

    public function getNbgroupe(): ?int
    {
        return $this->nbgroupe;
    }

    public function setNbgroupe(?int $nbgroupe): self
    {
        $this->nbgroupe = $nbgroupe;

        return $this;
    }

    public function getMaxpoint(): ?int
    {
        return $this->maxpoint;
    }

    public function setMaxpoint(?int $maxpoint): self
    {
        $this->maxpoint = $maxpoint;

        return $this;
    }

    public function getIdmat(): ?Matiere
    {
        return $this->idmat;
    }

    public function setIdmat(?Matiere $idmat): self
    {
        $this->idmat = $idmat;

        return $this;
    }

    public function getIdprof(): ?Professeur
    {
        return $this->idprof;
    }

    public function setIdprof(?Professeur $idprof): self
    {
        $this->idprof = $idprof;

        return $this;
    }

    /**
     * @return Collection|Salle[]
     */
    public function getIdsalle(): Collection
    {
        return $this->idsalle;
    }

    public function addIdsalle(Salle $idsalle): self
    {
        if (!$this->idsalle->contains($idsalle)) {
            $this->idsalle[] = $idsalle;
            $idsalle->addIddev($this);
        }

        return $this;
    }

    public function removeIdsalle(Salle $idsalle): self
    {
        if ($this->idsalle->contains($idsalle)) {
            $this->idsalle->removeElement($idsalle);
            $idsalle->removeIddev($this);
        }

        return $this;
    }

    /**
     * @return Collection|Enigme[]
     */
    public function getIdenigme(): Collection
    {
        return $this->idenigme;
    }

    public function addIdenigme(Enigme $idenigme): self
    {
        if (!$this->idenigme->contains($idenigme)) {
            $this->idenigme[] = $idenigme;
            $idenigme->addIddev($this);
        }

        return $this;
    }

    public function removeIdenigme(Enigme $idenigme): self
    {
        if ($this->idenigme->contains($idenigme)) {
            $this->idenigme->removeElement($idenigme);
            $idenigme->removeIddev($this);
        }

        return $this;
    }
    public function getArrayListe(): ?array
    {
		$res=array();
		$cpt=0;
		foreach ($this->getIdenigme() as $valeur) {
		$res[$cpt]=$valeur;
		$cpt+=1;
		}
        return $res;
    }
        public function getArraySalle(): ?array
    {
		$res=array();
		$cpt=0;
		foreach ($this->getIdsalle() as $valeur) {
		$res[$cpt]=$valeur;
		$cpt+=1;
		}
        return $res;
    }

}
