<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Salle
 *
 * @ORM\Table(name="SALLE")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\SalleRepository")
 */
class Salle
{
    /**
     * @var int
     *
     * @ORM\Column(name="idsalle", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idsalle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nomsalle", type="string", length=500, nullable=true)
     */
    private $nomsalle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descriptsalle", type="text", length=65535, nullable=true)
     */
    private $descriptsalle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="imagesalle", type="string", length=1000, nullable=true)
     */
    private $imagesalle;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Devoir", inversedBy="idsalle")
     * @ORM\JoinTable(name="COMPOSER_DE",
     *   joinColumns={
     *     @ORM\JoinColumn(name="idsalle", referencedColumnName="idsalle")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="iddev", referencedColumnName="iddev")
     *   }
     * )
     */
    private $iddev;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Objet", inversedBy="idsalle")
     * @ORM\JoinTable(name="IS_DANS",
     *   joinColumns={
     *     @ORM\JoinColumn(name="idsalle", referencedColumnName="idsalle")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="idobjet", referencedColumnName="idobjet")
     *   }
     * )
     */
    private $idobjet;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->iddev = new \Doctrine\Common\Collections\ArrayCollection();
        $this->idobjet = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getIdsalle(): ?int
    {
        return $this->idsalle;
    }

    public function getNomsalle(): ?string
    {
        return $this->nomsalle;
    }

    public function setNomsalle(?string $nomsalle): self
    {
        $this->nomsalle = $nomsalle;

        return $this;
    }

    public function getDescriptsalle(): ?string
    {
        return $this->descriptsalle;
    }

    public function setDescriptsalle(?string $descriptsalle): self
    {
        $this->descriptsalle = $descriptsalle;

        return $this;
    }

    public function getImagesalle(): ?string
    {
        return $this->imagesalle;
    }

    public function setImagesalle(?string $imagesalle): self
    {
        $this->imagesalle = $imagesalle;

        return $this;
    }

    /**
     * @return Collection|Devoir[]
     */
    public function getIddev(): Collection
    {
        return $this->iddev;
    }

    public function addIddev(Devoir $iddev): self
    {
        if (!$this->iddev->contains($iddev)) {
            $this->iddev[] = $iddev;
        }

        return $this;
    }

    public function removeIddev(Devoir $iddev): self
    {
        if ($this->iddev->contains($iddev)) {
            $this->iddev->removeElement($iddev);
        }

        return $this;
    }

    /**
     * @return Collection|Objet[]
     */
    public function getIdobjet(): Collection
    {
        return $this->idobjet;
    }

    public function addIdobjet(Objet $idobjet): self
    {
        if (!$this->idobjet->contains($idobjet)) {
            $this->idobjet[] = $idobjet;
        }

        return $this;
    }

    public function removeIdobjet(Objet $idobjet): self
    {
        if ($this->idobjet->contains($idobjet)) {
            $this->idobjet->removeElement($idobjet);
        }

        return $this;
    }
        public function getArrayObject(): ?array
    {
		$res=array();
		$cpt=0;
		foreach ($this->getIdobjet() as $valeur) {
		$res[$cpt]=$valeur;
		$cpt+=1;
		}
        return $res;
    }

}
