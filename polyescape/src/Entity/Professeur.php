<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Professeur
 *
 * @ORM\Table(name="PROFESSEUR", indexes={@ORM\Index(name="idpers", columns={"idpers"})})
 * @ORM\Entity
 */
class Professeur implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="idprof", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idprof;

    /**
     * @var \Personne
     *
     * @ORM\ManyToOne(targetEntity="Personne")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idpers", referencedColumnName="idpers")
     * })
     */
    private $idpers;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Matiere", inversedBy="idprof")
     * @ORM\JoinTable(name="ENSEIGNE",
     *   joinColumns={
     *     @ORM\JoinColumn(name="idprof", referencedColumnName="idprof")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="idmat", referencedColumnName="idmat")
     *   }
     * )
     */
    private $idmat;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idmat = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getIdprof(): ?int
    {
        return $this->idprof;
    }

    public function getIdpers(): ?Personne
    {
        return $this->idpers;
    }

    public function setIdpers(?Personne $idpers): self
    {
        $this->idpers = $idpers;

        return $this;
    }

    /**
     * @return Collection|Matiere[]
     */
    public function getIdmat(): Collection
    {
        return $this->idmat;
    }

    public function addIdmat(Matiere $idmat): self
    {
        if (!$this->idmat->contains($idmat)) {
            $this->idmat[] = $idmat;
        }

        return $this;
    }

    public function removeIdmat(Matiere $idmat): self
    {
        if ($this->idmat->contains($idmat)) {
            $this->idmat->removeElement($idmat);
        }

        return $this;
    }


    public function getUsername()
    {
        return $this->idpers->getEmail();
    }

    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    public function getPassword()
    {
        return $this->idpers->getMdp();
    }

    public function getRoles()
    {
        return array('ROLE_TEACHER');
    }

    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->idpers->email,
            $this->idpers->email,
            $this->idpers->mdp,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->idpers->email,
            $this->idpers->email,
            $this->idpers->mdp,
            // see section on salt below
            // $this->salt
        ) = unserialize($serialized, array('allowed_classes' => false));
    }

    public function AuthValid($credentials)
    {
        return hash("sha512", $credentials["password"]) === $this->idpers->getMdp();
    }


}
