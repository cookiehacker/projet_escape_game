<?php

namespace App\Command;

use App\Entity\Personne;
use App\Entity\Professeur;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

use Doctrine\ORM\EntityRepository;

class CreateTeacher extends ContainerAwareCommand
{
    protected static $defaultName = 'app:create-teacher';

    /**
     * Champs necessaire :
     *      String : Adresse email
     *      String : Mot de passe
     */

    public function __construct()
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription("Creation d'un nouveau prof")
            ->setHelp("Cette commande vous permet de creer un nouveau professeur...")
            ->addArgument('email', InputArgument::REQUIRED, 'L\'email du professeur')
            ->addArgument('password', InputArgument::REQUIRED, 'Le mot de passe du professeur');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(
            [
                'Creation d\'un professeur',
                '=========================',
                '',
            ]);
        
        $output->writeln('Creation d\'un professeur: '.$input->getArgument('email'));
        $this->addTeacher($input->getArgument('email'), $input->getArgument('password'));
        $output->writeln(['Votre professeur est créé']);
    }

    private function addTeacher($email, $password) : void
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $p = new Personne();
        $p->setNom("Teacher");
        $p->setPrenom("New");
        $p->setEmail($email);
                
        $p->setMdp(hash("sha512", $password));

        $em->persist($p);
        $em->flush();

        $prof = new Professeur();
        $prof->setIdpers($p);

        $em->persist($prof);
        $em->flush();
                
        $transport = (new \Swift_SmtpTransport('smtp.univ-orleans.fr', 25));
                
        $mailer = new \Swift_Mailer($transport);

        $message = (new \Swift_Message("PolyEscape"))
            -> setFrom("polyescape@etu.univ-orleans.fr")
            -> setTo($p->getEmail())
            -> setBody("
            <h2>Bienvenue sur l'application PolyEscape</h2>
                <p>
                    Votre enseignant vous a ajouté à l'application !
                    Pour vous connectez il vous suffit d'aller sur le site web et vous connectez en tant que professeur via vos identifiants.
                </p>
            <ul>
                <li> <b> Identifiant : </b>".$p->getEmail()."</li>
                <li> <b> Mot de passe : </b>".$password."</li>
            </ul>
            ", "text/html");

        $mailer->send($message);
    }
}