<?php


namespace App\Controller;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

use App\Entity\Salle;
use App\Entity\Niveau;
use App\Entity\Devoir;
use App\Entity\EstLierA;
use App\Entity\Objet;
use App\Entity\Enigme;
use App\Entity\Matiere;
use App\Entity\Professeur;
use App\Entity\Eleve;
use App\Entity\Personne;
use App\Entity\Groupe;
use App\Entity\Dm;
use App\Entity\Cadena;
use App\Entity\Contient;

#CSV
use League\Csv\Reader;
use League\Csv\Statement;

#Import basique
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


#Import formulaire
use Symfony\Component\Form\Extension\Core\Type\TextType; #Champs de texte
use Symfony\Bridge\Doctrine\Form\Type\EntityType; #combobox / checkbox / radio button pour entités
use Symfony\Component\Form\Extension\Core\Type\TextareaType; #Zone de texte
use Symfony\Component\Form\Extension\Core\Type\FileType; #bouton d'ajout de fichiers
use Symfony\Component\HttpFoundation\File\UploadedFile; #Traitement d'upload de fichiers
use Symfony\Component\Form\Extension\Core\Type\SubmitType; #Bouton validation
use Symfony\Component\Form\Extension\Core\Type\HiddenType; #Champs caché
use Symfony\Component\Form\Extension\Core\Type\IntegerType; #Champ de saisie pour valeur numerique
use Symfony\Component\Form\Extension\Core\Type\PasswordType; #Champ de saisie pour les mots de passe
use Symfony\Component\HttpFoundation\Request; #Traitement des requêtes POST / GET
use Symfony\Component\HttpFoundation\Response; #Envoie de reponse
use Symfony\Component\HttpFoundation\RedirectResponse; #Redirection vers une route
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

use Spipu\Html2Pdf\Html2Pdf;
use League\Csv\Writer;
use League\Csv;

use Doctrine\ORM\EntityRepository;

class EnseignantController extends AbstractController
{
    /**
     * @Route("/enseignant", name="enseignant")
     */
    public function index()
    {
      $listeDevoir=$this->getDoctrine()->getRepository(Devoir::class)->getDevoirParIdProf($this->getUser()->getIdprof());
      
      dump($listeDevoir);
      $option["En cours"]=array("diabled","","diabled","","");
      $option["En developpement"]=array("","diabled","diabled","diabled","");
      $option["Fermer"]=array("diabled","diabled","","diabled","diabled");
      return $this->render('enseignant/acceuilEnseignant.html.twig', [
            'controller_name' => 'EnseignantController',
            'listeDevoir' => $listeDevoir,
            'optionButon' => $option,
            'activeHome' => "active",
        ]);
    }

    /**
     * @Route("/enseignant/piece", name="affichagePiece")
     */
    public function showPiece()
    {
        $lsSalle = $this->getDoctrine()->getRepository(Salle::class)->findAll();

        return $this->render('enseignant/piece/affichage/index.html.twig', [
            'lsSalle' => $lsSalle,
            'activePerso' => "active",
        ]);
    }

    /**
     * @Route("/enseignant/piece/see/{idPiece}", name="affichageObjetContenant")
     */
    public function showPieceObjectContenant($idPiece)
    {
        $room = $this->getDoctrine()->getRepository(Salle::class)->find($idPiece);
        if($room!=null){
        return $this->render(
            'enseignant/piece/affichage/contenantPiece.html.twig',
            [
                'lsobjet'=>$room->getIdobjet(),
                'activePerso' => "active",
            ]
        );}
        return new RedirectResponse('/enseignant');
    }

    /**
     * @Route("/enseignant/objets/see/{idObjet}", name="affichageObjetInf")
     */
    public function showPieceObjectInf($idObjet)
    {
        $obj = $this->getDoctrine()->getRepository(Objet::class)->find($idObjet);
        return $this->render(
            'enseignant/objet/infObjet.html.twig',
            [
                'lsobjet'=>$obj->getIdobjetinferieur(),
                'activePerso' => "active",
            ]
        );
    }

    /**
     * @Route("/enseignant/piece/add", name="creationPiece")
     */
    public function createPiece(Request $request)
    {
        /**
         * Creation d'une piece et placement d'un objet dans une piece creer
         */

        $salle = new Salle(); #Creation d'une salle vide

        $lsNiveau = $this->getDoctrine()->getRepository(Niveau::class)->findAll(); #Recuperation des niveaux
        $lsDevoir = $this->getDoctrine()->getRepository(Devoir::class)->findAll(); #Recuperation des devoirs
        $lsSalle = $this->getDoctrine()->getRepository(Salle::class)->findAll(); #Recuperation des salles
        $lsObjet = $this->getDoctrine()->getRepository(Objet::class)->findAll(); #Recuperation des objets


        /**
         * Formulaire de creation de salle
         *
         * Champs :
         *  - nomSalle : un champ texte correspondant au nom de la salle
         *  - decriptsalle : une zone de texte correspondant à la description de la salle
         *  - imagesalle : champs de fichier correspondant à l'image de la salle
         *  - iddev : menu deroulant prenant les ID de tout les devoirs de la BD
         *  - niveau : menu deroulant prenant les ID de tout les niveaux de la BD
         *  - save : bouton de sauvegarde
         */
        $form = $this->createFormBuilder($salle)
            -> add('nomSalle', TextType::class, array("attr" => array("class" => "form-control", "placeholder" => "Nom de la salle")))
            -> add('descriptsalle', TextareaType::class, array("attr" => array("class" => "form-control", "placeholder" => "Description")))
            -> add('imagesalle', FileType::class, array("attr" => array("id" => "gI", "class" => "form-control-file", "onchange" => "previewFile()", "accept" => ".jpg, .png, .jpeg", "max-file-size" => "200KO")))
            -> add('save', SubmitType::class, array("label" => "Ajouter salle", "attr" => array("class" => "btn btn-primary")))
            -> getForm();

        $form2 = $this->createFormBuilder()
            -> add('x1', HiddenType::class, array("attr"=>array("value"=>"")))
            -> add('x2', HiddenType::class, array("attr"=>array("value"=>"")))
            -> add('y1', HiddenType::class, array("attr"=>array("value"=>"")))
            -> add('y2', HiddenType::class, array("attr"=>array("value"=>"")))
            -> add('save', SubmitType::class, array("label" => "Ajouter objet", "attr" => array("class" => "btn btn-primary")))
            -> getForm();



        //Ecoute des deux formulaires
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid())
        {
            //Recuperation des informations du formulaire d'ajout de salles
            $salle = $form->getData();

            //Sauvegarde d'une image dans le serveurs
            $newPic = "pics".date("Ymdgis").".jpg";
            $directory = "../public/picsStorage";
            $file = $form['imagesalle']->getData();
            $file->move($directory, $newPic);

            $salle->setImagesalle($newPic);
            //Envoie des informations à la BD SQL
            $em = $this->getDoctrine()->getManager();
            $em->persist($salle);
            $em->flush();

            return $this->render('enseignant/piece/creerPiece.html.twig', [
                'form' => $form->createView(),
                'error' => 'Salle ajoutée !',
                'activePerso' => "active",
            ]);
        }



        return $this->render('enseignant/piece/creerPiece.html.twig', [
            'form' => $form->createView(),
            'activePerso' => "active",
        ]);
    }

    /**
     * @Route("/enseignant/piece/remove", name="suppressionPiece")
     */
    public function removePiece(Request $request)
    {
        $lsSal = $this->getDoctrine()->getRepository(Salle::class)->findAll();

        $form = $this->createFormBuilder()
            -> add (
               'idsalle',
                EntityType::class,
                array(
                    'class' => Salle::class, //pour la categorie
                    'expanded' => true, //radio 
                    'multiple' => true, // plusieur croix
                    'choice_label' =>
                    function($lsSal) { return $lsSal->getIdsalle();}   
                )
            )
            -> add ('save', SubmitType::class, array('label'=>'Supprimer le ou les salles', "attr" => array("class" => "btn btn-danger")))
            -> getForm();
        
        $form->handleRequest($request);
          if($form->isSubmitted() && $form->isValid())
        {
            $check = $form->get('idsalle')->getData(); // la liste des selections
            foreach ($check as $cb)
            {
				$this->getDoctrine()->getRepository(Salle::class)->SupprimerSalleRequet($cb->getIdsalle());
                
            }
            return new RedirectResponse('/enseignant/piece');
        }
        return $this->render('enseignant/piece/supprimerPiece.html.twig', [
            'lsSal' => $lsSal,
            'form' => $form->createView(),
            'active_supp_sal' => 'active',
            'activePerso' => "active",
        ]);
    }

    /**
     * @Route("/enseignant/piece/update/{idSalle}", name="majPiece")
     */
    public function updatePiece($idSalle, Request $request)
    {
		$em = $this->getDoctrine()->getManager();
        $sal = $em->getRepository(Salle::class)->find($idSalle);
        if($sal!=null){
        $lsObjet = $sal->getIdobjet();
        $autoriser = $this->getDoctrine()->getRepository(Salle::class)->autorisationPlacerCadena($idSalle);

        $form = $this->createFormBuilder()
            -> add (
               'idobjet',
                ChoiceType::class,
                array(
                    "attr" => array("class" => "form-control"),
                    'expanded' => true, //radio 
                    'multiple' => true, // plusieur croix
                    'choices' => $lsObjet,
                )
            )
            -> add ('save', SubmitType::class, array('label'=>'Supprimer le ou les objets', "attr" => array("class" => "btn btn-danger")))
            -> getForm();
        
        $form2=$this->createFormBuilder()
            -> add('nomsalle', TextType::class, array("data"=>$sal->getNomsalle(), "attr" => array("class" => "form-control")))
            -> add('descriptsalle', TextareaType::class, array("data"=>$sal->getDescriptsalle(), "attr" => array("class" => "form-control")))
            -> add('modif', SubmitType::class, array('label' => 'Mettre à jour', "attr" => array("class" => "btn btn-primary")))
            -> getForm();
        
        $form->handleRequest($request);
        $form2->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $check = $form->get('idobjet')->getData(); // la liste des selections
            foreach ($check as $cb)
            {
                $sal->removeIdobjet($cb);
                $em->flush();
            }
            return new RedirectResponse('/enseignant/piece');
        }

        if($form2->isSubmitted() && $form2->isValid())
        {
            $sal->setNomsalle($form2->get('nomsalle')->getData());
            $sal->setDescriptsalle($form2->get('descriptsalle')->getData());

            $em = $this->getDoctrine()->getManager();
            $em->persist($sal);
            $em->flush();

            return new RedirectResponse('/enseignant/piece');
        }
        return $this->render('enseignant/piece/edit.html.twig', [
            'controller_name' => 'CategorieController',
            'lsObj' => $lsObjet,
            'form' => $form->createView(),
            'active_supp_cat' => 'active',
            'mid' => $idSalle,
            'obj' => $sal,
            'form2' => $form2->createView(),
            'access' => $autoriser,
            'activePerso' => "active",
        ]);}
        return new RedirectResponse('/enseignant');
    }

    /**
     * @Route("/enseignant/piece/addObject/{idSalle}", name="ajoutObjDansSalle")
     */
    public function addObjectInRoom($idSalle, Request $request)
    {
        // Recuperation des salles selon l'id
        
        $salle = $this->getDoctrine()->getRepository(Salle::class)->find($idSalle);
        $lsCoordObjPlacer = $this->getDoctrine()->getRepository(Salle::class)->getObjContenant($idSalle);

        $lsObjet = $this->getDoctrine()->getRepository(Objet::class)->findBy(["type" => "contenant"]);
        
        $objetDejaPlacer = $salle->getIdobjet()->getValues();
        $objetDispo = $lsObjet;

        for($i = 0 ; $i < sizeof($objetDejaPlacer) ; $i++)
        {
            if (in_array($objetDejaPlacer[$i], $objetDispo))
            {
                unset($objetDispo[array_search($objetDejaPlacer[$i], $objetDispo)]);
            }
        }
        
        /**
         * Formulaire de placement d'un objet dans une salle
         *
         * Champs :
         *  - x1, x2, y1, y2 : des champs cachés correspondant aux coordonnées du
         *                      cotés haut gauche et bas droite du rectangle
         *  - idobjet : menu deroulant prenant les ID de tout les objets de la BD
         *  - size : un champs pour indiquer la taille de l'objet
         *  - saveObj : bouton de sauvegarde
         *
         * Exemple rectangle :
         *
         * (x1,y1)    -----------------
         *            |               |
         *            |               |
         *            ----------------- (x2, y2)
         */
        $form = $this->createFormBuilder()
            -> add('x1', HiddenType::class, array("attr"=>array("value"=>"")))
            -> add('x2', HiddenType::class, array("attr"=>array("value"=>"")))
            -> add('y1', HiddenType::class, array("attr"=>array("value"=>"")))
            -> add('y2', HiddenType::class, array("attr"=>array("value"=>"")))
            -> add('idobjet', EntityType::class, array(
                "attr" => array("class" => "form-control"),
                'class' => Objet::class,
                "choices" => $objetDispo,
                'choice_label' => 'nomobjet',
                
            ))
            -> add('saveObj', SubmitType::class, array("label" => "Ajouter objet", "attr" => array("class" => "btn btn-primary")))
            -> getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $objet = $form["idobjet"]->getData();

            if(! is_null($objet))
            {
                //Recuperation des coordonnéesdu rectangle créer
                $x1 = $form["x1"]->getData();
                $x2 = $form["x2"]->getData();
                $y1 = $form["y1"]->getData();
                $y2 = $form["y2"]->getData();
                $taille = 0;

                $res = $x1.",".$y1.",".$x2.",".$y2; //Coordonnée zone selectionner

                //Vrai si deux zone qui ne se coupe pas sinon faux
                $correct = $this->getDoctrine()->getRepository(Objet::class)->verifConflicZone(intval($idSalle), $res);

                if($correct)
                {
                    //Ajout de l'objet dans la salle
                    $this->getDoctrine()->getRepository(Objet::class)->placeObjectInRoom(
                        intval($idSalle),
                        $objet->getIdobjet(),
                        $res,
                        $taille
                    );

                    $this->addFlash('success', "Objet ajouté !");
                    return $this->redirectToRoute('ajoutObjDansSalle', array('idSalle' => $idSalle));
                }
            
            
            return $this->render('enseignant/piece/placerObjet.html.twig', [
                'form' => $form->createView(),
                'imageName' => $salle->getImagesalle(),
                'lsObj' => $lsCoordObjPlacer,
                'error' => "Impossible d'ajouter l'objet",
                'errortype' => 'danger',
                'activePerso' => "active",
            ]);
            }
            else
            {
                return $this->render('enseignant/piece/placerObjet.html.twig', [
                    'form' => $form->createView(),
                    'imageName' => $salle->getImagesalle(),
                    'lsObj' => $lsCoordObjPlacer,
                    'error' => "Il n'y a plus d'objet !",
                    'errortype' => 'danger',
                    'activePerso' => "active",
                ]);
            }
        }



        // Si la salle n'existe pas alors rediriger vers la page de creation de pieces
        if (!$salle)
        {
            return $this->redirectToRoute('creationPiece');
        }

        // Sinon afficher la page d'ajout d'objet dans la piece
        else
        {   
            
            return $this->render('enseignant/piece/placerObjet.html.twig', [
                'form' => $form->createView(),
                'lsObj' => $lsCoordObjPlacer,
                'imageName' => $salle->getImagesalle(),
                'activePerso' => "active",
            ]);
        }

    }

    /**
     * @Route("/enseignant/piece/addCadena/{idSalle}", name="ajoutCadenaDansSalle")
     */
    public function addCadenaInRoom($idSalle, Request $request)
    {
        // Recuperation des salles selon l'id
        
        $salle = $this->getDoctrine()->getRepository(Salle::class)->find($idSalle);
        $lsCoordObjPlacer = $this->getDoctrine()->getRepository(Salle::class)->getObjContenant($idSalle);
        $autoriser = $this->getDoctrine()->getRepository(Salle::class)->autorisationPlacerCadena($idSalle);
        
        $lsObjet = $this->getDoctrine()->getRepository(Objet::class)->findBy(["type" => "cadena"]);
        
        /**
         * Formulaire de placement d'un objet dans une salle
         *
         * Champs :
         *  - x1, x2, y1, y2 : des champs cachés correspondant aux coordonnées du
         *                      cotés haut gauche et bas droite du rectangle
         *  - idobjet : menu deroulant prenant les ID de tout les objets de la BD
         *  - size : un champs pour indiquer la taille de l'objet
         *  - saveObj : bouton de sauvegarde
         *
         * Exemple rectangle :
         *
         * (x1,y1)    -----------------
         *            |               |
         *            |               |
         *            ----------------- (x2, y2)
         */
        $form = $this->createFormBuilder()
            -> add('x1', HiddenType::class, array("attr"=>array("value"=>"")))
            -> add('x2', HiddenType::class, array("attr"=>array("value"=>"")))
            -> add('y1', HiddenType::class, array("attr"=>array("value"=>"")))
            -> add('y2', HiddenType::class, array("attr"=>array("value"=>"")))
            -> add('idobjet', EntityType::class, array(
                "attr" => array("class" => "form-control"),
                'class' => Objet::class,
                "choices" => $lsObjet,
                'choice_label' => 'nomobjet',
                
            ))
            -> add('saveObj', SubmitType::class, array("label" => "Ajouter cadena", "attr" => array("class" => "btn btn-primary")))
            -> getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            if($autoriser)
            {
                $objet = $form["idobjet"]->getData();

                if(! is_null($objet))
                {
                    //Recuperation des coordonnéesdu rectangle créer
                    $x1 = $form["x1"]->getData();
                    $x2 = $form["x2"]->getData();
                    $y1 = $form["y1"]->getData();
                    $y2 = $form["y2"]->getData();
                    $taille = 0;

                    $res = $x1.",".$y1.",".$x2.",".$y2; //Coordonnée zone selectionner

                    //Vrai si deux zone qui ne se coupe pas sinon faux
                    $correct = $this->getDoctrine()->getRepository(Objet::class)->verifConflicZone(intval($idSalle), $res);

                    if($correct)
                    {
                        //Ajout de l'objet dans la salle
                        $this->getDoctrine()->getRepository(Objet::class)->placeObjectInRoom(
                            intval($idSalle),
                            $objet->getIdobjet(),
                            $res,
                            $taille
                        );

                        $this->addFlash('success', "Cadena ajouté !");
                        return $this->redirectToRoute('majPiece', array('idSalle' => $idSalle));
                    }
                
                
                return $this->render('enseignant/piece/placerObjet.html.twig', [
                    'form' => $form->createView(),
                    'imageName' => $salle->getImagesalle(),
                    'lsObj' => $lsCoordObjPlacer,
                    'error' => "Impossible d'ajouter le cadena",
                    'errortype' => 'danger',
                    'activePerso' => "active",
                ]);
                }
                else
                {
                    return $this->render('enseignant/piece/placerObjet.html.twig', [
                        'form' => $form->createView(),
                        'imageName' => $salle->getImagesalle(),
                        'lsObj' => $lsCoordObjPlacer,
                        'error' => "Il n'y a plus de cadena !",
                        'errortype' => 'danger',
                        'activePerso' => "active",
                    ]);
                }
            }
            else
            {
                return $this->render('enseignant/piece/placerObjet.html.twig', [
                    'form' => $form->createView(),
                    'imageName' => $salle->getImagesalle(),
                    'lsObj' => $lsCoordObjPlacer,
                    'error' => "Il y a déjà un cadena !",
                    'errortype' => 'danger',
                    'activePerso' => "active",
                ]);
            }
        }



        // Si la salle n'existe pas alors rediriger vers la page de creation de pieces
        if (!$salle)
        {
            return $this->redirectToRoute('creationPiece');
        }

        // Sinon afficher la page d'ajout d'objet dans la piece
        else
        {   
            
            return $this->render('enseignant/piece/placerObjet.html.twig', [
                'form' => $form->createView(),
                'lsObj' => $lsCoordObjPlacer,
                'imageName' => $salle->getImagesalle(),
                'activePerso' => "active",
            ]);
        }
    }

    /**
     * @Route("/enseignant/piece/suppCadena/{idSalle}", name="suppCadena")
     */
    public function suppCadena($idSalle, Request $request)
    {
        $this->getDoctrine()->getRepository(Salle::class)->supprimerCadena($idSalle);
        $this->addFlash('danger', "Cadena supprimé !");
        return $this->redirectToRoute('majPiece', array('idSalle' => $idSalle));
    }

    /**
     * @Route("/enseignant/objets/addObject/{idobjet}", name="ajoutObjDansObj")
     */
    public function addObjectInObject($idobjet, Request $request)
    {
        /**
         * Formulaire de placement d'un objet dans un autre objet
         * 
         * Champs :
         *  - x1, x2, y1, y2 : des champs cachés correspondant aux coordonnées du
         *                      cotés haut gauche et bas droite du rectangle
         *  - idobjet : menu deroulant prenant les ID de tout les objets de la BD
         *  - size : un champs pour indiquer la taille de l'objet
         *  - saveObj : bouton de sauvegarde
         * 
         * Exemple rectangle :
         * 
         * (x1,y1)    -----------------
         *            |               |
         *            |               |
         *            ----------------- (x2, y2)
         */

         // Recuperation des salles selon l'id
        $obj = $this->getDoctrine()->getRepository(Objet::class)->find($idobjet);
        $lsCoordObj = $this->getDoctrine()->getRepository(Objet::class)->getCoordNameObjetInf($idobjet);
        
        if ($obj->getType() === "contenant")
        {
            $lsObjet = $this->getDoctrine()->getRepository(Objet::class)->findBy(["type" => "contenu"]);
        }
        if ($obj->getType() === "contenu")
        {
            $lsObjet = $this->getDoctrine()->getRepository(Objet::class)->findBy(["type" => "objet"]);
        }
        if ($obj->getType() === "objet")
        {
            return $this->redirectToRoute('ajoutObjet');
        }
    
        $objetDejaPlacer = $obj->getIdobjetinferieur()->getValues();
        $objetDispo = $lsObjet;

        for($i = 0 ; $i < sizeof($objetDejaPlacer) ; $i++)
        {
            if (in_array($objetDejaPlacer[$i], $objetDispo))
            {
                unset($objetDispo[array_search($objetDejaPlacer[$i], $objetDispo)]);
            }
        }

        
        $form = $this->createFormBuilder()
            -> add('x1', HiddenType::class, array("attr"=>array("value"=>"")))
            -> add('x2', HiddenType::class, array("attr"=>array("value"=>"")))
            -> add('y1', HiddenType::class, array("attr"=>array("value"=>"")))
            -> add('y2', HiddenType::class, array("attr"=>array("value"=>"")))
            -> add('idobjet', EntityType::class, array(
                "attr" => array("class" => "form-control"),
                "class" => Objet::class,
                "choices" => $objetDispo,
                "choice_label" => 'getNomobjet',
                
            ))
            -> add('saveObj', SubmitType::class, array("label" => "Ajouter l’objet", "attr" => array("class" => "btn btn-primary")))
            -> getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $objet = $form["idobjet"]->getData();

            if(! is_null($objet))
            {
                //Recuperation des coordonnéesdu rectangle créer
                $x1 = $form["x1"]->getData();
                $x2 = $form["x2"]->getData();
                $y1 = $form["y1"]->getData();
                $y2 = $form["y2"]->getData();
                $taille = 0;

                $res = $x1.",".$y1.",".$x2.",".$y2; //Coordonnée zone selectionner

                //Vrai si deux zone qui ne se coupe pas sinon faux
                $correct = $this->getDoctrine()->getRepository(Objet::class)->verifConflicZoneObject(intval($idobjet), $res);

                if($correct)
                {
                    //Ajout de l'objet dans la salle
                    $this->getDoctrine()->getRepository(Objet::class)->placeObjectInObj(
                        intval($idobjet),
                        $objet->getIdobjet(),
                        $res
                    );

                    $this->addFlash('success', "Objet ajouté !");
                    return $this->redirectToRoute('ajoutObjDansObj', array('idobjet' => $idobjet));
                }

                return $this->render('enseignant/objet/placerObjet.html.twig', [
                    'form' => $form->createView(),
                    'imageName' => $obj->getImageobjet(),
                    'lsObj' => $lsCoordObj,
                    'error' => "Impossible d'ajouter l'objet",
                    'errortype' => 'danger', 
                    'activePerso' => "active",
                ]);
            }
            else
            {
                return $this->render('enseignant/objet/placerObjet.html.twig', [
                    'form' => $form->createView(),
                    'imageName' => $obj->getImageobjet(),
                    'lsObj' => $lsCoordObj,
                    'error' => "Il n'y a plus d'objet !",
                    'errortype' => 'danger',
                    'activePerso' => "active",
                ]);
            }
        }

        
        
        // Si la salle n'existe pas alors rediriger vers la page de creation de pieces
        if (!$obj)
        {
            return $this->redirectToRoute('ajoutObjet');
        }

        // Sinon afficher la page d'ajout d'objet dans la piece
        else
        {
            return $this->render('enseignant/objet/placerObjet.html.twig', [
                'form' => $form->createView(),
                'lsObj' => $lsCoordObj,
                'imageName' => $obj->getImageobjet(),
                'activePerso' => "active",
            ]);
        }
    }
    
     /**
     * @Route("/enseignant/genererDm/{idDevoir}", name="majObjet")
     */
    public function genererDm($idDevoir,Request $request)
    {
        return 0;
    }

    /**
     * Recuperation des objets par type
     * 
     * @Route("/enseignant/objets/", name="afficheObjets")
     */
    public function afficheObjets(Request $request)
    {
        $listObjectTypeObjet = $this->getDoctrine()->getRepository(Objet::class)->findBy(
            ["type" => "objet"]
        );
        $listObjectTypeContenant = $this->getDoctrine()->getRepository(Objet::class)->findBy(
            ["type" => "contenant"]
        );
        $listObjectTypeContenu = $this->getDoctrine()->getRepository(Objet::class)->findBy(
            ["type" => "contenu"]
        );
        $listObjectTypeCadena = $this->getDoctrine()->getRepository(Objet::class)->findBy(
            ["type" => "cadena"]
        );

        $form = $this->createFormBuilder()
        -> add ('delete', SubmitType::class, array('label'=>'Supprimer les objets', "attr" => array("class" => "btn btn-danger")))
        -> add ('ajout', SubmitType::class, array('label'=>'Ajouter un objet', "attr" => array("class" => "btn btn-danger")))
        -> getForm();
        $form->handleRequest($request);
        if ($form->getClickedButton() && 'delete' === $form->getClickedButton()->getName()) {
            
            return new RedirectResponse('/enseignant/objets/remove');
            }
        if ($form->getClickedButton() && 'ajout' === $form->getClickedButton()->getName()) {
                return new RedirectResponse('/enseignant/objets/add');
            }
        return $this->render('enseignant/objet/index.html.twig', array(
            'title' => "Les objets",
            'listObjectTypeObjet' => $listObjectTypeObjet,
            'listObjectTypeContenant' => $listObjectTypeContenant,
            'listObjectTypeContenu' => $listObjectTypeContenu,
            'listObjectTypeCadena' => $listObjectTypeCadena,
            'form' => $form->createView(),
            'activePerso' => "active",
        ));
    }

    /**
     * Fonction permettant l'ajout d'un objet
     * 
     * @Route("/enseignant/objets/add", name="ajoutObjet")
     */
    public function addObjet(Request $request)
    {
       $NouvelleObjet=new Objet();
       $form=$this->createFormBuilder($NouvelleObjet)
            -> add('nomobjet', TextType::class, array("attr" => array("class" => "form-control")))
            -> add('type', ChoiceType::class, array(
				'choices'  => array(
				'contenant' => 'contenant',
				'contenu' => 'contenu',
                'objet' => 'objet',
                'cadena' => 'cadena',),))
            -> add('imageobjet', FileType::class, array("attr" => array( "id" => "gI", "class" => "input-file", "onchange" => "previewFile()", "accept" => ".jpg, .png, .jpeg", "max-file-size" => "200KO")))
            -> add('save', SubmitType::class, array('label' => 'Enregistrer', "attr" => array("class" => "btn btn-primary")))
            -> getForm();
       $form->handleRequest($request);
       if($form->isSubmitted() && $form->isValid())
        {
            $NouvelleObjet = $form->getData();

            $newPic = "pics".date("Ymdgis").".jpg";
            $directory = "../public/picsStorage/objets";
            $file = $form['imageobjet']->getData();
            $file->move($directory, $newPic);

            $NouvelleObjet->setImageobjet($newPic);

            $em = $this->getDoctrine()->getManager();
            $em->persist($NouvelleObjet);
            $em->flush();
			return $this->redirectToRoute('afficheObjets');
        }
        return $this->render('enseignant/objet/addObjet.html.twig', array(
            'title' => "Ajout d'un objet",
            'form' => $form->createView(),
            'active_add_cat' => 'active',
            'activePerso' => "active",
        ));
    }

    /**
     * Modification d'un objet
     * 
     * @Route("/enseignant/objets/update/{idobjet}", name="majObjet")
     */
    public function updateObjet($idobjet, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $obj = $em->getRepository(Objet::class)->find($idobjet);
        if($obj!=null){
        $lsObjet = $obj->getIdobjetinferieur();

        $form = $this->createFormBuilder()
            -> add (
               'idobjet',
                ChoiceType::class,
                array(
                    "attr" => array("class" => "form-control"),
                    'expanded' => true, //radio 
                    'multiple' => true, // plusieur croix
                    'choices' => $lsObjet,
                )
            )
            -> add ('save', SubmitType::class, array('label'=>'Supprimer le ou les objets', "attr" => array("class" => "btn btn-danger")))
            -> getForm();
        
        $form2=$this->createFormBuilder()
            -> add('nomobjet', TextType::class, array("data"=>$obj->getNomobjet() ,"attr" => array("class" => "form-control")))
            -> add('type', ChoiceType::class, array(
                "attr" => array("class" => "form-control"),
				'choices'  => array(
				'contenant' => 'contenant',
				'contenu' => 'contenu',
                'objet' => 'objet',
                'cadena' => 'cadena',),"data"=>$obj->getType()))
            -> add('modif', SubmitType::class, array('label' => 'Mettre à jour', "attr" => array("class" => "btn btn-primary")))
            -> getForm();
        
        $form->handleRequest($request);
        $form2->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $check = $form->get('idobjet')->getData(); // la liste des selections
            foreach ($check as $cb)
            {
                $obj->removeIdobjetinferieur($cb);
                $em->flush();
            }
            return new RedirectResponse('/enseignant/objets');
        }

        if($form2->isSubmitted() && $form2->isValid())
        {
            $obj->setType($form2->get('type')->getData());
            $obj->setNomobjet($form2->get('nomobjet')->getData());

            $em = $this->getDoctrine()->getManager();
            $em->persist($obj);
            $em->flush();

            return new RedirectResponse('/enseignant/objets');
        }
        return $this->render('enseignant/objet/edit.html.twig', [
            'controller_name' => 'CategorieController',
            'lsCat' => $lsObjet,
            'form' => $form->createView(),
            'active_supp_cat' => 'active',
            'mid' => $idobjet,
            'obj' => $obj,
            'form2' => $form2->createView(),
            'activePerso' => "active",
        ]);
        
        }
        return new RedirectResponse('/enseignant');
    }

    /**
     * Supression d'un objet
     * 
     * @Route("/enseignant/objets/remove", name="suppObjet")
     */
    public function removeObjet(Request $request)
    {
        $lsObjet = $this->getDoctrine()->getRepository(Objet::class)->findAll();

        $form = $this->createFormBuilder()
            -> add (
               'idobjet',
                EntityType::class,
                array(
                    'class' => Objet::class, //pour la categorie
                    'expanded' => true, //radio 
                    'multiple' => true, // plusieur croix
                    'choice_label' =>
                    function($lsObjet) { return $lsObjet->getIdobjet();}   
                )
            )
            -> add ('save', SubmitType::class, array('label'=>'Supprimer les objets sélectionnés', "attr" => array("class" => "btn btn-danger")))
            -> getForm();
        
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $check = $form->get('idobjet')->getData(); // la liste des selections
            foreach ($check as $cb)
            {
				$this->getDoctrine()->getRepository(Objet::class)->SupprimerObjetRequet($cb->getIdobjet());
                
            }
            return new RedirectResponse('/enseignant/objets');
        }
        return $this->render('enseignant/objet/supprimerObjet.html.twig', [
            'controller_name' => 'CategorieController',
            'lsCat' => $lsObjet,
            'form' => $form->createView(),
            'active_supp_cat' => 'active',
            'activePerso' => "active",
        ]);
    }

    /**
     * @Route("/enseignant/enigme/add", name="ajoutEnigme")
     */
    public function addEnigme(Request $request)
    {   
        $eng = new Enigme();
        

        $form = $this->createFormBuilder($eng)
            -> add('nomEnigme', TextType::class, array("attr" => array("class" => "form-control", "placeholder" => "Nom de l'enigme")))
            -> add('maxPoint', IntegerType::class, array("attr" => array("class" => "form-control", "placeholder" => "Nombre de point maximum")))
            -> add('solution', TextareaType::class, array("attr" => array("class" => "form-control", "placeholder" => "Solution de l'enigme")))
            -> add('question', TextareaType::class, array("attr" => array("class" => "form-control", "placeholder" => "Question de l'enigme")))
            -> add('niveau', EntityType::class, array(
                "attr" => array("class" => "form-control"),
                "class" => Niveau::class,
                "choice_label" => function ($lsNiveau) { return $lsNiveau->getNiveau(); }
            ))
            -> add('idmat', EntityType::class, array(
                "attr" => array("class" => "form-control"),
                "class" => Matiere::class,
                "choice_label" => function ($lsMat) { return $lsMat->getNommat(); }
            ))
            -> add('create', SubmitType::class, array("label" => "Ajouter Enigme", "attr" => array("class" => "btn btn-primary")))
            -> getForm();
        
            $form->handleRequest($request);
        

            if ($form->isSubmitted() && $form->isValid())
            {
                
                $eng = $form->getData();

                $em = $this->getDoctrine()->getManager();
                $em -> persist($eng);
                $em->flush();
                return new RedirectResponse('/enseignant/enigme/gestion');
            }
        return $this->render('enseignant/enigme/creationEnigme.html.twig', [
            'form' => $form->createView(),
            'activePerso' => "active",]);
        
    }

    /**
     * @Route("/enseignant/enigme/update/{iden}", name="majEnigme")
     */
    public function updateEnigme($iden,Request $request)
    {   
        $Enigme = $this->getDoctrine()->getRepository(Enigme::class)->find($iden);

        $form = $this->createFormBuilder($Enigme)
            -> add('nomEnigme', TextType::class, array("attr" => array("class" => "form-control", "placeholder" => "Nom de l'enigme")))
            -> add('maxPoint', IntegerType::class, array("attr" => array("class" => "form-control", "placeholder" => "Nombre de point maximum")))
            -> add('solution', TextareaType::class, array("attr" => array("class" => "form-control", "placeholder" => "Solution de l'enigme")))
            -> add('question', TextareaType::class, array("attr" => array("class" => "form-control", "placeholder" => "Question de l'enigme")))
            -> add('niveau', EntityType::class, array(
                "attr" => array("class" => "form-control"),
                "class" => Niveau::class,
                "choice_label" => function ($lsNiveau) { return $lsNiveau->getNiveau(); }
            ))
            -> add('idmat', EntityType::class, array(
                "attr" => array("class" => "form-control"),
                "class" => Matiere::class,
                "choice_label" => function ($lsMat) { return $lsMat->getNommat(); }
            ))
            -> add('create', SubmitType::class, array("label" => "modifier Enigme", "attr" => array("class" => "btn btn-primary")))
            -> getForm();
        
            $form->handleRequest($request);
        

            if ($form->isSubmitted() && $form->isValid())
            {
                
                $eng = $form->getData();

                $em = $this->getDoctrine()->getManager();
                $em -> persist($eng);
                $em->flush();
                return new RedirectResponse('/enseignant/enigme/gestion');
            }
        return $this->render('enseignant/enigme/modificationEnigme.html.twig', [
            'form' => $form->createView(),
            'activePerso' => "active",]);
        
    }


    /**
     * @Route("/enseignant/enigme/gestion", name="gestEnigme")
     */
    public function gestionEnigme(Request $request)
    {   
        $lsEnigme = $this->getDoctrine()->getRepository(Enigme::class)->findAll();

        $form = $this->createFormBuilder()
            -> add (
                'idenigme',
                EntityType::class,
                array(
                    'class' => Enigme::class,
                    'expanded' => true,
                    'multiple' => true,
                    'choice_label' =>
                    function($lsEnigme) { return $lsEnigme->getIdenigme();}
                )
            )
            -> add ('delete', SubmitType::class, array('label'=>'Supprimer les énigme sélectionnées', "attr" => array("class" => "btn btn-danger")))
            -> add ('ajout', SubmitType::class, array('label'=>'Ajouter une énigme', "attr" => array("class" => "btn btn-danger")))
            
            -> getForm();

        $form->handleRequest($request);

        
        if ($form->getClickedButton() && 'delete' === $form->getClickedButton()->getName()) {
            
            $check = $form->get('idenigme')->getData();
            $em = $this->getDoctrine()->getManager();
            foreach ($check as $cb)
            {
                $this->getDoctrine()->getRepository(Enigme::class)->removeEnigme($cb->getIdEnigme());
            }
            return new RedirectResponse('/enseignant/enigme/gestion');
            }
        if ($form->getClickedButton() && 'ajout' === $form->getClickedButton()->getName()) {
                return new RedirectResponse('/enseignant/enigme/add');
            }
    
        return $this->render('enseignant/enigme/gestionEnigme.html.twig', [
            'lsEnigme' => $lsEnigme,
            'form' => $form->createView(),
            'activePerso' => "active",
        ]);
    }

    /**
     * @Route("/enseignant/eleve/gestion", name="gestEleve")
     */
    public function gestionEleve(Request $request)
    {   
        $lsEleve = $this->getDoctrine()->getRepository(Eleve::class)->findAll();

        $form = $this->createFormBuilder()
            -> add (
                'netu',
                EntityType::class,
                array(
                    'class' => Eleve::class,
                    'expanded' => true,
                    'multiple' => true,
                    'choice_label' =>
                    function($lsEleve) { return $lsEleve->getNetu();}
                )
            )
            -> add ('delete', SubmitType::class, array('label'=>'Supprimer les étudiants sélectionnés', "attr" => array("class" => "btn btn-danger")))
            -> add ('ajout', SubmitType::class, array('label'=>'Ajouter un / des étudiant(s)', "attr" => array("class" => "btn btn-danger")))
            -> add ('pass', SubmitType::class, array('label' => 'Réinitialiser le mot de passe de l’étudiant', "attr" => array("class" => "btn btn-danger")))
            -> getForm();

        $form->handleRequest($request);

        
        if ($form->getClickedButton() && 'delete' === $form->getClickedButton()->getName()) {
            
            $check = $form->get('netu')->getData();
            $em = $this->getDoctrine()->getManager();
            foreach ($check as $cb)
            {
                $this->getDoctrine()->getRepository(Eleve::class)->removeEleve($cb->getNetu());
            }
            return new RedirectResponse('/enseignant/eleve/gestion');
            }
        
            if ($form->getClickedButton() && 'pass' === $form->getClickedButton()->getName()) {
                $check = $form->get('netu')->getData();
                $em = $this->getDoctrine()->getManager();

                foreach ($check as $cb) {
                    $passuncrypt = $this->generatePassword();
                    $person = $cb->getIdpers();

                    $person->setMdp(hash("sha512", $passuncrypt));
                    $em->flush();

                    $this->sendMail(
                        "PolyEscape", 
                        $person->getEmail(),
                        "
                        <h2>Bonjour</h2>
                        <p>
                            Votre enseignant vous a modifié votre mot de passe !
                            Pour vous connectez il vous suffit d'aller sur le site web et vous connectez en tant qu'étudiant via vos identifiants.
                        </p>
                        <ul>
                            <li> <b> Identifiant : </b>".$person->getEmail()."</li>
                            <li> <b> Mot de passe : </b>".$passuncrypt."</li>
                        </ul>
                        "
                    );

                    $this->addFlash('success', $person->getNom()." ".$person->getPrenom()." a son mot de passe modifier !");
                    
                }
                return new RedirectResponse('/enseignant/eleve/gestion');
            }

        if ($form->getClickedButton() && 'ajout' === $form->getClickedButton()->getName()) {
                return new RedirectResponse('/enseignant/importStudent');
            }
    
        return $this->render('enseignant/gestionEtu.html.twig', [
            'lsEleve' => $lsEleve,
            'form' => $form->createView(),
            'activeGestion' => "active",
        ]);
    }

    /**
     * @Route("/etudiant/partie/{idDm}", name="jouepartie")
     */
    public function jouepartie($idDm, Request $request)
    {
        $dm = $this->getDoctrine()->getRepository(Dm::class)->find($idDm);
        $s = $this->getDoctrine()->getRepository(Salle::class)->getRoomByDM($idDm);
        $nbSalle = $this->getDoctrine()->getRepository(Salle::class)->getNbSalle($idDm);
        
        $em = $this->getDoctrine()->getManager();

        $dm = $em->getRepository(Dm::class)->find($idDm);
        $s = $this->getDoctrine()->getRepository(Salle::class)->getRoomByDM($idDm);
        
        //id salle courant : $s[0]["idsalle"]
        
        
        if (sizeof($s) > 0)
        {
            $autorisePlacerCadena = $this->getDoctrine()->getRepository(Salle::class)->autorisationPlacerCadena($s[0]["idsalle"]);
            $cont = $em->getRepository(Contient::class)->findOneBy([
                "iddm" => $dm->getIddm(),
                'idsalle' => $s[0]["idsalle"],
            ]);
            $room = $this->getDoctrine()->getRepository(Salle::class)->find($s[0]["idsalle"]);
            $lsObj = $room->getIdobjet();
            
            $link = $this->getDoctrine()->getRepository(EstLierA::class)->findBy([
                'idsalle' =>  intval($s[0]["idsalle"]),
                'iddm' => intval($idDm)
            ]);
           
            $form = $this->createFormBuilder();
            
            $cpt = 0;
            foreach ($link as $enigme) {
                $cpt+=1;
                dump($enigme);
                $form->add($enigme->getIdenigme()->getIdenigme(), TextAreaType::class, array("label"=>$enigme->getIdenigme()->getNomenigme(), "attr" => array("style"=>"margin:5px;","class" => "form-control")));
            }

            $form-> add ('save', SubmitType::class, array('label'=>'Valider', "attr" => array("style"=>"margin:5px;","class" => "btn btn-primary")));
            $recup = $form->getForm();
            $recup->handleRequest($request);
            if($recup->isSubmitted() && $recup->isValid())
            {
                if ($dm->getIdgr()->getIdpush() != $this->getUser()->getNetu())
                {
                    return $this->render('partie/index.html.twig', [
                        'dm' => $dm,
                        'image' => $s[0]["imagesalle"],
                        'salle' => $s,
                        'lsObj' => $lsObj,
                        'form' => $recup->createView(),
                        'nbSalle' => $nbSalle,
                        'error' => "Vous n'êtes pas le chef de groupe !",
                        'allRoom' => $dm->getIddev()->getIdsalle(),
                        'idSalleCourant' => $s[0]["idsalle"],
                        'autoriser' => $autorisePlacerCadena,
                        'error_type' => "danger",
                        'cadena' => $cont->getIdcadena(),
                        'activeHome' => "active",
                    ]);
                }
                else
                {
                    $cadena = $cont->getIdcadena();
                    $valEnigme = 0;
                    foreach ($link as $enigme)
                    {
                        $rep = $recup->get($enigme->getIdenigme()->getIdenigme())->getData();
                        if($rep != $enigme->getIdenigme()->getSolution())
                        {
                            if($cont->getIdcadena()->getNbessai() < $cont->getIdcadena()->getNbessaimax()-1)
                            {
                                $cont->getIdcadena()->setNbessai($cont->getIdcadena()->getNbessai() + 1);
                                $em->flush();   
                            }
                            else
                            {
                                $cont->getIdcadena()->setEtatc(TRUE);
                                $em->flush();   
                                return $this->redirectToRoute('jouepartie',  ['idDm' => $dm->getIddm()]);
                            }

                            
                            return $this->render('partie/index.html.twig', [
                                'dm' => $dm,
                                'image' => $s[0]["imagesalle"],
                                'salle' => $s,
                                'lsObj' => $lsObj,
                                'form' => $recup->createView(),
                                'allRoom' => $dm->getIddev()->getIdsalle(),
                                'idSalleCourant' => $s[0]["idsalle"],
                                'nbSalle' => $nbSalle,
                                'autoriser' => $autorisePlacerCadena,
                                'error' => "Reponse invalide !",
                                'error_type' => "danger",
                                'cadena' => $cont->getIdcadena(),
                                
                            ]);
                        }
                        $nbMaxPoint = $dm->getIddev()->getMaxpoint();
                        $maxpointEnigme = $enigme->getIdenigme()->getMaxpoint();
                        $coef = (20 * $maxpointEnigme) / $nbMaxPoint;
                        $valEnigme += ($maxpointEnigme) * (($cadena->getNbessaimax() - $cadena->getNbessai()) / $cadena->getNbessaimax());
                        $ajoutPoint = ($coef * $valEnigme) / $maxpointEnigme;
                    }
                    
                    $dm->setNote($dm->getNote() + $ajoutPoint);
                    $cont->getIdcadena()->setEtatc(TRUE);
                    $em->flush();
                    return $this->redirectToRoute('jouepartie',  ['idDm' => $dm->getIddm()]);
                }
                
            }

            return $this->render('partie/index.html.twig', [
                'dm' => $dm,
                'image' => $s[0]["imagesalle"],
                'idSalleCourant' => $s[0]["idsalle"],
                'salle' => $s,
                'lsObj' => $lsObj,
                'allRoom' => $dm->getIddev()->getIdsalle(),
                'form' => $recup->createView(),
                'autoriser' => $autorisePlacerCadena,
                'cadena' => $cont->getIdcadena(),
                'nbSalle' => $nbSalle,
               
            ]);
        }
        if(! is_null($dm))
        {
            // $valPts = $dm->getNote();
            // $resultatFinal = ($dm->getNote() * 20) / $dm->getIddev()->getMaxpoint();
            // $dm->setNote($resultatFinal);
            // $em->flush();

            $this->addFlash('success', "Vous avez fini la partie avec ".$dm->getNote()." pts !");
            return $this->redirectToRoute('pageAccueil');
        }        

        $this->addFlash('danger', "La partie est déjà fini !");
        return $this->redirectToRoute('pageAccueil');
        
    }

    /**
     * @Route("/etudiant/partie/{idDm}/{idContenant}", name="jouerPartieContenant")
     */
    public function jouerPartieContenant($idDm, $idContenant)
    {
        $lsContenu = $this->getDoctrine()->getRepository(Objet::class)->getInfObject($idContenant);
        $contenant = $this->getDoctrine()->getRepository(Objet::class)->find($idContenant);

        return $this->render('partie/contenant.html.twig', [
            'image' => $contenant->getImageobjet(),
            'contenu' => $lsContenu,
            'iddm' => $idDm,
            'idc' => $idContenant,
           
        ]); 
    }

    /**
     * @Route("/etudiant/partie/{idDm}/{idContenant}/{idContenu}", name="jouerPartieContenantContenu")
     */
    public function jouerPartieContenu($idDm, $idContenant, $idContenu)
    {
        $lsObjet = $this->getDoctrine()->getRepository(Objet::class)->getInfObject($idContenu);
        $contenu = $this->getDoctrine()->getRepository(Objet::class)->find($idContenu);
        $s = $this->getDoctrine()->getRepository(Salle::class)->getRoomByDM($idDm);
        $link = $this->getDoctrine()->getRepository(EstLierA::class)->findBy([
            'idsalle' =>  intval($s[0]["idsalle"]),
            'iddm' => intval($idDm)
        ]);
        
        return $this->render('partie/contenu.html.twig', [
            'image' => $contenu->getImageobjet(),
            'objet' => $lsObjet,
            'lsenigme' => $link,
            'iddm' => $idDm,
            'idc' => $idContenant,
         
        ]); 
    }

   
    public function changeInformationDevoir($idDevoir)
    {
	    /**
        * Mise à jour nombre de groupe et étudiants dans un devoir
        */			
		
		$devoir=$this->getDoctrine()->getRepository(Devoir::class)->find($idDevoir);
		$em = $this->getDoctrine()->getManager();
		
		//Nombre de groupe pour ce devoir
        $cpt=$this->getDoctrine()->getRepository(Groupe::class)->getNbGroupe($idDevoir);
                
        //Nombre de étudiants pour ce devoir
        $nbEtudiant=$this->getDoctrine()->getRepository(Groupe::class)->getNbEtudiantMoyenne($idDevoir);
                
        $devoir->setNbpersonnegroupe($nbEtudiant);
        $devoir->setNbgroupe($cpt[0]['cpt']);
                
        //Sauvegarde dans la BD
        $em->persist($devoir);
		$em->flush();
		}   

    /**
     * @Route("/enseignant/genere/etudiant/{idDevoir}/{nbessaimax}", name="gerereEtudiant")
     */
    public function gerereEtudiant($idDevoir,$nbessaimax,Request $request)
    {
	    /**
        * Génerer les dms d'un devoir selon des étudiants
        */			
        	
		//$devoir=$this->getDoctrine()->getRepository(Devoir::class)->find($idDevoir);
        
        //la liste de tous les étudiants de la BD
        $lsEtudiant = $this->getDoctrine()->getRepository(Eleve::class)->findAll();
        
         /**
         * Formulaire de génération de Dm par étudiant
         *
         * Champs :
         *  - netu : checkbox correspondant à tous les étudiants
         *  - nbGroupe : champs texte correspondant au nombre d'étudiants par groupe
         *  - save : bouton de géneration des Dms
         */        
        $form = $this->createFormBuilder()
            -> add('nbGroupe',NumberType::class, array("attr" => array("class" => "form-control")))
            -> add (
                'netu',
                EntityType::class,
                array(
                    'class' => Eleve::class,
                    'expanded' => true,
                    'multiple' => true,
                    'choice_label' =>
                    function($lsEtudiant) { return  $this->getDoctrine()->getRepository(Personne::class)->find($lsEtudiant->getIdpers())->getNom();}))
            -> add ('save', SubmitType::class, array('label'=>'Générer', "attr" => array("class" => "btn btn-danger")))
            -> getForm();
            $form->handleRequest($request);
            
            if($form->isSubmitted() && $form->isValid())
            {
				 //Recuperation des informations du formulaire de generation dms par étudiant
                $nbGroupe = $form->get('nbGroupe')->getData();
                $check = $form->get('netu')->getData();
                
                // la liste des étudiants selectionnés
                $liste=array();
                foreach($check as $e){
					$m=array();
					$m['netu']=$e->getNetu();
					$liste[]=$m;
					}
               
                // Géneration automatique des groupes  
                $this->ChargerGroupe($liste,$nbGroupe,$idDevoir,"");
                
                //Géneration aléatoire des Dms
                $this->FaireAleatoireDM($idDevoir,$nbessaimax);
                
                //Mise à jour nombre de groupe et étudiants dans un devoir
				$this->changeInformationDevoir($idDevoir);
          
                return new RedirectResponse('/enseignant');
        }
        return $this->render('enseignant/genererEtudiant.html.twig', [
            'controller_name' => '',
            'lsEtudiant' => $lsEtudiant,
            'form' => $form->createView(),
            'active_supp_cat' => 'active',
            'activeDM' => "active",
        ]);
    }
    

    /**
     * @Route("/enseignant/genere/groupe/{idDevoir}/{nbessaimax}", name="gerereGroupe")
     */
    public function genereGroupe($idDevoir,$nbessaimax,Request $request)
    {
	    /**
        * Génerer les dms d'un devoir selon des groupe
        */		
		$devoir=$this->getDoctrine()->getRepository(Devoir::class)->find($idDevoir);
        
        //la liste de tout les groupes de la BD
        $lsGroupe = $this->getDoctrine()->getRepository(Groupe::class)->findAll();
        
         /**
         * Formulaire de génération de Dm par groupe
         *
         * Champs :
         *  - idgr : checkbox correspondant à tous les groupe
         *  - save : bouton de géneration des Dms
         */
        $form = $this->createFormBuilder()
            -> add (
                'idgr',
                EntityType::class,
                array(
                    'class' => Groupe::class,
                    'expanded' => true,
                    'multiple' => true,
                    'choice_label' =>
                    function($lsGroupe) { return  $lsGroupe->getNomgroupe();}))
            -> add ('save', SubmitType::class, array('label'=>'Générer', "attr" => array("class" => "btn btn-danger")))
            -> getForm();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
		    //Recuperation des informations du formulaire de generation dms par groupe
			$em = $this->getDoctrine()->getManager();
            $check = $form->get('idgr')->getData();
            
            // Pour chaque groupe on va créer un Dm  
            foreach($check as $gr) 
            {
				
				//Création d'un Dm
				$dm=new DM();
				$dm->setIdgr($gr);
				$dm->setIddev($this->getDoctrine()->getRepository(Devoir::class)->find($idDevoir));
				
				//Sauvegarde dans la BD
				$em->persist($dm);
				$em->flush();
				
				//Géneration d'une partie pour ce groupe
				$this->FaireAleatoire($dm,$idDevoir,$nbessaimax);
            }
            // Mise à jour du nombre de groupe(s) et étudiants d'un devoir
            $this->changeInformationDevoir($idDevoir);
            
            return new RedirectResponse('/enseignant');
        }
        return $this->render('enseignant/genererGroupe.twig', [
            'controller_name' => '',
            'lsGroupe' => $lsGroupe,
            'form' => $form->createView(),
            'active_supp_cat' => 'active',
            'activeDM' => "active",
        ]);
   
    }
    
    public function CreationUnGroupe($lsEtudiant,$cpt,$idDevoir,$nbetudiant,$classe)
    {
		/**
        * Création d'un groupe
        * lsEtudiant : la liste des étudiants
        * cpt : la positon de l'étudiant actuel dans la liste lsEtudiant
        * idDevoir : l'id du devoir
        * nbetudiant : le nombre étudiants par groupe
        * classe : la classe du groupe si plusieur classes alors c'est une chaine vide
        */
		
		$em = $this->getDoctrine()->getManager();
		//Création d'un groupe
		$groupe=new Groupe();
	    $groupe->setIdpush($lsEtudiant[$cpt]["netu"]);
	    $dm=new Dm();
	    $em->persist($dm);
	    $em->flush();
	    $j=0;
	        
		//Remplir le groupe d'étudiants
	    for ($j=0; $j<$nbetudiant; $j++) 
	    {
		    $eleve=$lsEtudiant[$cpt];
		    $e=$this->getDoctrine()->getRepository(Eleve::class)->find($eleve["netu"]);
			$groupe->addNetu($e);
		    $cpt++;
         }
         
          $dm->setIdgr($groupe);
          $dm->setIddev($this->getDoctrine()->getRepository(Devoir::class)->find($idDevoir));
          $em->persist($groupe);
            
          //Sauvegarde dans la BD du Dm
            $em->persist($dm);
            $em->flush();
            
            //Sauvegarde dans la BD du groupe
            
            $groupe->setNomgroupe("Groupe ".$classe." ".$groupe->getIdgr());
            $em->persist($groupe);
            $em->flush();
            return array($cpt,$groupe->getIdgr());
		}
    
    
    public function ChargerGroupe($lsEtudiant,$nbetudiant,$idDevoir,$classe): void
    {
	    /**
        * Création des groupes automatiquement
        * lsEtudiant : la liste des étudiants
        * idDevoir : l'id du devoir
        * nbetudiant : le nombre étudiants par groupe
        * classe : la classe du groupe si plusieur classes alors c'est une chaine vide
        */
        
        //Dernier groupe creer et -1 : il n'y a pas de groupe creer
		$listeGroupe=-1;
		
		// pour indiquer l'étudiant actuel de la lsEtudiant
		$cpt=0;
		
		$nbEleveRestant=sizeof($lsEtudiant)%$nbetudiant;
		$nbGroupe=(int)((sizeof($lsEtudiant)-$nbEleveRestant)/$nbetudiant);
		
		$em = $this->getDoctrine()->getManager();
		
		//Création de tous les groupe
		for ($i=0; $i<$nbGroupe; $i++) 
		{
			//Création d'un groupe
			$res=$this->CreationUnGroupe($lsEtudiant,$cpt,$idDevoir,$nbetudiant,$classe);
			$cpt=$res[0];
			$listeGroupe=$res[1];
        }
        //Si on n'a pas assez pour faire un groupe alors on ajouter dans le groupe précedant ou on recreer un groupe
       if($nbEleveRestant !=0){
		   //Si il  reste qu'une personne alors on l'ajout au dernier groupe
        if($nbEleveRestant==1 and $listeGroupe!=-1){
			$g=$this->getDoctrine()->getRepository(Groupe::class)->find($listeGroupe);
		    $eleve=$lsEtudiant[$cpt];
		    $e=$this->getDoctrine()->getRepository(Eleve::class)->find($eleve["netu"]);
		    $g->addNetu($e);
		    $cpt++;
		    
		    //Sauvegarde dans le BD
		    $em->persist($g);
			}
		else{
			//Creer un groupe avec les personnes restantes
			$cpt=$this->CreationUnGroupe($lsEtudiant,$cpt,$idDevoir,$nbEleveRestant,$classe);
			}
		}
	
	   $em->flush();
    }
    
   

    public function FaireAleatoire($Dm,$idDevoir,$nbessaimax): void
    {
		/**
        * Géneration aléatoire d'une partie pour un Dm
        */
		$devoir=$this->getDoctrine()->getRepository(Devoir::class)->find($idDevoir);
		
		// la liste des emigne pour ce devoir
		$listeEmigne=$devoir->getArrayListe();
		
		// la liste des salles pour ce devoir
		$listeSalle=$devoir->getArraySalle();
		
		$nbSalle=sizeof($listeSalle);
		
		// le nombre d'emigne par cadena : 
		$nbEmigneParCadena=sizeof($listeEmigne)/sizeof($listeSalle);
		
		//Mélange aléatoirement les emignes
		shuffle($listeEmigne);
		
		$listeCadena=array();
		
		//Compter le nombre emigne
		$cpt=0;
		$em = $this->getDoctrine()->getManager();
		
		// Pour chaque salle on va creer un cadena
		for ($i=0; $i<$nbSalle; $i++) 
		{
			//Création d'un cadena
			$cadena=new Cadena();
			$cadena->setNbessaimax(intval($nbessaimax));
			$cadena->setEtatc(0);
			$cadena->setNbessai(0);
			
			// Sauvegarde du cadena dans la BD
			$em->persist($cadena);
			$em->flush();
			
			//Ajout un salle 
			$listeCadena[$listeSalle[$i]->getIdsalle()]=array();
		    $salle=$this->getDoctrine()->getRepository(Salle::class)->find($listeSalle[$i]);
			
			//liste de tous les objets de cette salle
			$listeObject=$this->listeObjetSalle($listeSalle[$i]);
			
			//Mélange aléatoirement la liste des objects
			shuffle($listeObject);
			
			//Permet de lier le cadena à une salle à un Dm
			$contient = new Contient();
			$contient->setIdsalle($salle);
			$contient->setIdcadena($cadena);
			$contient->setIddm($Dm);
			
			//Sauvegarder dans la BD
			$em->persist($contient);
			$em->flush();
			
			//Pour chaque tour on va associer emigne à un object pour un cadena
			for ($j=0 ;$j<$nbEmigneParCadena;$j++)
			{
				
				$listeCadena[$i][$j]=$listeEmigne[$cpt];
				
				//Permet de lier une emigne à object à salle à un dm
				$EstLieA =new EstLierA();
				$EstLieA->setIdenigme($listeEmigne[$cpt]);
				$EstLieA->setIdsalle($salle);
				$o=$this->getDoctrine()->getRepository(Objet::class)->find($listeObject[$j]['idobjet']);
				$EstLieA->setIdobjet($o);
				$EstLieA->setIddm($Dm);
				
				//Sauvegarder dans la BD
				$em->persist($EstLieA);
				$em->flush();
				$cpt++;
			}
		}
	
    }
    public function FaireAleatoireDM($idDevoir,$nbessaimax): void
    {
		/**
        * Géneration aléatoire des parties pour tous les Dms d'un devoir
        */
      
        //la liste des DMs pour ce devoir  
	    $liste=$this->getDoctrine()->getRepository(Dm::class)->findBy(["iddev"=>$idDevoir]);
        foreach ($liste as $dm)
        {
			//Géner aléatoirement une partie pour ce DM
            $this->FaireAleatoire($dm,$idDevoir,$nbessaimax);
		}
            
	
    }
   
   /**
     * @Route("/enseignant/genere/classe/{idDevoir}/{nbessaimax}", name="gerereClasse")
     */
    public function genereClasse($idDevoir,$nbessaimax,Request $request)
    {
	    /**
        * Génerer les dms d'un devoir selon des classes
        * Donc il y a pour chaque classe le création des plusieur groupes
        */
        
		$devoir=$this->getDoctrine()->getRepository(Devoir::class)->find($idDevoir);
		
		//la liste des classe présents dans la BD
        $lsClasse = $this->getDoctrine()->getRepository(Eleve::class)->ListeClasse();
        
         /**
         * Formulaire de génération de Dm par classe
         *
         * Champs :
         *  - nbGroupe : un champ texte correspondant au nombre d'eleves par groupe
         *  - classe : checkbox correspondant à tous les classes
         *  - save : bouton de géneration des Dms
         */
        $form = $this->createFormBuilder()
            -> add('nbGroupe',NumberType::class, array("attr" => array("class" => "form-control")))
            -> add('classe', ChoiceType::class, array(
		        'expanded' => true,
                'multiple' => true,
				'choices'  => $lsClasse,))
             -> add ('save', SubmitType::class, array('label'=>'Générer', "attr" => array("class" => "btn btn-danger")))
             -> getForm();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
			//Recuperation des informations du formulaire de generation dms par classe
			$em = $this->getDoctrine()->getManager();
            $check = $form->get('classe')->getData();
            $nbGroupe = $form->get('nbGroupe')->getData();
            foreach($check as $gr) 
            {
				//la liste de étudiants de cette classe
		        $lsE = $this->getDoctrine()->getRepository(Eleve::class)->ListeEtudiantParClasse($gr);
				
				//~ dump($lsE);
				//~ $m=m;
				
				//Création des groupes selon la liste "$lsE"
				$this->ChargerGroupe($lsE ,$nbGroupe,$idDevoir,$gr);
            }
            
            // Création des Dm pour ces groupe
            $this->FaireAleatoireDM($idDevoir,$nbessaimax);
            
            //Mise à jour du nombre de groupes et étudiants dans un devoir
            $this->changeInformationDevoir($idDevoir);
            
		    return new RedirectResponse('/enseignant');
        }
        return $this->render('enseignant/genererClasse.html.twig', [
            'controller_name' => '',
            'lsClasse' => $lsClasse,
            'form' => $form->createView(),
            'active_supp_cat' => 'active',
            'activeDM' => "active",
        ]);
    }
     
   /**
     * @Route("/enseignant/genere/{idDevoir}/{type}/{nbessaimax}", name="gerere")
     */
    public function genere($idDevoir,$type,$nbessaimax,Request $request)
    {
		/**
        * Génerer les dms d'un devoir selon l'option choisi
        * 1: il faut choisir des étudiants 
        * 2: il faut choisir des groupes
        * 3: il faut choisi des classes
        */
		if($type==1)
		{
			return $this->RedirectToRoute('gerereEtudiant',array("idDevoir"=>$idDevoir,"nbessaimax"=>$nbessaimax));
		}
		else if ($type==2)
		{
			return $this->RedirectToRoute('gerereGroupe',array("idDevoir"=>$idDevoir,"nbessaimax"=>$nbessaimax));
		}
		else
		{
			return $this->RedirectToRoute('gerereClasse',array("idDevoir"=>$idDevoir,"nbessaimax"=>$nbessaimax));
		}
    }
    /**
     * @Route("/enseignant/add/group/", name="AddGroupe")
     * 
     */
    public function AddGroupe(Request $request)
    {
        /**
        * Creation d'un groupe dans la BD
        */

		$groupe= new Groupe();
		
		// la liste des étudiants
        $lsEtudiant = $this->getDoctrine()->getRepository(Eleve::class)->findAll();
        
        /**
         * Formulaire de creation d'un groupe
         *
         * Champs :
         *  - monGroupe : un champ texte correspondant au nom du groupe
         *  - netu : checkbox correspondant à tous les étudiants
         *  - EtudiantPush : menu deroulant correspondant à tous les étudiants pour désigner le chef du groupe
         *  - save : bouton de sauvegarde
         */
        $form = $this->createFormBuilder()
            -> add('monGroupe',TextType::class, array("attr" => array("class" => "form-control")))
            -> add (
                 'netu',
                  EntityType::class,
                  array(
                      'class' => Eleve::class,
                      'expanded' => true,
                      'multiple' => true,
                      
                      'choice_label' =>
                  function($lsEtudiant) { return  $this->getDoctrine()->getRepository(Personne::class)->find($lsEtudiant->getIdpers())->getNom();}))
			-> add('EtudiantPush',TextType::class, array("attr" => array("class" => "form-control","hidden"=>"")))
            -> add ('save', SubmitType::class, array('label'=>'Ajouter', "attr" => array("class" => "btn btn-danger")))
            
            -> getForm();
            $form->handleRequest($request);
            
            if($form->isSubmitted())
            {
			    //Recuperation des informations du formulaire d'ajout d'un groupe
			    $em = $this->getDoctrine()->getManager();
                $nomGroupe = $form->get('monGroupe')->getData();
                
                $etudiantPushId =$form->get('EtudiantPush')->getData();
                $check = $form->get('netu')->getData();
				
                foreach($check as $c)
                {
				    $groupe->addNetu($c);
				}
				
			    $groupe->setNomgroupe($nomGroupe);
				$groupe->setIdpush(intval($etudiantPushId));
			
			    //Enregistre les informations dans la BD SQL
                $em->persist($groupe);
		        $em->flush();
          
                return new RedirectResponse('/enseignant/groupe/remove');
        }
        return $this->render('enseignant/Groupe/addGroupe.html.twig', [
            'controller_name' => '',
            'lsEtudiant'=> $lsEtudiant,
            'form' => $form->createView(),
            'active_supp_cat' => 'active',
            'activeGestion' => "active",
        ]);
    }
    /**
     * @Route("/enseignant/groupe/update/{idgroupe}", name="updateGroupe")
     */
    public function updateGroupe($idgroupe,Request $request)
    {
        /**
        * Mise à jour d'un groupe dans la BD
        */
        $groupe=$this->getDoctrine()->getRepository(Groupe::class)->find($idgroupe);
		
        // la liste des étudiants qui n'appartient pas à ce groupe
        $lsParGroupe=$this->getDoctrine()->getRepository(Eleve::class)->getPasEtudiantGroupe($idgroupe);
		
        
        // la liste des étudiants qui appartient à ce groupe 
        $lsGroupe=$this->getDoctrine()->getRepository(Groupe::class)->getEtudiantGroupe($groupe);
        
        // la liste des étudiants qui appartient à ce groupe sans le chef du groupe
        $lsGroupeSansPush=$this->getDoctrine()->getRepository(Groupe::class)->getEtudiantGroupeSansPush($groupe);
        /**
         * Formulaire de modification de ce groupe avec les informations
         *
         * Champs :
         *  - monGroupe : un champ texte correspondant au nom du groupe
         *  - EtudiantPush : menu deroulant correspondant à tous les étudiants pour désigner le chef du groupe
         *  - save : bouton de sauvegarde
         */
         $form1 = $this->createFormBuilder()
            -> add('nomgroupe', TextType::class, array("attr" => array("class" => "form-control",)))
            -> add('idpush', ChoiceType::class, array(
                   "attr" => array("class" => "form-control"),
				    'choices'  => $lsGroupe,))    
            -> add ('save', SubmitType::class, array('label'=>'Modifier', "attr" => array("class" => "btn btn-danger")))
            -> getForm();
        /**
         * Formulaire de ajouter et supprimer un étudiant de ce groupe
         *
         * Champs :
         *  - GroupePasEtudiant : menu deroulant correspondant tous le étudaints de la BD sauf aux étudiants de ce groupe
         *  - ajouter : bouton d'ajout d'un étudiants de ce groupe
         *  - EtudiantGroupe : menu deroulant correspondant aux étudiants de ce groupe
         *  - supprimer : bouton de suppression  d'un étudiants de ce groupe
         */            
        $form = $this->createFormBuilder()
            -> add('EtudiantGroupe', ChoiceType::class, array(
                  "attr" => array("class" => "form-control"),
				  'choices'  => $lsGroupe,))
		    -> add('GroupePasEtudiant', ChoiceType::class, array(
                  "attr" => array("class" => "form-control"),
				  'choices'  => $lsParGroupe,))
            -> add ('ajouter', SubmitType::class, array('label'=>'Ajouter', "attr" => array("class" => "btn btn-danger")))
            -> add ('supprimer', SubmitType::class, array('label'=>'Supprimer', "attr" => array("class" => "btn btn-danger")))
            -> getForm();
            
        $form->handleRequest($request);
        $form1->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
         //~ dump($groupe);
        if ($form->getClickedButton() && 'ajouter' === $form->getClickedButton()->getName()) 
        {
			 //Ajouter un étudiant au groupe
			
			 //Recuperation l'informations du formulaire d'ajout d'un étudiant à un groupe
			 $check = $form->get('GroupePasEtudiant')->getData();
			 $eleve=$this->getDoctrine()->getRepository(Eleve::class)->find($check);
			 //Modifie le groupe et Save dans la BD
			 $groupe->addNetu($eleve);
			 
             $em->persist($groupe);
			 $em->flush(); 
			 
			 
             return $this->RedirectToRoute('updateGroupe',array("idgroupe"=>$idgroupe));
         }
         if ($form->getClickedButton() && 'supprimer' === $form->getClickedButton()->getName()) 
         {
			 //Supprimer un étudiant au groupe
	
			 //Recuperation l'informations du formulaire de l'étudiant à supprimer du groupe
	         $check = $form->get('EtudiantGroupe')->getData();
			 $eleve=$this->getDoctrine()->getRepository(Eleve::class)->find($check);
			 
			 //Modifie le groupe et Save dans la BD
			 $groupe->removeNetu($eleve);
             $em->persist($groupe);
			 $em->flush(); 
             return $this->RedirectToRoute('updateGroupe',array("idgroupe"=>$idgroupe));
         }
         
         if($form1->getClickedButton() && 'save' === $form1->getClickedButton()->getName())
         {
			 //Mise à jour d'un groupe
			 
			 //Recuperation l'informations du formulaire du groupe
		     $non= $form1->get('nomgroupe')->getData();
			 $etudiantPush =$form1->get('idpush')->getData();
            
             //Modifie le groupe et Save dans la BD
			 $groupe->setIdpush($etudiantPush);
             $groupe -> setNomgroupe($non);
             $em -> persist($groupe);
             $em->flush();
    
		 }     
         return $this->render('enseignant/Groupe/UpdateGroupe.html.twig', [
            'controller_name' => '',
            'groupe' => $groupe,
            'form' => $form->createView(),
            'form1' => $form1->createView(),
            'active_supp_cat' => 'active',
            'activeGestion' => "active",
        ]);
    }

    /**
     * @Route("/enseignant/groupe/remove", name="suppGroupe")
     */
    public function removeGroupe(Request $request)
    {
	    /**
        * Suppression d'un groupe dans la BD
        */
        
        $lsEtudiant = $this->getDoctrine()->getRepository(Groupe::class)->findAll();
           
		// la liste de tous les chefs des groupes
        $lsEtudiantsPush=$this->getDoctrine()->getRepository(Groupe::class)->getEtudiantPush();
        
        
         /**
         * Formulaire pour supprimer des groupes
         *
         * Champs :
         *  - groupe : checkbox correspondant tous les groupes de la BD
         *  - save : bouton pour sauvegarde
         */ 
        $form = $this->createFormBuilder()
            -> add (
                'groupe',
                 EntityType::class,
                 array(
                     'class' => Groupe::class,
                     'expanded' => true,
                     'multiple' => true,
                     'choice_label' =>
                     function($lsEtudiant) { return $lsEtudiant->getIdgr(); }))
            -> add ('save', SubmitType::class, array('label'=>'Supprimer les groupes sélectionnés', "attr" => array("class" => "btn btn-danger")))
            -> getForm();
            $form->handleRequest($request);
            
            if($form->isSubmitted() && $form->isValid())
            {
			     //Recuperation les informations du formulaire des groupe à supprimer
			    $check = $form->get('groupe')->getData();
                
                foreach($check as $c)
                {
					//Supprimer le groupe et renvoie un false si le groupe n'a pas été supprimer
				    $ok=$this->getDoctrine()->getRepository(Groupe::class)->deleteGroupe($c->getIdgr());
				    $groupeok=$this->getDoctrine()->getRepository(Groupe::class)->find($c);
				    if (! $ok)
				    {
						//Ajout le groupe à la liste des groupe qui ne sont pas supprimer
					    $this->addFlash('success', $groupeok->getNomgroupe()." n'a pas pu être supprimé");
					}
					else{
						 $this->addFlash('success', $groupeok->getNomgroupe()." a été supprimé");
						}
					
				}
				
			return new RedirectResponse('/enseignant/groupe/remove');
        
        }
        return $this->render('enseignant/Groupe/deleteGroupe.html.twig', [
            'controller_name' => '',
            'lsEtudiantsPush'=>$lsEtudiantsPush,
            'lsEtudiant'=> $lsEtudiant,
            'form' => $form->createView(),
            'active_supp_cat' => 'active',
            'activeGestion' => "active",
        ]);
    }
	
	
	
	public function listeObjetSalle($idsalle)
    {	
		/**
        * Sa liste tous les objets d'une  salle
        */
		$liste=array();
		$Salle=$this->getDoctrine()->getRepository(Salle::class)->find($idsalle);
		
		//la liste des objets de la salle au premier plan
		foreach ($Salle->getIdobjet() as $d)
		{
	        // la liste de tous les objets inferieur à cette objet
			$lsObjet = $this->getDoctrine()->getRepository(Salle::class)->getObjet($d->getIdobjet());
			foreach ($lsObjet as $o)
			{
				$liste[$o["idobjet"]]=$o;
			}
		}
	return $liste;
    }
	
	 public function NbObjet($idsalle)
    {	
		/**
        * Le nombre objets pour cette seule
        */
        $cpt=0;
		$Salle=$this->getDoctrine()->getRepository(Salle::class)->find($idsalle);
		
		//la liste des objets de la salle au premier plan
		foreach ($Salle->getIdobjet() as $d)
		{
			// le nombre objets  pour cette objets
			$lsObjet = $this->getDoctrine()->getRepository(Salle::class)->getObjet($d->getIdobjet());
			$cpt+=(sizeof($lsObjet));
		}
	return $cpt;
    }
    
   public function Liste()
    {	
		/**
        * Un tuple avec le liste de toutes les salles et le liste du nombre d'objets pour chaque salle
        */
		$liste=array();
        $liste2=array();
		$Salle=$this->getDoctrine()->getRepository(Salle::class)->findall();
		foreach ($Salle as $s)
		{
            $l=array();
            $l2=array();
            
            //le nombre d'objets pour cette salle
            $cpt=$this->NbObjet($s->getIdsalle());
            
            //la salle
            $l['salle']=$s; 
            
			$l2['cpt']=$cpt;
            $liste[]=$l;
            $liste2[]=$l2;
		}
	return array($liste, $liste2);
    }

    /**
     * @Route("/enseignant/statistiquesEleves", name="statEleves")
     */
    public function statStudent()
    {	
		return 0;
    }
    
     /**
     * @Route("/enseignant/devoir/lancer/{iddevoir}", name="devoirLancer")
     */
    public function devoirLancer($iddevoir)
    {
	   $em = $this->getDoctrine()->getManager();
	   $devoir=$this->getDoctrine()->getRepository(Devoir::class)->find($iddevoir);
	   $devoir->setStatut("En cours");
       $em->persist($devoir);
       $em->flush();

       $listDM = $this->getDoctrine()->getRepository(DM::class)->findBy(['iddev' => $iddevoir]);
       foreach ($listDM as $DM)
       {
            $group = $DM->getIdgr();
            foreach ($group->getNetu() as $etu)
            {
                if($etu->getNetu() == $group->getIdpush())
                {
                    $this->sendMail(
                        "PolyEscape", 
                        $etu->getIdpers()->getEmail(),
                        "
                        <h2>Ouverture d'une partie !</h2>
                        <p>
                            Ce message vous préviens qu'une nouvelle partie est ouvert, et que vous êtes le chef de groupe de cette partie !
                        </p>
                        <p>
                            <b>Information de la partie :</b>
                            <ul>
                                <li>Titre de la partie : ".$devoir->getNomdev()."</li>
                                <li>Professeur responsable : ".$devoir->getIdprof()->getIdpers()->getNom()." ".$devoir->getIdprof()->getIdpers()->getPrenom()."</li>
                                <li>
                                    Etudiant de votre groupe :
                                    ".$this->printListStudent($group->getNetu())."
                                </li>
                            </ul>
                        </p>
                        "
                    );
                }
                else
                {
                    $this->sendMail(
                        "PolyEscape", 
                        "polyescape@yahoo.com", 
                        $etu->getIdpers()->getEmail(),
                        "
                        <h2>Ouverture d'une partie !</h2>
                        <p>
                            Ce message vous préviens qu'une nouvelle partie est ouvert !
                        </p>
                        <p>
                            <b>Information de la partie :</b>
                            <ul>
                                <li>Titre de la partie : ".$devoir->getNomdev()."</li>
                                <li>Professeur responsable : ".$devoir->getIdprof()->getIdpers()->getNom()." ".$devoir->getIdprof()->getIdpers()->getPrenom()."</li>
                                <li>
                                    Etudiant de votre groupe :
                                    ".$this->printListStudent($group->getNetu())."
                                </li>
                            </ul>
                        </p>
                        "
                    );
                }
            }
       }
       // TODO : Send email
       return new RedirectResponse('/enseignant');
    }
     /**
     * @Route("/enseignant/devoir/delete/{iddevoir}", name="devoirDelete")
     */
    public function devoirDelete($iddevoir)
    {
		/**
        * Suppression d'un Devoir dans la BD
        */
	    $em = $this->getDoctrine()->getManager();
	    $devoir=$this->getDoctrine()->getRepository(Devoir::class)->find($iddevoir);
	    
	    //Change le statut du devoir=>les étudiants n'ont plus access à ce Devoir
	    $devoir->setStatut("Fermer");
	    
	    //Suppression d'un devoir en supprimant tout les dépendances
        $this->getDoctrine()->getRepository(Devoir::class)->getDevoirSupprimer($iddevoir);
        
        //Save dans la BD
        $em->persist($devoir);
        $em->flush();
        return new RedirectResponse('/enseignant');
    }
    
    
     /**
     * @Route("/enseignant/devoir/update/{iddevoir}", name="devoirUpDate")
     */
     
    public function devoirUpDate($iddevoir,Request $request)
    {
		/**
        * Mise à jour d'un Devoir dans la BD
        */
	   	$devoir=$this->getDoctrine()->getRepository(Devoir::class)->find($iddevoir);
	   	//~ dump($devoir->getDate());
	   	//~ $m=l;
	   	//la liste des groupes de ce Devoir
        $lsDM=$this->getDoctrine()->getRepository(Groupe::class)->getGroupeDevoir($iddevoir);
        
        //la liste des groupes qui n'appatient pas à ce devoir
        $lsParGroupe=$this->getDoctrine()->getRepository(Groupe::class)->getGroupeParDevoir($iddevoir);
        
        /**
         * Formulaire de modification d'un Devoir
         *
         * Champs :
         *  - date : chanmps date correspondant à la date de fin de ce devoir
         *  - nomDev : chanmps date correspondant au nom de devoir
         *  - save : bouton pour sauvegarder
         */ 
        $form1 = $this->createFormBuilder()
            -> add('date', DateType::class, [
                'widget' => 'single_text',
                // this is actually the default format for single_text
                'format' => 'yyyy-MM-dd',])
            -> add('nomDev', TextType::class, array("attr" => array("class" => "form-control")))
            -> add ('save', SubmitType::class, array('label'=>'Modifier', "attr" => array("class" => "btn btn-danger")))
            -> getForm();
        /**
         * Formulaire de ajouter et supprimer un groupe de ce devoir
         *
         * Champs :
         *  - GroupeParDevoir : menu deroulant correspondant tous les groupes de la BD sauf aux groupe de ce devoir
         *  - ajouter : bouton d'ajout d'un groupe à ce devoir
         *  - GroupeDevoir : menu deroulant correspondant aux groupes de ce devoir
         *  - supprimer : bouton de suppression  d'un groupe à ce devoir
         */ 
        $form = $this->createFormBuilder()
            -> add('GroupeParDevoir', ChoiceType::class, array(
                "attr" => array("class" => "form-control"),
				'choices'  => $lsParGroupe,))
		    -> add('GroupeDevoir', ChoiceType::class, array(
                "attr" => array("class" => "form-control"),
				'choices'  => $lsDM,))
            -> add ('ajouter', SubmitType::class, array('label'=>'Ajouter', "attr" => array("class" => "btn btn-danger")))
            -> add ('supprimer', SubmitType::class, array('label'=>'Supprimer', "attr" => array("class" => "btn btn-danger")))
            -> getForm();
         $form->handleRequest($request);
         $form1->handleRequest($request);
         $em = $this->getDoctrine()->getManager();
         
         if ($form->getClickedButton() && 'ajouter' === $form->getClickedButton()->getName()) 
         {
			 //Ajout d'un groupe
			 
			 //Recuperation l'informations du formulaire du devoir du groupe à ajouter	 
		     $check = $form->get('GroupeParDevoir')->getData();
			 $g=$this->getDoctrine()->getRepository(Groupe::class)->find($check);
			 
			 // Création d'un nouveau Dm pour ce groupe
			 $dm =new Dm();
			 
			 // Ajout de ce Dm dans la BD
			 $dm->setIddev($devoir);
			 $dm->setIdgr($g);
			 $em->persist($dm);
			 $em->flush();
			 
			 //Récuperer le mb essai Max dans la BD pour ce devoir
			 $nbessaiMax=$this->getDoctrine()->getRepository(Devoir::class)->getDevoirNbEssaiMax($iddevoir);
             
             //Génération aléatoire de la partie pour ce Dm
             $this->FaireAleatoire($dm,$iddevoir,$nbessaiMax);
             
            //Mise à jour du nombre de groupes et étudiants dans un devoir
            $this->changeInformationDevoir($iddevoir);
                
           return $this->RedirectToRoute('devoirUpDate',array("iddevoir"=>$iddevoir));
         }
         if ($form->getClickedButton() && 'supprimer' === $form->getClickedButton()->getName()) 
         {
			 //Suppression d'un groupe
			 
			 //Recuperation l'informations du formulaire du devoir du groupe à supprimer			 
			 $check = $form->get('GroupeDevoir')->getData();
			 $dm=$this->getDoctrine()->getRepository(Dm::class)->findBy(["iddev"=>$iddevoir,"idgr"=>$check]);
			 
			 //Suppression du groupe et de ses dépendances comme son Dm et sa partie
			 $this->getDoctrine()->getRepository(Devoir::class)->getDevoirSupprimerUnDM($dm[0]->getIddm());
             
            //Mise à jour du nombre de groupes et étudiants dans un devoir
            $this->changeInformationDevoir($iddevoir);
             
             return $this->RedirectToRoute('devoirUpDate',array("iddevoir"=>$iddevoir));
         }
         if($form1->getClickedButton() && 'save' === $form1->getClickedButton()->getName())
         {
			 //Mise à jour d'un groupe
			 
			 //Recuperation l'informations du formulaire du devoir 
			 $non= $form1->get('nomDev')->getData();
             $dated= $form1->get('date')->getData();
            
             //Mise à jour et sauvegarder dans le BD
             $devoir -> setNomdev($non);
             $devoir -> setDate($dated);
             $em -> persist($devoir);
             $em->flush();
             return $this->RedirectToRoute('devoirUpDate',array("iddevoir"=>$iddevoir));
		 }  
         return $this->render('enseignant/updateDevoir.html.twig', [
            'controller_name' => '',
            'devoir' => $devoir,
            'form' => $form->createView(),
            'form1' => $form1->createView(),
            'active_supp_cat' => 'active',
            'activeDM' => "active",
        ]);
    }
    /**
     * @Route("/enseignant/creationDM", name="creationDM")
     */
    public function creationDm(Request $request){
        $lsEnigme = $this->getDoctrine()->getRepository(Enigme::class)->findAll();  
        $lsSalle = $this->Liste()[0]; 
        $l2 = $this->Liste()[1];     
        $lsMat = $this->getDoctrine()->getRepository(Matiere::class)->findAll();

        $form = $this->createFormBuilder()
            -> add('nbEnigme', IntegerType::class, array("attr" => array("class" => "form-control","value"=>"1", "placeholder" => "nombre d'enigme par salle")))
            -> add('date_created', DateType::class,[
                'widget' => 'single_text',
                // this is actually the default format for single_text
                'format' => 'yyyy-MM-dd',
            ])
            -> add('essaiCadena', IntegerType::class, array("attr" => array("class" => "form-control", "placeholder" => "Nombre d'essai maximum pour un cadena")))
            -> add('nomDM', TextType::class, array("attr" => array("class" => "form-control", "placeholder" => "nom du DM")))
            -> add (
                'idenigme',
                EntityType::class,
                array(
                    'class' => Enigme::class,
                    'expanded' => true,
                    'multiple' => true,
                    'choice_label' =>
                    function($lsEnigme) { return $lsEnigme->getIdenigme();}
                )
            )
            -> add (
                'idsalle',
                ChoiceType::class,
                array(
                    'expanded' => true,
                    'multiple' => true,
                    'choices' => $lsSalle,
                )
            )
            -> add('type', ChoiceType::class, array(
                "attr" => array("class" => "form-control"),
				'choices'  => array(
                
				'classe' => 3,
				// 'groupe' => 2,
                'etudiant' => 1,),))

            -> add('idmat', EntityType::class, array(
                    "attr" => array("class" => "form-control test", "data-table" => "table-enigme"),
                    "class" => Matiere::class,
                    "choice_label" => function ($lsMat) { return $lsMat-> getNommat(); }
            ))
            
            -> add('valide', SubmitType::class, array("label" => "Valider", "attr" => array("class" => "btn btn-primary")))
            -> getForm();
                
            $form->handleRequest($request);
            
            if ($form->isSubmitted() && $form->isValid()) {
                $check = $form->get('idenigme')->getData();
                $check2 = $form->get('idsalle')->getData();
                $check3 = $form->get('nbEnigme')->getData();
                if(sizeof($check) == sizeof($check2)*$check3){
					$em = $this->getDoctrine()->getManager();
                    $dev= new Devoir();
                    $idP = $this -> getUser();
                    $nd= $form->get('nomDM')->getData();
                    $dated= $form->get('date_created')->getData();
                    $eng =  $form->get('idenigme')->getData();
                    $sallet =  $form->get('idsalle')->getData();
                    $mat = $form->get('idmat')->getData();
					$nbessaimax = $form->get('essaiCadena')->getData();
					$type=$form->get('type')->getData();
					
                    foreach ($eng as $cb){
                        $dev -> addIdenigme($cb);
                    }
                    foreach ($sallet as $cb){
                        $dev -> addIdsalle($cb); 
                    }
                    $dev -> setIdprof($idP);
                    $dev -> setNomdev($nd);
                    $dev -> setDate($dated);
                    $dev -> setStatut('En developpement');
                    $dev -> setIdmat($mat);
                    $em -> persist($dev);
                    $em->flush();
                    $cpt=1;
                    foreach ($sallet as $cb){
						$li=$this->getDoctrine()->getRepository(Devoir::class)->changerOrdersalle($cb->getIdsalle(),$dev->getIddev(),strval($cpt));
                        $cpt++;
                    }
                    $pt=$this->getDoctrine()->getRepository(Devoir::class)->NbPoint($dev->getIddev());
					$dev ->setMaxpoint($pt);
					$em -> persist($dev);
                    $em->flush();
                return     $this->RedirectToRoute('gerere',array("idDevoir"=>$dev->getIddev(),"nbessaimax"=>$nbessaimax,"type"=>$type));
              //  return new RedirectResponse('/enseignant/enigme/gestion');
                }
               else{
				  
				   if (sizeof($check) <= sizeof($check2)*$check3){
				   $this->addFlash('success', "Il manque ".abs(sizeof($check)-sizeof($check2)*$check3)." énigme(s)");}
				   else{
					$this->addFlash('success', "Il faut supprimer ".abs(sizeof($check)-sizeof($check2)*$check3)." énigme(s)");
					   }
				   }
				   
            }
            
            return $this->render('enseignant/DM/creationDm.html.twig', [
                'lsEnigme' => $lsEnigme,
                'lsSalle' => $lsSalle,
                'l2' => $l2,
                'form' => $form->createView(),
                'activeDM' => "active",
            ]);
    }


    // public function listeObjetSalle($idsalle)
    // {    
    //     $liste=array();
    //     $Salle=$this->getDoctrine()->getRepository(Salle::class)->find($idsalle);
    //     foreach ($Salle->getIdobjet() as $d)
    //     {
    //         $lsObjet = $this->getDoctrine()->getRepository(Salle::class)->getObjet($d->getIdobjet());
    //         foreach ($lsObjet as $o)
    //         {
    //             $liste[]=$o;
    //         }
    //     }
    //     return $liste;
    // }

    /**
     * @Route("/enseignant/stat/{iddev}", name="statEtu")
     */
    public function statEtu($iddev, Request $request)
    {
        $listeDevoir=$this->getDoctrine()->getRepository(Devoir::class)->getClassementEleve($iddev);
        $getDev = $this->getDoctrine()->getRepository(Devoir::class)->find($iddev);
        
        $PDFgenerator = $this->createFormBuilder()
            -> add ('pdf', SubmitType::class, array('label'=>'Exporter en PDF', "attr" => array("class" => "btn btn-danger")))
            -> getForm();
        
        $CSVgenerator = $this->createFormBuilder()
            -> add ('csv', SubmitType::class, array('label'=>'Exporter en CSV', "attr" => array("class" => "btn btn-danger")))
            -> getForm();

        $PDFgenerator->handleRequest($request);
        $CSVgenerator->handleRequest($request);

        if($CSVgenerator->isSubmitted() && $CSVgenerator->isValid())
        {
            $csv = \League\Csv\Writer::createFromFileObject(new \SplTempFileObject());
            $csv->insertOne(['Nom', 'Prenom', 'Classe', 'Moyenne']);
            foreach ($listeDevoir as $eleve) {
                $csv->insertOne([$eleve['nom'], $eleve['prenom'], $eleve['classe'], $eleve['note']]);
            }
            $csv->output($getDev->getNomdev().'.csv');
            die;
        }

        if($PDFgenerator->isSubmitted() && $PDFgenerator->isValid())
        {
            $res = "
            <table class='table table-bordered table-striped'>
            <thead>
            <tr>
                <th scope='col'>Nom</th>
                <th scope='col'>Prenom</th>
                <th scope='col'>Classe</th>
                <th scope='col'>Moyenne</th>
            </tr>
            </thead>
            <tbody>
            ";

            foreach ($listeDevoir as $eleve) {
                dump($eleve);
                $res = $res."
                <tr>
                    <td> ".$eleve['nom']." </td>
                    <td> ".$eleve['prenom']." </td>
                    <td> ".$eleve['classe']." </td>
                    <td> ".$eleve['note']." </td>
                </tr>
                ";
            }

            $res = $res."
                        </tbody>
            </table>
            ";

            $html2pdf = new Html2Pdf();
            $html2pdf->writeHTML($res);
            $html2pdf->output();
        }
        
        return $this->render('etudiant/statEtu.html.twig', [
          'controller_name' => 'EtudiantController',
          'listeDevoir' => $listeDevoir,
          'formPDF' => $PDFgenerator->createView(),
          'formCSV' => $CSVgenerator->createView(),
          

      ]);
    }
    
    
        /**
     * @Route("/enseignant/statchanger/{iddev}", name="statEtuChanger")
     */
    public function statEtuChanger($iddev, Request $request)
    {
        $listeDevoir=$this->getDoctrine()->getRepository(Devoir::class)->getClassementEleve($iddev);
        $listenb=$this->getDoctrine()->getRepository(Groupe::class)->getEudiantParGroupe($iddev);
        $getDev = $this->getDoctrine()->getRepository(Devoir::class)->find($iddev);
        return $this->render('etudiant/statEtuCorrection.html.twig', [
          'controller_name' => 'EtudiantController',
          'listeDevoir' => $listeDevoir,
          'listenb' =>$listenb,

      ]);
    }
    
    
      /**
     * @Route("/enseignant/changerNote/{iddm}/{iddev}/{note}", name="changerNote")
     */
    public function changerNote($iddm, $iddev,$note)
    {
       
        $dm = $this->getDoctrine()->getRepository(Dm::class)->find($iddm);
		$em = $this->getDoctrine()->getManager();
		$dm->setNote($note);
		
		$em->persist($dm);
		$em->flush();
		return $this->RedirectToRoute('statEtuChanger',array("iddev"=>$iddev));

    }
    
    /**
     * @Route("/enseignant/importStudent", name="importStud")
     */
    public function importStud(Request $request)
    {
        $form=$this->createFormBuilder()
            -> add('etuCSV', FileType::class, array("attr" => array("class" => "form-control-file", "accept" => ".csv")))
            -> add('save', SubmitType::class, array('label' => 'Importer', "attr" => array("class" => "btn btn-primary")))
            -> getForm();

        $form2=$this->createformBuilder()
            -> add('nom',TextType::class, array("attr" => array("class" => "form-control", "placeholder"=>"Nom")))
            -> add('prenom',TextType::class, array("attr" => array("class" => "form-control", "placeholder"=>"Prenom")))
            -> add('email',EmailType::class, array("attr" => array("class" => "form-control", "placeholder"=>"Email")))
            -> add('classe',TextType::class, array("attr" => array("class" => "form-control", "placeholder"=>"Classe")))
            -> add('datec',TextType::class, array("attr" => array("class" => "form-control", "placeholder"=>"Date de promo")))
            -> add('ajouter', SubmitType::class, array('label' => 'Ajouter', "attr" => array("class" => "btn btn-primary")))
            -> getForm();

        $form->handleRequest($request);
        $form2->handleRequest($request);
            
        if ($form->isSubmitted() && $form->isValid()) 
        {
            $newCSV = "csv".date("Ymdgis").".csv";
            $directory = "../uploads";
            $file = $form['etuCSV']->getData();
    
            
            $file->move($directory, $newCSV);
            

            return $this->redirectToRoute('importStudConfig', array('file' => $newCSV));

            // return $this->render('enseignant/importStudent.html.twig', [
            //     'form2' => $form2->createView(),
            // ]);

        }

        if ($form2->isSubmitted() && $form2->isValid())
        {
            $this->addStudent(
                $form2['nom']->getData(),
                $form2['prenom']->getData(),
                $form2['email']->getData(),
                $form2['classe']->getData(),
                $form2['datec']->getData()
            );

            

            $this->addFlash('success', "Etudiant ajouté !");
            return $this->redirectToRoute('importStud');
        }

        return $this->render('enseignant/importStudent.html.twig', [
            'form' => $form->createView(),
            'form2' => $form2->createView(),
            'activeGestion' => "active",
        ]);
    }

    /**
     * @Route("/enseignant/importStudent/config/{file}", name="importStudConfig")
     */
    public function importStudConfig($file, Request $request, \Swift_Mailer $mailer)
    {
        $path = "../uploads/".$file;
            
        $reader = Reader::createFromPath($path, 'r');
        $reader->setHeaderOffset(0);
        $records = $reader->getRecords();
        $headers = $reader->getHeader();

        dump($headers);
        

        $form=$this->createFormBuilder()
            -> add (
                'nom',
                 ChoiceType::class,
                 array(
                     "attr" => array("class" => "form-control"),
                     'choices' => $headers,
                     'label' => "nom Etudiant",
                     'choice_label' => function($choiceValue, $key, $value) {return $value;}))
            -> add (
                'prenom',
                 ChoiceType::class,
                 array(
                     "attr" => array("class" => "form-control"),
                     'choices' => $headers,
                     'label' => "prenom Etudiant",
                     'choice_label' => function($choiceValue, $key, $value) {return $value;}))
            -> add (
                'email',
                 ChoiceType::class,
                 array(
                     "attr" => array("class" => "form-control"),
                     'choices' => $headers,
                     'label' => "email Etudiant",
                     'choice_label' => function($choiceValue, $key, $value) {return $value;}))
            -> add('type', ChoiceType::class, array(
                'choices'  => array(
                    'Par le CSV' => 'Par le CSV',
                    'Par saisie' => 'Par saisie',
                ),
                'expanded' => true,
                ))
            -> add (
                'classeCT',
                 ChoiceType::class,
                 array(
                     "attr" => array("class" => "form-control", "style" => "display: none"),
                     'choices' => $headers,
                     'label' => "classe Etudiant",
                     'choice_label' => function($choiceValue, $key, $value) {return $value;}))
            -> add('classeTT', TextType::class, array("attr" => array("class" => "form-control", "placeholder" => "classe", "style" => "display: none;")))
            -> add('annee', TextType::class, array("attr" => array("class" => "form-control", "placeholder" => "annee-annee")))
            -> add('submit', SubmitType::class, array('label' => 'Valider', "attr" => array("class" => "btn btn-primary")))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $modSelect = $form['type']->getData();

            $colForName = $form['nom']->getData();
            $colForFName = $form['prenom']->getData();
            $colForMail = $form['email']->getData();

            $Years = $form['annee']->getData();

            if(strcmp($modSelect,"Par le CSV") == 0)
            {
                $colForClassRoom = $form['classeCT']->getData();
                foreach ($records as $offset => $record) {
                    $this->addStudent(
                        $record[$colForName], 
                        $record[$colForFName],
                        $record[$colForMail],
                        $record[$colForClassRoom],
                        $Years
                    );
                }
            }
            else if(strcmp($modSelect,"Par saisie") == 0)
            {
                $classRoom = $form['classeTT']->getData();
                foreach ($records as $offset => $record) {
                    $this->addStudent(
                        $record[$colForName], 
                        $record[$colForFName],
                        $record[$colForMail],
                        $classRoom,
                        $Years
                    );
                }
            }
            unlink($path);

            $this->addFlash('success', "Etudiants ajoutés !");
            return $this->redirectToRoute('importStud');
        }

        return $this->render('enseignant/configImportEtu.html.twig', [
            'form' => $form->createView(),
            'activeGestion' => "active",
        ]);
    }

    /**
     * @Route("/enseignant/changerMDP", name = "changemdpProf")
     */
    public function newMDPTeacher(Request $request)
    {
        $form=$this->createFormBuilder()
            -> add('oldpassword', PasswordType::class, array("attr" => array("class" => "form-control")))
            -> add('mdpSaisie1', PasswordType::class, array("attr" => array("class" => "form-control")))
            -> add('mdpSaisie2', PasswordType::class, array("attr" => array("class" => "form-control")))
            -> add('submit', SubmitType::class, array('label' => 'Valider', "attr" => array("class" => "btn btn-primary")))
            -> getForm();
        
        $form->handleRequest($request);
        $form2=$this->createFormBuilder()
            
            -> add('name', TextType::class, array("attr" => array("class" => "form-control", "placeholder" => "Prenom")))
            -> add('surname', TextType::class, array("attr" => array("class" => "form-control", "placeholder" => "Nom")))
            -> add('submit2', SubmitType::class, array('label' => 'Valider', "attr" => array("class" => "btn btn-primary")))
            -> getForm();
        
        $form2->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $first = $form['mdpSaisie1']->getData();
            $second = $form['mdpSaisie2']->getData();
            $old = hash("sha512", $form['oldpassword']->getData());

            if(strcmp($first, $second) == 0 && strcmp($old, $this->getUser()->getIdpers()->getMdp()) == 0)
            {
                $em = $this->getDoctrine()->getManager();
                $this->getUser()->getIdpers()->setMdp(hash("sha512", $second));
                $em->flush();

                $this->sendMail(
                    "PolyEscape", 
                    $this->getUser()->getIdpers()->getEmail(),
                    "
                    <h2>Information de securite</h2>
                    <p>
                        Ce message vous prévient que votre mot de passe sur l'application PolyEscape a été changer le ".date("d/m/y")." à ".date("H:i:s")."
                    </p>
                    "
                );

                $this->addFlash('success', "Mot de passe modifier !");
                return $this->redirectToRoute('changemdpProf');
            }
            $this->addFlash('danger', "Votre saisie est incorrect");
            return $this->redirectToRoute('changemdpProf');
        }
        if($form2->isSubmitted() && $form2->isValid())
        {
            $name=$form2->get('name')->getData();
            $surname=$form2->get('surname')->getData();
            $em = $this->getDoctrine()->getManager();
            $this->getUser()->getIdpers()->setNom($surname);
            $this->getUser()->getIdpers()->setPrenom($name);
            $em->flush();

            $this->addFlash('success', "Nom et prénom modifiés");
            return $this->redirectToRoute('changemdpProf');
        }

        return $this->render('enseignant/changerMDP.html.twig', [
            'form' => $form->createView(),
            'form2' => $form2->createView(),
            'activeMDP' => "active",
        ]);
    }

    private function generatePassword() : String
    {
        $res = "";
        for ($i = 0; $i < 5; $i++)
        {
            $value = 65 + rand(0,(90-65));
            $strAsciiTab = chr($value);
            $res = $res.$strAsciiTab;
        }
        return $res;
    }

    private function addStudent($name, $firstname, $mail, $classroom, $year) : void
    {
        $em = $this->getDoctrine()->getManager();
        $p = new Personne();
        $p->setNom($name);
        $p->setPrenom($firstname);
        $p->setEmail($mail);
                
        $mdpUncrypt = $this->generatePassword();
        $p->setMdp(hash("sha512", $mdpUncrypt));

        $em->persist($p);
        $em->flush();

        $etu = new Eleve();
        $etu->setClasse($classroom);
        $etu->setDatec($year);
        $etu->setIdpers($p);

        $em->persist($etu);
        $em->flush();

        $this->sendmail(
            "PolyEscape", 
            $p->getEmail(),
            "
            <h2>Bonjour</h2>
            <p>
                Votre enseignant vous a modifié votre mot de passe !
                Pour vous connectez il vous suffit d'aller sur le site web et vous connectez en tant qu'étudiant via vos identifiants.
            </p>
            <ul>
                <li> <b> Identifiant : </b>".$p->getEmail()."</li>
                <li> <b> Mot de passe : </b>".$mdpUncrypt."</li>
            </ul>
            "
        );
    }

    private function sendMail($title, $to, $body)
    {
        // $transport = (new \Swift_SmtpTransport('smtp.mail.yahoo.com', 465))
        //     ->setUsername('polyescape@yahoo.com')
        //     ->setPassword('EVLJMescapegame')
        //     ->setEncryption('ssl');
        
        $transport = (new \Swift_SmtpTransport('smtp.univ-orleans.fr', 25));

        $mailer = new \Swift_Mailer($transport);

        $message = (new \Swift_Message($title))
            // -> setFrom("polyescape@yahoo.com")
            -> setFrom("polyescape@etu.univ-orleans.fr")
            -> setTo($to)
            -> setBody($body, "text/html");

        $mailer->send($message);

        sleep(1);
    }

    private function printListStudent($listStudent) : string
    {
        $res = " ";
        $size = sizeof($listStudent);
        $cpt = 0;
        foreach($listStudent as $student)
        {
            $cpt+=1;
            if($cpt == $size)
            {
                $res=$res.$student->getIdPers()->getNom()." ".$student->getIdPers()->getPrenom();
            }
            else
            {
                $res=$res.$student->getIdPers()->getNom()." ".$student->getIdPers()->getPrenom()." , ";
            }
        }
        return $res;
    }
    
    /**
     * @Route("/enseignant/devoir/infos/{idDevoir}", name="infosDevoirId")
     */
    public function infosDevoir($idDevoir)
    {
        $infos = $this->getDoctrine()->getRepository(Devoir::class)->getInfosDev($idDevoir);
        $listeEmignes = $this->getDoctrine()->getRepository(Devoir::class)->getEnigmesDevoir($idDevoir);
        $listeGroupes = $this->getDoctrine()->getRepository(Devoir::class)->getGroupesDev($idDevoir);
        $listeSalles =  $this->getDoctrine()->getRepository(Devoir::class)->getSallesDev($idDevoir);
        dump($listeGroupes);
        return $this->render('enseignant/afficheInfosDevoir.html.twig', [
            'controller_name' => 'EnseignantController',
            'infos' => $infos,
            'listeEnigmes' => $listeEmignes,
            'listeGroupes' => $listeGroupes,
            'listeSalles' => $listeSalles,
            
        ]);
    }
    /**
     * @Route("/enseignant/matiere/gestion", name="gestMatiere")
     */
    public function gestionMatiere(Request $request)
    {   
        $lsMatiere = $this->getDoctrine()->getRepository(Matiere::class)->findAll();

        $form = $this->createFormBuilder()
            -> add (
                'idmat',
                EntityType::class,
                array(
                    'class' => Matiere::class,
                    'expanded' => true,
                    'multiple' => true,
                    'choice_label' =>
                    function($lsMatiere) { return $lsMatiere->getIdmat();}
                )
            )
            
            -> add ('delete', SubmitType::class, array('label'=>'Supprimer les matières sélectionnées', "attr" => array("class" => "btn btn-danger")))
            -> getForm();
            
            
        $form2 = $this->createFormBuilder()
            -> add('nom',TextType::class, array("attr" => array("class" => "form-control", "placeholder"=>"Nom")))
            -> add ('ajout', SubmitType::class, array('label'=>'Ajouter une matière', "attr" => array("class" => "btn btn-danger")))
            -> getForm();
        
        $form->handleRequest($request);
        $form2->handleRequest($request);
        
        if ($form->getClickedButton() && 'delete' === $form->getClickedButton()->getName()) {
            
            $check = $form->get('idmat')->getData();
            $em = $this->getDoctrine()->getManager();
           foreach ($check as $cb)
            {
                $this->getDoctrine()->getRepository(Matiere::class)->removeMatiere($cb->getIdmat());
           }
            return new RedirectResponse('/enseignant/matiere/gestion');
            } 
        if ($form2->getClickedButton() && 'ajout' === $form2->getClickedButton()->getName()) {
            $em = $this->getDoctrine()->getManager();
            $eng = $form2->get('nom')->getData();
            $mat = new Matiere();
            $mat -> setNommat($eng);
            
            $em -> persist($mat);
            $em->flush();
            return new RedirectResponse('/enseignant/matiere/gestion');
           } 
    
        return $this->render('enseignant/gestionMatiere.html.twig', [
            'lsMatiere' => $lsMatiere,
            'form' => $form->createView(),
            'form2' => $form2 ->createView(),
            'activeGestion' => "active",
        ]);
    }
}
