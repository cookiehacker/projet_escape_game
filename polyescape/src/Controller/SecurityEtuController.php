<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Classe permettant la connexion d'un etudiant
 */
class SecurityEtuController extends AbstractController
{
    /**
     * Methode permettant la connexion de l'utilisation d'un etudiant
     * 
     * @Route("/etudiant/login", name="login_Etu")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // Recupere les erreurs lors du login s'il y en a une 
        $error = $authenticationUtils->getLastAuthenticationError();
        // Recupere l'identifiant de l'utiliseur
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/loginEtu.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * Methode permettant de ce deconnecter
     * 
     * @Route("/etudiant/logout", name="app_logout_etu")
     */
    public function logout()
    {
        throw new \Exception('Will be intercepted before getting here');
    }
}
