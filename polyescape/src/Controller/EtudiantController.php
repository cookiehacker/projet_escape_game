<?php

namespace App\Controller;

use App\Entity\Dm;
use App\Entity\Devoir;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request; #Traitement des requêtes POST / GET
use Symfony\Component\HttpFoundation\Response; #Envoie de reponse
use Symfony\Component\HttpFoundation\RedirectResponse; #Redirection vers une route
use Symfony\Component\Form\Extension\Core\Type\PasswordType; #Champ de saisie pour les mots de passe

use Symfony\Component\Form\Extension\Core\Type\SubmitType; #Bouton validation

class EtudiantController extends AbstractController
{
    /**
     * @Route("/etudiant", name="pageAccueil")
     */
    public function index()
    {
        $listeDM=$this->getDoctrine()->getRepository(Dm::class)->getDMParNetu($this->getUser()->getNetu());
        return $this->render('etudiant/etudiant2.html.twig', [
            'controller_name' => 'EtudiantController',
            'listeDevoir' => $listeDM,
            'activeHome' => "active",
        ]);
    }

    /**
     * @Route("/etudiant/profil", name="profilEtu")
     */
    public function profil()
    {
        $listeListeEleve=[];
        $listeDM=$this->getDoctrine()->getRepository(Dm::class)->getGroupMarkForStudent($this->getUser()->getNetu());
        
        foreach ($listeDM as $dm){
            $listeEleveGrp=$this->getDoctrine()->getRepository(Dm::class)->getStudentByGroup($dm['idgr']);
            array_push($listeListeEleve, $listeEleveGrp);
        }

        
        return $this->render('etudiant/profile.html.twig', [
            'controller_name' => 'EtudiantController',
            'listeDm' => $listeDM,
            'listeEleveGrp' => $listeListeEleve,
            'activeProfil' => "active",
            
        ]);
    }

    /**
     * @Route("/etudiant/changerMDP", name = "changemdpEtu")
     */
    public function newMDPStudent(Request $request)
    {
        $form=$this->createFormBuilder()
            -> add('oldpassword', PasswordType::class, array("attr" => array("class" => "form-control")))
            -> add('mdpSaisie1', PasswordType::class, array("attr" => array("class" => "form-control")))
            -> add('mdpSaisie2', PasswordType::class, array("attr" => array("class" => "form-control")))
            -> add('submit', SubmitType::class, array('label' => 'Valider', "attr" => array("class" => "btn btn-primary")))
            -> getForm();
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid())
        {
            $first = $form['mdpSaisie1']->getData();
            $second = $form['mdpSaisie2']->getData();
            $old = hash("sha512", $form['oldpassword']->getData());

            if(strcmp($first, $second) == 0 && strcmp($old, $this->getUser()->getIdpers()->getMdp()) == 0)
            {
                $em = $this->getDoctrine()->getManager();
                $this->getUser()->getIdpers()->setMdp(hash("sha512", $second));
                $em->flush();

                $this->sendMail(
                    "PolyEscape", 
                    $this->getUser()->getIdpers()->getEmail(),
                    "
                    <h2>Information de securite</h2>
                    <p>
                        Ce message vous prévient que votre mot de passe sur l'application PolyEscape a été changer le ".date("d/m/y")." à ".date("H:i:s")."
                    </p>
                    "
                );

                $this->addFlash('success', "Mot de passe modifier !");
                return $this->redirectToRoute('changemdpEtu');
            }
            $this->addFlash('danger', "Votre saisie est incorrect");
            return $this->redirectToRoute('changemdpEtu');
        }

        return $this->render('etudiant/changerMDP.html.twig', [
            'form' => $form->createView(),
            'activeMDP' => "active",
        ]);
    }

    private function sendMail($title, $to, $body)
    {
        // $transport = (new \Swift_SmtpTransport('smtp.mail.yahoo.com', 465))
        //     ->setUsername('polyescape@yahoo.com')
        //     ->setPassword('EVLJMescapegame')
        //     ->setEncryption('ssl');
        
        $transport = (new \Swift_SmtpTransport('smtp.univ-orleans.fr', 25));

        $mailer = new \Swift_Mailer($transport);

        $message = (new \Swift_Message($title))
            // -> setFrom("polyescape@yahoo.com")
            -> setFrom("polyescape@etu.univ-orleans.fr")
            -> setTo($to)
            -> setBody($body, "text/html");

        $mailer->send($message);

        sleep(1);
    }
}
